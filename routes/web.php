<?php

Route::get('/notification/data','AdminnotificationController@notification')->name('');

Route::get('/list/notification','AdminnotificationController@listnotification')->name('');

Route::get('/All/notification/list','AdminnotificationController@alldata')->name('');
/****************Api For Login  Related*******************/
Route::post('/Registration','UserregisterapiController@adduserapi')->name('');

Route::post('/verify/email','UserregisterapiController@confirmemail')->name('');

Route::post('/User/Login','UserloginapiController@userlogin')->name('');

Route::post('/User/Login/Social','UserloginapiController@sociallogin')->name('');

Route::post('/user/update/password','UserregisterapiController@updatepassword')->name('');

Route::post('/Sendemail/otp/reset','UserforgatepassController@send_email_otp')->name('');

Route::post('/user/reset/password','UserforgatepassController@reset_password')->name('');

/***********************Api User Personal Related********************/

Route::post('/List/profile/image','UpdateprofileapiController@showimage')->name('');

Route::get('/update/profile','UpdateprofileapiController@update_data')->name('');

Route::post('/user/update/personal/profile','UpdateprofileapiController@update_personal_info')->name('');
Route::post('/update/personal/Quote','UpdateprofileapiController@update_quote')->name('');

Route::post('/user/List/Quote','UpdateprofileapiController@userquotelist')->name('');

Route::post('/Personal/List','UpdateprofileapiController@user_personal_list')->name('');

Route::post('/User/hobbies/List','UpdateprofileapiController@hobbieslist')->name('');

Route::post('/image/upload','UpdateprofileapiController@image_update')->name('');

Route::post('/Update/user/name','UserregisterapiController@change_user_name')->name('');

Route::post('/Update/user/education','UpdateprofileapiController@usereducation')->name('');

Route::post('/User/educationlist','UpdateprofileapiController@educationlist')->name('');

Route::post('/User/employement','UpdateprofileapiController@useremployement')->name('');

Route::post('/User/employementlist','UpdateprofileapiController@employeelist')->name('');

Route::post('/User/skill/update','UpdateprofileapiController@userskillupdate')->name('');

Route::post('/User/skill/list','UpdateprofileapiController@userskillapi')->name('');

Route::post('/delete/education','UpdateprofileapiController@deleteedu')->name('');

Route::post('/delete/employement','UpdateprofileapiController@deleteemp')->name('');

/*******************API FOR ADMIN PANNEL To MIBILE REALETED***************************/

Route::get('/Country/Listing/ForApi','LocationController@apilist')->name('');

Route::post('/City/Listing/ForApi','LocationController@citylist')->name('');

Route::get('/Hobbies/Listing/ForApi','HobbiesController@Hobbieslist')->name('');

Route::get('/Job/List/ForApi','JobcategoryController@joblist')->name('');

Route::post('/Profession/List/ForApi','JobcategoryController@professionapi')->name('');

Route::get('/Langauge/Listing/ForApi','LangaugeController@languageapi')->name('');

Route::get('/Skill/Listing/ForApi','LangaugeController@skillapi')->name('');

Route::get('/Pet/Listing/ForApi','PetsaddController@petlist')->name('');

/*****************************API For User Job Posting*****************************/

Route::post('/Use/Job/Posting','JobpostController@store')->name('');

Route::post('/Use/Job/Posting/List','JobpostController@jobpstinglist')->name('');

Route::post('/Use/Job/Apply','ApplyjobController@applyjob')->name('');


/******************************Update Profile Url**********************************/

Route::get('/Admin/Update/Profile/','UpdateprofileController@index')->name('');

Route::post('/Update/admin/{id}/','UpdateprofileController@update_profile')->name('');

Route::get('/User/Listing/Detail/','UserprofileController@index')->name('');

Route::get('/User/Detail/{id}','UserprofileController@detailusers')->name('');

Route::get('/Delete/User/Onlist/{id}','UserprofileController@deleteuser')->name('');

/********************************Location Management*****************************/

Route::get('/Add/Country/','LocationController@index')->name('');

Route::post('/Add/Country/Code/','LocationController@insert_country')->name('');

Route::get('/Delete/Country/Code/{id}','LocationController@delete_country')->name('');

Route::get('/Update/Country/{id}','LocationController@updatecountry')->name('');

Route::post('/Update/Country/Code/{id}','LocationController@update_country')->name('');

Route::get('/Add/City/','LocationController@citymanagement')->name('');

Route::post('/Add/City/State','LocationController@addcity')->name('');

Route::get('/Delete/City/State/{ID}','LocationController@deletecity')->name('');

Route::get('/Update/City/{ID}','LocationController@updatecity')->name('');

Route::post('Update/City/State/{ID}','LocationController@update_city')->name('');

/****************************Hobbies management*********************************/
Route::get('/Add/User/Hobbies/','HobbiesController@index')->name('');

Route::post('/Add/Hobbies/','HobbiesController@store')->name('');

Route::get('/Edit/User/Hobbies/{id}','HobbiesController@edit')->name('');

Route::post('/update/hobbies/user/{id}','HobbiesController@update')->name('');

Route::get('/Delete/Usre/Hobbies/{id}','HobbiesController@destroy')->name('');

/*********************************Notification**************************/

Route::get('/Send/User/Notification/','NotificationController@index')->name('');

Route::post('/Add/User/Notification','NotificationController@store')->name('');

Route::get('/Delete/Notification/{id}','NotificationController@destroy')->name('');

Route::post('/chat/Notification','NotificationController@chatsendnotification')->name('');

/**************************Job Category*****************************/

Route::get('/Add/Job/Category/','JobcategoryController@index')->name('');

Route::get('/Readmore/jobdetail/{id}','UserprofileController@readmoredetail')->name('');

Route::post('/Add/User/Job/Category','JobcategoryController@addjob')->name('');

Route::get('/Edit/Job/Category/{id}','JobcategoryController@edit')->name('');

Route::post('/Update/Job/Category/{id}','JobcategoryController@update_job')->name('');

Route::get('/Delete/Job/Category/{id}','JobcategoryController@delete_id')->name('');

Route::get('/Add/Job/Profession/','JobcategoryController@professionindex')->name('');

Route::post('/Add/Profession/','JobcategoryController@addprofession')->name('');

Route::get('/Edit/Job/Profession/{id}','JobcategoryController@viewprofession')->name('');

Route::post('/Update/Profession/Record/{id}','JobcategoryController@update_recode')->name('');

Route::get('/Delete/Profession/{id}','JobcategoryController@delete_profession')->name('');

/***************************Add Langauge**********************************/

Route::get('/Add/Langauge/','LangaugeController@index')->name('');

Route::post('/Add/User/Langauge/','LangaugeController@addlanguage')->name('');

Route::get('/Edit/Langauge/{id}','LangaugeController@edit')->name('');

Route::post('/Update/Language/User/{id}','LangaugeController@update_language')->name('');

Route::get('/Delete/Langauge/{id}','LangaugeController@delete_language')->name('');

Route::get('/Add/Skill/','LangaugeController@skillindex')->name('');

Route::post('/Add/User/Skill/','LangaugeController@addskill')->name('');

Route::get('/Edit/Skill/{id}','LangaugeController@editskill')->name('');

Route::post('/Update/User/Skill/{id}','LangaugeController@updateskill')->name('');

Route::get('/Delete/Skill/{id}','LangaugeController@delete_skill')->name('');

/********************Pets Add Controller***********************/

Route::get('/Pet/Add','PetsaddController@index')->name('');

Route::get('/Edit/Pet/{id}','PetsaddController@edit')->name('');

Route::get('/Delete/Pet/{id}','PetsaddController@destroy')->name('');

Route::post('/Add/User/Pets','PetsaddController@store')->name('');

Route::post('/Update/User/Pets/{id}','PetsaddController@update')->name('');

/***************************Contact Us && Api ********************************/

Route::get('/Update/Contact/Us','ContactusController@contact')->name('');

Route::post('/Update/Contact','ContactusController@contact_us')->name('');

Route::get('/All/data/contact','ContactusController@getcontact')->name('');

Route::post('/User/Help/Contact','ContactusController@contacthelp')->name('');

/***********************Rating And Like Api*******************************/

Route::post('/job/like','RatingandlikeController@joblikeapi')->name('');

Route::post('/job/like/listing','RatingandlikeController@totaljoblike')->name('');

Route::post('/job/like/user/list','RatingandlikeController@joblikeuser_list')->name('');

Route::post('/user/rating','RatingandlikeController@addreating')->name('');

Route::post('/user/like','RatingandlikeController@addlike')->name('');

Route::post('/user/total/like','RatingandlikeController@totallike')->name('');

Route::post('/total/like/userlist','RatingandlikeController@userlist_likewise')->name('');

Route::post('/total/rating','RatingandlikeController@totalrating')->name('');

Route::post('/user/list/from/loginuser','RatingandlikeController@nonloginuser_list')->name('');

Route::post('/joblist/from/loginuser','RatingandlikeController@loginuser_likejob')->name('');

/**************************profile filter URL ********************************/

Route::get('/profile/filter','FilterControllercls@allpersonalfilter')->name('');

Route::get('/job/filter','FilterControllercls@alljobfilter')->name('');

Route::get('/character/list/for/filter','FilterControllercls@characterlist')->name('');

/*******************User Last Seen***************/
Route::post('/user/lastseen','LastseenController@lastseen')->name('');

/***************Change password admin****************/

Route::get('/Admin/Update/Password','ChangepasswordController@index')->name('');

Route::post('/change/password','ChangepasswordController@store')->name('');


/*********************job listing in admin pannel**********************/

Route::get('/User/job/Listing/','JobcategoryController@joblisting')->name('');


/*********************Abouse report api *****************/
Route::post('/Abouse/report','AbusereportController@abusereport')->name('');

Route::get('/Job/Abuse/Report/{id}','AbusereportController@abouselist')->name('');

Route::get('/User/Job/block/{id}','JobcategoryController@jobblock')->name('');


/*********************User Profile Video Upload********************/

Route::post('/User/vide/Upload','ProfilevideoController@upload')->name('');

Route::post('/User/vide/defalut','ProfilevideoController@videodefault')->name('');

Route::post('/User/defalutvideo/list','ProfilevideoController@uservideolist')->name('');

/*********************job publish and make private relate****************/

Route::post('/publish/job','ApplyjobController@jobpublish')->name('');

Route::post('/publish/job/apply/alluser','ApplyjobController@applyjobuserlist')->name('');

Route::post('/jobtitle/list','ApplyjobController@userapplyjoblist');
/**************************Update posted Job *************************/

Route::post('/update/posted/job','UpdatepostjobController@updatepostjob')->name('');

/*****************Add characteristics********************/
Route::get('/Add/Characteristics','CharactersticsController@index')->name('');

Route::post('/Insert/Characterstics','CharactersticsController@store')->name('');

Route::get('/Edit/characterstics/{id}','CharactersticsController@edit')->name('');

Route::post('/Update/Characterstics/{id}','CharactersticsController@update')->name('');

Route::get('/Delete/characterstics/{id}','CharactersticsController@destroy')->name('');

Route::get('/Characterstics/List','CharactersticsController@charactersticsapi')->name('');

Route::post('/User/Video/List/API','UpdateprofileapiController@uservideo')->name('');

/*******************************Excel Upload And download**********************/
Route::get('/Download-User','ExcelUploadController@mainuser')->name('');

Route::get('/Download-User-Sample','ExcelUploadController@downloaduser')->name('');

Route::post('/Upload/User/Byexcel','ExcelUploadController@userexcelupload')->name('');

Route::get('/Download-Job-Sample','ExceljobuploadController@jobsample')->name('');

Route::post('/Upload/job/Byexcel','ExceljobuploadController@jobuploadfromexcel')->name('');

Route::post('/Upload/Media/Byadmin','ExceljobuploadController@uploadmedia')->name('');

/********************job delete**************************/

Route::post('/Repost-job','JobpostController@repostjob')->name('');

Route::post('/delete-job','JobpostController@delete_posted_job')->name('');

Route::post('/delete-profile-video','ProfilevideoController@delete_user_profile_video')->name('');

Route::post('/total-like-people-job','RatingandlikeController@total_job_people_like')->name('');
/*****************flow chart********************/
Route::post('/personal-searching','SearchingController@personalsearch');

Route::post('/job-searching','SearchingController@jobsearch');

//Route::get('/chart/data','FlowchartController@getdata')->name('');
/**********************forgate password***********************/

Route::post('/send/otp','forgatepassController@sendotp')->name('sendotp');

Route::get('/verify/opt','forgatepassController@verify');

Route::post('/emailotp/verify','forgatepassController@verifyotp')->name('verifyotp');


Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('/home', function()
{
	
	$totaluser=DB::table('chat_user')->count('id');
	$totaljob=DB::table('user_job_post')->count('id');
	$totalapplyjob=DB::table('screen_job_apply')->count('id');
	$language=DB::table('language')->count('id');
/*********************************************************************************/
	$data="SELECT count(*) as 'No_OF_Users',Monthname(created_at) as 'Month',Year(created_at) as 'Year' from chat_user group by Month,year";
	$userdata=DB::select($data);
	
	$dataPoints=array();
	foreach ($userdata as $key => $value) 
	{
		$arr=array();
		$arr['y']=$value->No_OF_Users;
		$arr['label']=$value->Month;			
		array_push($dataPoints,$arr);
	}
/****************************************************************************/
$jobpost="SELECT count(*) as 'No_OF_post',Monthname(created_at) as 'Month',Year(created_at) as 'Year' from user_job_post group by Month,year";
$allpostjob=DB::select($jobpost);
$allpostjobdata = array();
foreach ($allpostjob as $key => $value) 
	{
		$arr=array();
		$arr['y']=$value->No_OF_post;
		$arr['label']=$value->Month;			
		array_push($allpostjobdata,$arr);
	}

/**********************************************************************/
$jobdata="SELECT COUNT(job_category) as 'totalcategory',job_category from job_category group by job_category";
$relatedjob=DB::select($jobdata);
$jobcategory = array();
foreach ($relatedjob as $key => $value) 
	{
		$arr=array();
		$arr['label']=$value->job_category;
		$arr['y']=$value->totalcategory;			
		array_push($jobcategory,$arr);
	}

   	return view('home',compact('totaluser','totaljob','totalapplyjob','language','dataPoints','jobcategory','allpostjobdata'));
});
//Route::get('/home', 'HomeController@index')->name('home');
