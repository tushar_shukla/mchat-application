  @extends('admin_dash.header')

 <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">

          <img src="{{ asset('image')}}/{{ Auth::user()->image }}" class="img-circle" alt="User Image">
        
        </div>
        <div class="pull-left info">
          <p>{{ Auth::user()->name }}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
 
      <ul class="sidebar-menu" data-widget="tree">
       
       
       
       <li><a href="{{url('/home')}}"><i class="fa fa-list"></i>Dashborad</a></li> 

       <li><a href="{{url('/Admin/Update/Profile/')}}"><i class="fa fa-edit"></i>Update Profile</a></li> 
       
       <li><a href="{{url('/User/Listing/Detail/')}}"><i class="fa fa-list"></i>User Listing</a></li>
       <li><a href="{{url('/User/job/Listing/')}}"><i class="fa fa-list"></i>Job Listing</a></li> 

      
        <li class="treeview">
          <a href="#">
            <i class="fa fa-map-marker"></i>
            <span>Location Management</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('/Add/Country/')}}"><i class="fa fa-edit"></i>Add Country</a></li>          
            <li><a href="{{url('/Add/City/')}}"><i class="fa fa-edit"></i>Add City</a></li>          
          </ul>
        </li> 

      <li><a href="{{url('/Add/User/Hobbies/')}}"><i class="fa fa-edit"></i>Add Hobbies</a></li> 
      <li><a href="{{url('/Pet/Add')}}"><i class="fa fa-paw"></i>Add Pets</a></li> 

      <li><a href="{{url('/Send/User/Notification/')}}"><i class="fa fa-bell"></i>Notification</a></li>


        <li class="treeview">
          <a href="#">
            <i class="fa fa-language"></i>
            <span>Job & Profession</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
             <li><a href="{{url('/Add/Job/Category/')}}"><i class="fa fa-edit"></i>Add Job Category</a></li> 
      <li><a href="{{url('/Add/Job/Profession/')}}"><i class="fa fa-edit"></i>Add Profession</a></li>  
         
                   
          </ul>
        </li>





    
        <li class="treeview">
          <a href="#">
            <i class="fa fa-language"></i>
            <span>Skills & Language</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('/Add/Langauge/')}}"><i class="fa fa-edit"></i>Add Language</a></li>          

            <li><a href="{{url('/Add/Skill/')}}"><i class="fa fa-edit"></i>Add Skills</a></li>          
                   
          </ul>
        </li> 
 <li><a href="{{url('/Add/Characteristics')}}"><i class="fa fa-edit"></i>Add Characteristics</a></li>
     <li><a href="{{url('/Update/Contact/Us')}}"><i class="fa fa-phone"></i>Contact-Us</a></li>

    <li><a href="{{ url ('/Admin/Update/Password') }}"><i class="fa fa-edit"></i>Change Password</a></li>

    <!--    <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
        document.getElementById('logout-form').submit();">
        <i class="fa fa-sign-out">
         <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf</form></i><span>Logout</span></a></li> -->

           
 
     
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>