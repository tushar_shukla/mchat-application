  
  <div class="wrapper">

  <header class="main-header">

    <!-- Logo -->
    <a href="{{'/home'}}" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><img src="{{ asset('css/dist/img/mchatlogo.png')}}" class="img-circle" alt="User Image" style="height: 40px;
    width: 40px;"></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg">
         <img src="{{ asset('css/dist/img/mchatlogo.png')}}" class="img-circle" alt="User Image" style="height: 46px;width: 48px;">
      </span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
        
          <!-- Notifications: style can be found in dropdown.less -->
          <li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" id="clickvalue">
              <i class="fa fa-bell-o" style="color:yellow"></i>
              <span class="label label-danger" id="value">0</span>
            </a>
            <ul class="dropdown-menu">
              <!-- <li class="header" >You have 0 notifications</li> -->
              <li>
              
                <ul class="menu" style="padding: 0px 0px 0px 10px;">

           
                </ul>
              </li>
              <li class="footer"><a href="{{url('/All/notification/list')}}">View all</a></li>
            </ul>
          </li>
          <li>
             <a style="padding-top: 1px;color: #fff;" href="href="{{ route('logout') }}"
                  onclick="event.preventDefault();
                   document.getElementById('logout-form').submit();" class="dropdown-toggle" data-toggle="dropdown">
                    <div class="btn btn-danger btn-flat" style="margin-top: -6px;" >{{ __('Logout') }}</a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                  </a>
          </li>
         
         
        </ul>
      </div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script type="text/javascript">

 $(document).ready(function() {
    //alert();
    $.ajax({
        url: "{{url('/notification/data')}}",
        type: "GET",
        dataType:"json",
        success: function(data){
           //console.log(data);//alert(data);
            $("#value").html(data);
        }
    });
});



 $("#clickvalue").click(function(){
    
    $.ajax({
        url: "{{url('/list/notification')}}",
        type: "GET",
        dataType:"text",
        success: function(data){
                      
            var obj = $.parseJSON($.parseJSON(data).data);
            //console.log(obj);
            var res="";

            $.each(obj, function(key,value) 
            {
              var name=value.user_name;
              //alert(name);
              if (value.type=='user') {
                res+="<li><i class='fa fa-user text-aqua'> <a href='{{url('detail/users')}}/"+value.user_id+"'>"+value.user_name+" </a></i> New User Add </li>";
              }else if (value.type=='job'){
                res+="<li><i class='fa fa-briefcase text-aqua'> "+value.jobuser+" </i> Post New Job</li>";
              }

                
                
              });
            //console.log(obj);
           $(".menu").html(res);
          
        }
    });
});
</script>
    </nav>
  </header>
