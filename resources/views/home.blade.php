
@extends('admin_dash.design')
@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
  

    
<div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>{{$totaluser}}</h3>

              <p>Total User's</p>
            </div>
            <div class="icon">
              <i class="fa fa-users"></i>
            </div>
            <a href="{{url('User/Listing/Detail')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>{{$language}}<sup style="font-size: 20px"></sup></h3>

              <p>Total Language</p>
            </div>
            <div class="icon">
              <i class="fa fa-comments"></i>
            </div>
            <a href="{{url('/Add/Langauge')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>{{$totaljob}}</h3>

              <p>Total Job's</p>
            </div>
            <div class="icon">
              <i class="fa fa-briefcase"></i>
            </div>
            <a href="{{url('/User/job/Listing')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red" style="height: 128px;">
            <div class="inner">
              <h3>{{$totalapplyjob}}</h3>

              <p>Total Apply Job</p>
            </div>
            <div class="icon">
              <i class="fa fa-comment"></i>
            </div>
           <!--  <a class="small-box-footer"> </a> -->
          </div>
        </div>
        <!-- ./col -->
      </div>
      <div class="row">
         <div class="col-md-6">
        <div id="chartContainer" style="height: 370px; width: 100%;"></div>
      </div>
      <div class="col-md-6">
        <div id="jobContainer" style="height: 370px; width: 100%;"></div>
      </div>

      </div>
   <div class="row" style="margin-top: 10px;">
    <div class="col-md-12">
      <div id="jobpostContainer" style="height: 370px; width: 100%;"></div>
   </div>
   </div>

<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
<script>
window.onload = function () {
 
var chart = new CanvasJS.Chart("chartContainer", {
  animationEnabled: true,
  theme: "light2", // "light1", "light2", "dark1", "dark2"
  title: {
    text: "Total User Added Month Wise"
  },
  subtitles: [{
    text: ""
  }],
  data: [{
    type: "column",
    //yValueFormatString: "#,##0.00\"%\"",
    indexLabel: "{label} ({y})",
    dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>
  }]
});
chart.render();
/*  animationEnabled: true,
  exportEnabled: true,
  theme: "light1", // "light1", "light2", "dark1", "dark2"
  title:{
    text: "Total User Added Month Wise"
  },
  data: [{
    type: "column", //change type to bar, line, area, pie, etc
    
    indexLabelFontColor: "#5A5757",
    indexLabelPlacement: "outside",   
    dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>
  }]
});
chart.render();*/
 var chart = new CanvasJS.Chart("jobpostContainer", {
  animationEnabled: true,
  theme: "light2", // "light1", "light2", "dark1", "dark2"
  title: {
    text: "All Job Post"
  },
  subtitles: [{
    text: ""
  }],
  data: [{
    type: "area",
    //yValueFormatString: "#,##0.00\"%\"",
    indexLabel: "{label} ({y})",
    dataPoints: <?php echo json_encode($allpostjobdata, JSON_NUMERIC_CHECK); ?>
  }]
});
chart.render();

var chart = new CanvasJS.Chart("jobContainer", {
  animationEnabled: true,
  theme: "light2", // "light1", "light2", "dark1", "dark2"
  title: {
    text: "Job Categories"
  },
  subtitles: [{
    text: ""
  }],
  data: [{
    type: "pie",
    //yValueFormatString: "#,##0.00\"%\"",
    indexLabel: "{label} ({y})",
    dataPoints: <?php echo json_encode($jobcategory, JSON_NUMERIC_CHECK); ?>
  }]
});
chart.render();
 
}
</script>



    </section>
    <!-- /.content -->
  </div>
   @endsection

