@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Reset Password') }}</div>
<style type="text/css">
.msg{
font-size: 18px !important;
text-align: center !important;
width: 50% !important;
margin: auto !important;
}

</style>

                @if(Session::has('flash_message'))
                <div class="alert alert-success msg" ><span class="glyphicon glyphicon-ok"></span>
                    <em>
                {!! session('flash_message') !!}</em></div> @endif

                @if(Session::has('flash_ma'))
                <div class="alert alert-danger msg" ><span class="glyphicon glyphicon-ok"></span>
                    <em>
                {!! session('flash_ma') !!}</em></div> @endif

                @if(Session::has('flash'))
                <div class="alert alert-danger msg" ><span class="glyphicon glyphicon-ok"></span>
                    <em>
                {!! session('flash') !!}</em></div> @endif

                @if(Session::has('edit'))
                <div class="alert alert-warning msg" ><span class="glyphicon glyphicon-ok"></span>
                    <em>
                {!! session('edit') !!}</em></div> @endif
                <div class="card-body">
                @if (session('status'))
                <div class="alert alert-success msg">
                {{ session('status') }}
                </div>
                @endif

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('sendotp') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                     {{ __('Send OTP') }}
                                   <!--  {{ __('Send Password Reset Link') }} -->
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
