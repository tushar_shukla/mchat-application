@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Reset Password') }}</div>

<style type="text/css">
  .msg{
    font-size: 18px;
    text-align: center;
    width: 50%;
    margin: auto;
  }

</style>
   
       @if(Session::has('flash_message'))
                    <div class="alert alert-success msg" ><span class="glyphicon glyphicon-ok"></span><em>
                      {!! session('flash_message') !!}</em></div> @endif

                      @if(Session::has('flash_ma'))
                    <div class="alert alert-danger msg" ><span class="glyphicon glyphicon-ok"></span><em>
                      {!! session('flash_ma') !!}</em></div> @endif

                     @if(Session::has('flash'))
                    <div class="alert alert-danger msg" ><span class="glyphicon glyphicon-ok"></span><em>
                      {!! session('flash') !!}</em></div> @endif

                      @if(Session::has('edit'))
                    <div class="alert alert-warning msg" ><span class="glyphicon glyphicon-ok"></span><em>
                      {!! session('edit') !!}</em></div> @endif

                <div class="card-body">
                    <form method="POST" action="{{ route('verifyotp') }}" id="form" name="form" onsubmit="return check()">
                        @csrf

                        

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email"     required autofocus>

                               
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="" class="col-md-4 col-form-label text-md-right">{{ __('Enter Valid Otp') }}</label>

                            <div class="col-md-6">
                                <input id="otp" type="number" name="otp" class="form-control" required autofocus>

                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                               
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="confirm_password" type="password" class="form-control" name="password_confirmation" required>
                                 <span id='message'></span>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Reset Password') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

    <script type="text/javascript">
      
 $('#password, #confirm_password').on('keyup', function () {
  if ($('#password').val() == $('#confirm_password').val()) {
    $('#message').html('Password Matching').css('color', 'green');
  } else 
    $('#message').html('Password Not Matching').css('color', 'red');
});
           
    $("#form").submit(function(){
     if($("#password").val()!=$("#confirm_password").val())
     {
         alert("New password And Confirm Password should be same");
         return false;
     }
    })

    </script>
</div>
@endsection
