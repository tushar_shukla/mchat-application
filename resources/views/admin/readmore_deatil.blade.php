@extends('admin_dash.design')
@section('content')
<div class="content-wrapper" style="min-height: 946px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Post Job Description
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('/home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
         <li><a href="{{url('/User/Listing/Detail/')}}">User Listing</a></li>
      <li><a href="{{url('/User/Detail/')}}">User Detail</a></li>
      <li><a href="">Read More</a></li>
   
      </ol>
        
   
                  @if(Session::has('flash_message'))
                    <div class="alert alert-success msg" ><span class="glyphicon glyphicon-ok"></span><em>
                      {!! session('flash_message') !!}</em></div> @endif

                      @if(Session::has('flash_ma'))
                    <div class="alert alert-danger msg" ><span class="glyphicon glyphicon-ok"></span><em>
                      {!! session('flash_ma') !!}</em></div> @endif

                     @if(Session::has('flash'))
                    <div class="alert alert-danger msg" ><span class="glyphicon glyphicon-ok"></span><em>
                      {!! session('flash') !!}</em></div> @endif

                      @if(Session::has('edit'))
                    <div class="alert alert-success msg" ><span class="glyphicon glyphicon-ok"></span><em>
                      {!! session('edit') !!}</em></div> @endif
                   
    </section>


   <section class="content">

      <div class="row">
        
        <!-- /.col -->
       <div class="col-md-9">
          <div class="nav-tabs-custom">
          
            <div class="">
  
            
                <div class="post">
                  <table class="table table-user-information">
                
                    <tbody>
                    @if($readmore->description !='')
                      <tr>
                        <td>Description:</td>
                       
                        <td>{{$readmore->description}}</td>
                        </tr>
                        <tr>
                        <td>Working Type:</td>
                       
                        <td>{{$readmore->work_type}}</td>
                        </tr>
                        <tr>
                        <td>Working Time:</td>
                       
                        <td>{{$readmore->working_time}}</td>
                        </tr>
                        <tr>
                        <td>Salary:</td>
                       
                        <td>{{$readmore->salery_range}}</td>
                        </tr> 

                        <tr>
                        <td>Public Holiday:</td>
                       
                        <td>{{$readmore->public_holiday}}</td>
                        </tr>
                        <tr>
                        <td>Leave Option:</td>
                       
                        <td>{{$readmore->leave_option}}</td>
                        </tr>
                        <tr>
                        <td>Characteristics:</td>
                       
                        <td>{{$readmore->chareterstics}}</td>
                        </tr>
                        <tr>
                        <td>Job Post Date:</td>
                       
                        <td>{{$readmore->created_at}}</td>
                        </tr> 
                         <tr>
                        <td>Job Start Date:</td>
                       
                        <td>{{$readmore->start_date}}</td>
                        </tr>
                      @else
                       <tr>
                        <td>Job apply </td>
                       
                        <td>Description not available</td>
                        </tr>
                     @endif
                    </tbody>
                   
                  </table>
                 
                
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
<!-- Modal -->

    </section>
    <!-- /.content -->
  </div>

   @endsection 