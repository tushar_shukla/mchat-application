@extends('admin_dash.design')
@section('content')
<div class="content-wrapper" style="min-height: 946px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       All Post Job Listing
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('/home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Post Job Listing</a></li>
      
   
      </ol>
        
   
                  @if(Session::has('flash_message'))
                    <div class="alert alert-success msg" ><span class="glyphicon glyphicon-ok"></span><em>
                      {!! session('flash_message') !!}</em></div> @endif

                      @if(Session::has('flash_ma'))
                    <div class="alert alert-danger msg" ><span class="glyphicon glyphicon-ok"></span><em>
                      {!! session('flash_ma') !!}</em></div> @endif

                     @if(Session::has('flash'))
                    <div class="alert alert-danger msg" ><span class="glyphicon glyphicon-ok"></span><em>
                      {!! session('flash') !!}</em></div> @endif

                      @if(Session::has('edit'))
                    <div class="alert alert-success msg" ><span class="glyphicon glyphicon-ok"></span><em>
                      {!! session('edit') !!}</em></div> @endif
                   
    </section>


    <!-- Main content -->
    <section class="content">
 <div class="row">
        <div class="col-sm-3">
      
        </div>
      <div class="col-sm-3">
     
        </div>
      
<div class="col-sm-6" style="padding-left: 64px;">
   <a href="{{url('/Download-Job-Sample')}}"><button type="button" class="btn btn-info">Export excel sample</button></a>
  <button type="button" class="btn btn-primary"   data-toggle="modal" data-target="#myModal">Import excel file</button>

  <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModalvideo">Upload Job Video/Audio</button></a>

</div>
</div>
  <div class="box">
            <div class="box-header">
              <h3 class="box-title">Job Listing</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap"><div class="row"><div class="col-sm-12"><table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                <thead>
                <tr role="row">
        <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 164px;">User name</th>
        <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 203px;">Job description</th>
        <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" style="width: 180px;">Work Type</th>
        
        <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 100px;">Salery Range</th>

        <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 100px;">Location</th>

        <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 100px;">Job date</th>
          <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 100px;">Posted Date</th>

        <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 100px;">Public holiday</th>  
         <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 100px;">job report</th>
         <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 100px;">job block</th>
        
        </tr>
                </thead>
                <tbody>
                
                
            @foreach($joblist as $key=>$data) 
                <tr role="row" class="even">
                  <td class="sorting_1">{{$data->username}}</td>
                  <td>{{$data->description}}</td>
                  <td>{{$data->work_type}}</td>
                  <td>{{$data->salery_range}}</td>
                  <td>{{$data->location}}</td>
                  <td>{{$data->start_date}}</td>
                  <td>{{date("d-m-Y",strtotime($data->created_at))}}</td>
                  <td>{{$data->public_holiday}}</td>
                  <td>@if($data->abusejob== $data->id)<a href="{{url('/Job/Abuse/Report')}}/{{$data->abusejob}}"><i title='Report on this job' class='fa fa-check-circle-o chechname' aria-hidden='true' ></i></a>@endif</td>
                  @if($data->status==1)
                <td><a href="{{url('/User/Job/block/')}}/{{$data->id}}"><button type="button" class="btn btn-danger" id="butt">
                <span class="fa fa-ban"></span> Block
              </button></a></td>
              @else
              <td><a href="{{url('/User/Job/block/')}}/{{$data->id}}"><button type="button" class="btn btn-warning" id="buttdd">
                <span class="fa fa-ban"></span>Unblock
              </button></a></td>
              @endif
                </tr>
                @endforeach
              </tbody>
                <tfoot>
              </tr>
                </tfoot>
              </table></div></div></div>
            </div>
            <!-- /.box-body -->
          </div>
<!-------------------Excel Upload Model----------------->
   <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Upload Job</h4>
        </div>
        <div class="modal-body">
          <form action="{{ url('/Upload/job/Byexcel') }}" method="POST" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
             
          <div class="form-group">
          <label for="inputEmail3" class="col-sm-3 control-label">Upload File</label>

          <div class="col-sm-8">
          <input type="file" class="form-control" name="uploadfile" placeholder="Fees" required="required"><br>
          </div>
          </div> 

            <div class="box-footer col-md-offset-5">
                 <button type="submit" class="btn btn-info">Upload</button>
             </div>

          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>

<!-------------------------Video Aduio Upload Model---------------------------->
 <div class="modal fade" id="myModalvideo" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Upload Media</h4>
        </div>
        <div class="modal-body">
          <form action="{{ url('/Upload/Media/Byadmin') }}" method="POST" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
             
          <div class="form-group">
          <label for="inputEmail3" class="col-sm-4 control-label">Upload Video/Audio</label>

          <div class="col-sm-7">
          <input type="file" class="form-control" name="video[]" placeholder="Fees" required="required" multiple><br>
          </div>
          </div>

            <div class="box-footer col-md-offset-5">
                 <button type="submit" class="btn btn-info">Upload</button>
             </div>

          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
<style>
.chechname{
       color: #f9213f;
    font-size: 21px;
}
</style>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script type="text/javascript">

$(document).ready(function(){
    $("#butt").click(function(){
        alert("Do you want to block this job in the suggestion");
    });
});

</script> 
   
    </section>
    <!-- /.content -->
  </div>

   @endsection 