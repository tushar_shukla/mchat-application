@extends('admin_dash.design')
@section('content')
<div class="content-wrapper" style="min-height: 946px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Country
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('/home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{url('/Add/Country/')}}">Add Country</a></li>
   
      </ol>
       @if(Session::has('flash_message'))
                    <div class="alert alert-success msg" ><span class="glyphicon glyphicon-ok"></span><em>
                      {!! session('flash_message') !!}</em></div> @endif

                      @if(Session::has('flash_ma'))
                    <div class="alert alert-danger msg" ><span class="glyphicon glyphicon-ok"></span><em>
                      {!! session('flash_ma') !!}</em></div> @endif

                     @if(Session::has('flash'))
                    <div class="alert alert-danger msg" ><span class="glyphicon glyphicon-ok"></span><em>
                      {!! session('flash') !!}</em></div> @endif

                      @if(Session::has('edit'))
                    <div class="alert alert-warning msg" ><span class="glyphicon glyphicon-ok"></span><em>
                      {!! session('edit') !!}</em></div> @endif
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
     
        <!-- right column -->
        <div class="col-md-8 col-md-offset-2">
                   
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title"></h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form action="{{url('/Add/Country/Code/')}}" method="POST" class="form-horizontal">
              <input type="hidden" name="_token" value="{{csrf_token()}}">
              
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-3 control-label">Country Name<span class="point">*</span></label>

                  <div class="col-sm-8">
                    <input type="text" name="country_name" class="form-control" id="name" placeholder="Enter Country Name">
                    
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-3 control-label">Country Code<span class="point">*</span></label>

                  <div class="col-sm-8">
                    <input type="text" name="country_code" class="form-control" id="code"  placeholder="Enter Country Code" maxlength="3" minlength="3">
                    <span id="errmsg"></span> 
                  </div>
                </div>
               

               
              </div>
              <!-- /.box-body -->
              <div class="box-footer col-md-offset-4">
                <button type="submit" name="submit" class="btn btn-success ">Add Country</button>
                <button type="reset" class="btn btn-danger">Reset</button>
                
              </div>
          
            </form>
          </div>
      
        </div>

      </div>
      <!-- /.row -->
      <div class="row">
      <div class="col-md-10 col-md-offset-1">
<div class="box">
            <div class="box-header">
              <h3 class="box-title">Country Listing</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap"><div class="row"><div class="col-sm-12"><table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                <thead>
                <tr role="row">
        <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 164px;">Name</th>

         <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 164px;">Code</th>
   <!--       <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 164px;">Code2</th> -->
        
        <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 100px;">Action</th>
        
        </tr>
                </thead>
                <tbody>
                
              @foreach($country as $key=>$data)
                 <tr role="row" class="even">
                  <td >{{$data->Name}}</td>
                  <td >{{$data->Code}}</td>
             <!--    -->
                                  
                  
                  <td>
                    <a href="{{url('/Update/Country/')}}/{{$data->id}}">
                      <i class="fa fa-edit total pointer" aria-hidden="true">
                       </i>
                     </a>
                     <a href="{{url('/Delete/Country/Code/')}}/{{$data->id}}"><i class="fa fa-fw fa-trash-o total pointer red"></i></a>
                   <!--  <a href="{{url('/User/Detail/')}}">
                      <i class="fa fa-info-circle total pointer" aria-hidden="true">
                       </i></a> -->
                  </td>
                </tr> 
              @endforeach

              </tbody>
                <tfoot>
              </tr>
                </tfoot>
              </table></div></div></div>
            </div>
            <!-- /.box-body -->
          </div>
          </div>
          </div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script type="text/javascript">
$( "#code" ).on('input', function() {
    if ($(this).val().length>3) {
        alert('you have reached a limit of 3');       
    }
});
</script> 

    </section>
    <!-- /.content -->
  </div>

   @endsection