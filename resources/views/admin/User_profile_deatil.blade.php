@extends('admin_dash.design')
@section('content')
<div class="content-wrapper" style="min-height: 946px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Update Profile
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('/home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
         <li><a href="{{url('/User/Listing/Detail/')}}">User Listing</a></li>
    <li><a href="{{url('/User/Detail/')}}">User Detail</a></li>
   
      </ol>
        
   
                  @if(Session::has('flash_message'))
                    <div class="alert alert-success msg" ><span class="glyphicon glyphicon-ok"></span><em>
                      {!! session('flash_message') !!}</em></div> @endif

                      @if(Session::has('flash_ma'))
                    <div class="alert alert-danger msg" ><span class="glyphicon glyphicon-ok"></span><em>
                      {!! session('flash_ma') !!}</em></div> @endif

                     @if(Session::has('flash'))
                    <div class="alert alert-danger msg" ><span class="glyphicon glyphicon-ok"></span><em>
                      {!! session('flash') !!}</em></div> @endif

                      @if(Session::has('edit'))
                    <div class="alert alert-success msg" ><span class="glyphicon glyphicon-ok"></span><em>
                      {!! session('edit') !!}</em></div> @endif
                   
    </section>


   <section class="content">

      <div class="row">
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">
              @if($personal !='')
                @if($personal->image !='')
              <img class="profile-user-img img-responsive img-circle" src="{{ asset('')}}{{$personal->image}}" alt="User profile picture" style="    height: 100px;
    width: 100px;">
              @else
               <img class="profile-user-img img-responsive img-circle" src="{{ asset('image/dummy.jpg')}}" alt="User profile picture">
               @endif
              <!-- {{ asset('css/dist/img/user4-128x128.jpg')}} -->
              @else
              <img class="profile-user-img img-responsive img-circle" src="{{ asset('image/dummy.jpg')}}" alt="User profile picture">
              @endif
              <h3 class="profile-username text-center">{{$user->user_name}}</h3>
              @if(empty($employee->designation))
              <p class="text-muted text-center">Designation Not Available</p>
              @else
              <p class="text-muted text-center">{{$employee->designation}}</p>
              @endif
              

              

              
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          <!-- About Me Box -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">About Me</h3>
            </div>
            <!-- /.box-header -->
          <div class="box-body">
              <strong><i class="fa fa-book margin-r-5"></i> Education</strong>
              @if(!empty($useredu->level))
              <p class="text-muted">
               {{$useredu->level}}
              </p>
              @else
                <p class="text-muted">
                  Not Available
                 </p>
              @endif
              <hr>

              <strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>
              @if(!empty($personal->ca1) && !empty($personal->ca2) && !empty($personal->ca3) &&!empty($personal->ccity_id))
              <p class="text-muted">{{$personal->ca1}} , {{$personal->ca2}} , {{$personal->ca3}} , {{$personal->city->Name}}</p>

              @else
               <p class="text-muted">Not Available</p>

               @endif
              <hr>
               <strong><i class="fa fa-pencil margin-r-5"></i> Skills & Charterstics</strong>
              
              <p>
                 @foreach($skill as $data)
                             
                 @if(!empty($data->skill_id))
                <span class="label label-primary">{{$data->skill_id}}</span>
                @else
                <span class="label label-danger">Not Available</span>
                @endif
                @if(!empty($data->charecter_sticks))
               <span class="label label-info">{{$data->charecter_sticks}}</span>
                 <!-- <span class="label label-info">Javascript</span>
                <span class="label label-warning">PHP</span>
                <span class="label label-primary">Node.js</span> -->
                @else
                <span class="label label-success">Not Available</span>
                @endif
                 @endforeach
              </p>
           
            </div>
         
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
       <div class="col-md-9">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#activity" data-toggle="tab">Personal</a></li>
              <li><a href="#timeline" data-toggle="tab">Educational</a></li>
              <li><a href="#settings" data-toggle="tab">Professional</a></li>
              <li><a href="#jobpost" data-toggle="tab">Posted Jobs</a></li>
              <li><a href="#applyjob" data-toggle="tab">Applied Jobs</a></li>
            </ul>
            <div class="tab-content">
              <div class="active tab-pane" id="activity">
                <!-- Post -->
                <div class="post">
                  <div class="user-block">
                  
                    
                  </div>
                  <!-- /.user-block -->

                 <table class="table table-user-information">
                 
                    <tbody>
                      <tr>
                        <td>Name</td>
                        <td>{{$user->user_name}}</td>
                      </tr>

                        <tr>
                        <td>Quote</td>
                        @if(!empty($quote->quote))
                        <td>{{$quote->quote}}</td>
                        @else
                         <td>Not Available</td>
                        @endif 
                      </tr>
                        <tr>
                        <td>Email</td>
                        <td><a href="#.">{{$user->email}}</a></td>
                      </tr>
                      <tr>
                        <td>Mobile</td>
                        @if(!empty($personal->contact))
                        <td>{{$personal->contact}}</td>
                        @else
                        <td>Not Available</td>
                        @endif
                      </tr>
                      <tr>
                        <td>Date of Birth</td>
                        @if(!empty($personal->dob))
                        <td>{{$personal->dob}}</td>
                        @else
                        <td>Not Available</td>
                        @endif
                      </tr>
                      <tr>
                        <td>Gender</td>
                        @if(!empty($personal->gender))
                        <td>{{$personal->gender}}</td>
                        @else
                        <td>Not Available</td>
                        @endif
                      </tr>
                      <tr>
                        <td>Marital Status</td>
                        @if(!empty($personal->meterial_status))
                        <td>{{$personal->meterial_status}}</td>
                        @else
                        <td>Not Available</td>
                        @endif
                      </tr>
                      <tr>
                        <td>Weight</td>
                        @if(!empty($personal->weight))
                        <td>{{$personal->weight}}</td>
                        @else
                        <td>Not Available</td>
                        @endif
                      </tr>
                      <tr>
                        <td>Height</td>
                        @if(!empty($personal->height))
                        <td>{{$personal->height}}</td>
                        @else
                        <td>Not Available</td>
                        @endif
                      </tr>
                      <tr>
                        <td>Nationality</td>
                        @if(!empty($personal->nationality))
                        <td>{{$personal->national->Name}}</td>
                        @else
                        <td>Not Available</td>
                        @endif
                      </tr>
                      <tr>
                        <td>Pets Love</td>
                          @if(!empty($personal->pet_love))
                        <td>{{$personal->petname->pets_name}}</td>
                        @else
                        <td>Not Available</td>
                        @endif
                      </tr>
                    
                        <tr>
                        <td>Current Address</td>
                        @if(!empty($personal->ca1) && !empty($personal->ca2) && !empty($personal->ca3) && !empty($personal->ccity_id))
                        <td>{{$personal->ca1}} , {{$personal->ca2}} , {{$personal->ca3}} , {{$personal->city->Name}}</td>
                        @else
                        <td>Not Available</td>
                        @endif
                      </tr>
                       <tr>
                        <td>Permanent Address</td>
                         @if(!empty($personal->pa1) && !empty($personal->pa2) && !empty($personal->pa3) && !empty($personal->pcity_id))
                        <td>{{$personal->pa1}} , {{$personal->pa2}} , {{$personal->pa3}} , {{$personal->pcity->Name}}</td>

                        @else
                        <td>Not Available</td>
                        @endif
                      </tr>
                    
                    
                     
                    </tbody>
                   
                  </table>
                  

                 
                </div>
               
                              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="timeline">
                <!-- The timeline -->
                   <div class="post">
                  <div class="user-block">
                   
                  </div>
                 
                  <!-- END timeline item -->
                  <!-- timeline item -->
                  <table class="table table-user-information">
                  
                    @foreach($education as $dataedu)
                    <tbody>
                    
                      <tr>
                        <td>Education Level</td>
                        @if(!empty($dataedu->level))
                        <td>{{$dataedu->level}}</td>
                        @else
                        <td>Not Available</td>
                        @endif
                      </tr>
                      <tr>
                        <td>University/Collage</td>
                        @if(!empty($dataedu->board))
                        <td>{{$dataedu->board}}</td>
                        @else
                        <td>Not Available</td>
                        @endif
                      </tr>
                      <tr>
                        <td>Course</td>
                        @if(!empty($dataedu->degree))
                        <td>{{$dataedu->degree}}</td>
                        @else
                        <td>Not Available</td>
                        @endif
                      </tr>
                      <tr>
                        <td>From Year</td>
                        @if(!empty($dataedu->from_year))
                        <td>{{$dataedu->from_year}}</td>
                        @else
                        <td>Not Available</td>
                        @endif
                      </tr>
                      <tr>
                        <td>To Year</td>
                        @if(!empty($dataedu->to_year))
                        <td>{{$dataedu->to_year}}</td>
                        @else
                        <td>Not Available</td>
                        @endif
                      </tr>
                  
                     
                    </tbody>
                    @endforeach
                    
                  </table>
                 
                </div>

                 
                  
                 
              </div>
              <!-- /.tab-pane -->

              <div class="tab-pane" id="settings">
                <div class="post">
                  <div class="user-block">
                  
                    
                  </div>
                <table class="table table-user-information">
                  @foreach($employement as $empdata)
                    <tbody>
                    
                      <tr>
                        <td>Company Name:</td>
                        @if(!empty($empdata->company))
                        <td>{{$empdata->company}}</td>
                        @else
                        <td>Not Available</td>
                        @endif
                      </tr>
                      <tr>
                        <td>Designation:</td>
                        @if(!empty($empdata->designation))
                        <td>{{$empdata->designation}}</td>
                        @else
                        <td>Not Available</td>
                        @endif
                      </tr>
                      <tr>
                        <td>From Date</td>
                        @if(!empty($empdata->fromdate))
                        <td>{{$empdata->fromdate}}</td>
                        @else
                        <td>Not Available</td>
                        @endif
                      </tr>
                   
                         <tr>
                             <tr>
                        <td>To Date</td>
                        @if(!empty($empdata->todate))
                        <td>{{$empdata->todate}}</td>
                        @else
                        <td>Not Available</td>
                        @endif
                      </tr>
                    
                     
                    </tbody>
                    @endforeach
                  </table>
              </div>
              <!-- /.tab-pane -->
            </div>

            <div class="tab-pane" id="jobpost">
                <div class="post">
                  <div class="user-block">
                   
                    
                  </div>
                 
                  <ul class="timeline timeline-inverse">
              @foreach($jobpost as $jobdata)
                  <li class="time-label">
                        <span class="bg-red">
                         {{$jobdata->created_at->format('d/m/Y')}}
                        </span>
                  </li>
                
                  <li>
                    <i class="fa fa-envelope bg-blue"></i>

                    <div class="timeline-item">
                     <h3 class="timeline-header"><a href="#">Job start Date</a>   {{$jobdata->start_date}}</h3>

                      <div class="timeline-body">
                       {{$jobdata->description}}
                      </div>
                      <div class="timeline-footer">
                        <a href="{{url('/Readmore/jobdetail')}}/{{$jobdata->id}}" class="btn btn-primary btn-xs">Read more</a>
                       <!--  <a class="btn btn-danger btn-xs">Delete</a> -->
                      </div>
                    </div>
                  </li>
                 
                  @endforeach
                
         
                </ul>
              </div>
          
            </div>
             <div class="tab-pane" id="applyjob">
                <div class="post">
                  <div class="user-block">
                  
                    
                  </div>
                 
                  <ul class="timeline timeline-inverse">
                  <!-- timeline time label -->
                  @foreach($jobapply as $applyjob)
              
                  <li>
                    <i class="fa fa-briefcase bg-blue"></i>

                    <div class="timeline-item">
                      <h3 class="timeline-header"><a href="#">Applied date </a> {{$applyjob->created_at}}</h3>

                      <div class="timeline-body">
                        <video width="320" height="240" controls>
                        <source src="{{ asset('')}}{{$applyjob->screening_ans}}" type="video/mp4">
                        
                      
                      </video>
                       
                      </div>
                      <div class="timeline-footer">
                         <!-- <a href="{{url('/Readmore/jobdetail')}}/{{$applyjob->job_id}}" class="btn btn-primary btn-xs">Read more</a> -->
                        
                       <!--  <a class="btn btn-danger btn-xs">Delete</a> -->
                      </div>
                    </div>
                  </li>
                 @endforeach
                </ul>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
<!-- Modal -->

    </section>
    <!-- /.content -->
  </div>

   @endsection 