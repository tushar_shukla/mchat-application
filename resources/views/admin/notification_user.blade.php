@extends('admin_dash.design')
@section('content')
<div class="content-wrapper" style="min-height: 946px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Admin Notification Listing
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('/home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="">Admin Notification Listing</a></li>
      
   
      </ol>
        
   
                  @if(Session::has('flash_message'))
                    <div class="alert alert-success msg" ><span class="glyphicon glyphicon-ok"></span><em>
                      {!! session('flash_message') !!}</em></div> @endif

                      @if(Session::has('flash_ma'))
                    <div class="alert alert-danger msg" ><span class="glyphicon glyphicon-ok"></span><em>
                      {!! session('flash_ma') !!}</em></div> @endif

                     @if(Session::has('flash'))
                    <div class="alert alert-danger msg" ><span class="glyphicon glyphicon-ok"></span><em>
                      {!! session('flash') !!}</em></div> @endif

                      @if(Session::has('edit'))
                    <div class="alert alert-success msg" ><span class="glyphicon glyphicon-ok"></span><em>
                      {!! session('edit') !!}</em></div> @endif
                   
    </section>


    <!-- Main content -->
    <section class="content">
 
  <div class="box">
            <div class="box-header">
              <h3 class="box-title">Admin Notification Listing</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap"><div class="row"><div class="col-sm-12"><table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                <thead>
                <tr role="row">
				<th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 164px;">Add User Name</th>
      
				<th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 203px;">Add date</th>
				<th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" style="width: 180px;">Type</th>
				<!-- <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending" style="width: 140px;">Country</th>-->
				<th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 100px;">Action</th> 
				
				</tr>
                </thead>
                <tbody>
                
                
		        @foreach($notificationdata as $key=>$data) 
                <tr role="row" class="even">
              @if($data->type== 'user')
                  <td class="sorting_1">{{$data->user_name}}</td>
                  
                  <td>{{$data->created_at}}</td>
                  <td>{{$data->type}}</td>
                  <td>
                    <a href="{{url('/User/Detail/')}}/{{$data->user_id}}">
                      <i class="fa fa-info-circle total pointer " aria-hidden="true">
                       </i></a>
                  </td>
                  @elseif($data->type=='job')
                  <td>{{$data->jobuser}}</td>
                  <td>{{$data->created_at}}</td>
                   <td>{{$data->type}}</td>
                     <td> <a href="{{url('/User/Detail/')}}/{{$data->post_job_user_id}}">
                      <i class="fa fa-info-circle total pointer " aria-hidden="true">
                       </i></a>  </td>
                  @endif
                </tr>
                @endforeach
              </tbody>
                <tfoot>
              </tr>
                </tfoot>
              </table></div></div></div>
            </div>
            <!-- /.box-body -->
          </div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script type="text/javascript">
 
</script> 
   
    </section>
    <!-- /.content -->
  </div>

   @endsection 