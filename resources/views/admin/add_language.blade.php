@extends('admin_dash.design')
@section('content')
<div class="content-wrapper" style="min-height: 946px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Language
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('/home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Add Language</a></li>
   
      </ol>
       @if(Session::has('flash_message'))
                    <div class="alert alert-success msg" ><span class="glyphicon glyphicon-ok"></span><em>
                      {!! session('flash_message') !!}</em></div> @endif

                      @if(Session::has('flash_ma'))
                    <div class="alert alert-danger msg" ><span class="glyphicon glyphicon-ok"></span><em>
                      {!! session('flash_ma') !!}</em></div> @endif

                     @if(Session::has('flash'))
                    <div class="alert alert-danger msg" ><span class="glyphicon glyphicon-ok"></span><em>
                      {!! session('flash') !!}</em></div> @endif

                      @if(Session::has('edit'))
                    <div class="alert alert-warning msg" ><span class="glyphicon glyphicon-ok"></span><em>
                      {!! session('edit') !!}</em></div> @endif
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
     
        <!-- right column -->
        <div class="col-md-8 col-md-offset-2">
                   
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title"></h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form action="{{url('/Add/User/Langauge/')}}" method="POST" class="form-horizontal">
              <input type="hidden" name="_token" value="{{csrf_token()}}">
              
              <div class="box-body">
                
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-3 control-label">Language<span class="point">*</span></label>

                  <div class="col-sm-9">
                    <input type="text" name="language" class="form-control" id="language" required="required" placeholder="Enter Language Here...">
                    
                  </div>
                </div>
               

               
              </div>
              <!-- /.box-body -->
              <div class="box-footer col-md-offset-4">
                <button type="submit" name="submit" class="btn btn-success ">Submit</button>
                <button type="reset" class="btn btn-danger">Reset</button>
                
              </div>
          
            </form>
          </div>
      
        </div>

      </div>
     <!-- /.row -->
      <div class="row">
      <div class="col-md-8 col-md-offset-2">
<div class="box">
            <div class="box-header">
              <h3 class="box-title">Language Listing</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap"><div class="row"><div class="col-sm-12"><table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                <thead>
                <tr role="row">
        <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 164px;">Notification</th>
        
        
        <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 100px;">Action</th>
        
        </tr>
                </thead>
                <tbody>
                
                
                @foreach($lang as $key=>$data)
                <tr role="row" class="even">
                  <td class="sorting_1">{{$data->language}}</td>
                 

                  
                  <td>
                    <a href="{{url('/Edit/Langauge/')}}/{{$data->id}}">
                      <i class="fa fa-edit total pointer" aria-hidden="true">
                       </i>
                     </a>
                  <a href="{{url('/Delete/Langauge')}}/{{$data->id}}"><i class="fa fa-fw fa-trash-o total pointer red" onclick="return confirm('Are You sure To Delete This Record')"></i></a>

                    <!-- <a href="{{url('/User/Detail/')}}">
                      <i class="fa fa-info-circle total pointer" aria-hidden="true">
                       </i>
                     </a> -->
                  </td>
                </tr>
                @endforeach
              </tbody>
                <tfoot>
              </tr>
                </tfoot>
              </table></div></div></div>
            </div>
            <!-- /.box-body -->
          </div>
          </div>
          </div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script type="text/javascript">
 
</script> 
    </section>
    <!-- /.content -->
  </div>

   @endsection