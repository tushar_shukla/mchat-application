@extends('admin_dash.design')
@section('content')
<div class="content-wrapper" style="min-height: 946px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Update Characterstics
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('/home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{url('/Add/Characteristics')}}">Add Characterstics</a></li>
        <li><a href="#">Update Characterstics</a></li>
   
      </ol>
         @if(Session::has('flash_message'))
                    <div class="alert alert-success msg" ><span class="glyphicon glyphicon-ok"></span><em>
                      {!! session('flash_message') !!}</em></div> @endif

                      @if(Session::has('flash_ma'))
                    <div class="alert alert-danger msg" ><span class="glyphicon glyphicon-ok"></span><em>
                      {!! session('flash_ma') !!}</em></div> @endif

                     @if(Session::has('flash'))
                    <div class="alert alert-danger msg" ><span class="glyphicon glyphicon-ok"></span><em>
                      {!! session('flash') !!}</em></div> @endif

                      @if(Session::has('edit'))
                    <div class="alert alert-warning msg" ><span class="glyphicon glyphicon-ok"></span><em>
                      {!! session('edit') !!}</em></div> @endif
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
     
        <!-- right column -->
        <div class="col-md-8 col-md-offset-2">
                 
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title"></h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form action="{{url('/Update/Characterstics')}}/{{$data->id}}" method="POST" class="form-horizontal">
              <input type="hidden" name="_token" value="{{csrf_token()}}">
              
              <div class="box-body">
                
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-3 control-label">Characterstics<span class="point">*</span></label>

                  <div class="col-sm-9">
                    <input type="text" name="characterstics" class="form-control" id="Characterstics" required="required" placeholder="Update Characterstics Here..." value="{{$data->characterstics}}">
                    
                  </div>
                </div>
               

               
              </div>
              <!-- /.box-body -->
              <div class="box-footer col-md-offset-4">
                <button type="submit" name="submit" class="btn btn-success ">Update</button>
                <a href="{{url('/Add/Characteristics/')}}"><button type="button" class="btn btn-danger">Cancel</button></a>
                
              </div>
          
            </form>
          </div>
      
        </div>

      </div>
     
    </section>
    <!-- /.content -->
  </div>

   @endsection