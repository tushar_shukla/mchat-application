@extends('admin_dash.design')
@section('content')
<div class="content-wrapper" style="min-height: 946px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        User Listing
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('/home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{url('/User/Listing/Detail/')}}">User Listing</a></li>
      
   
      </ol>
        
   
                  @if(Session::has('flash_message'))
                    <div class="alert alert-success msg" ><span class="glyphicon glyphicon-ok"></span><em>
                      {!! session('flash_message') !!}</em></div> @endif

                      @if(Session::has('flash_ma'))
                    <div class="alert alert-danger msg" ><span class="glyphicon glyphicon-remove"></span><em>
                      {!! session('flash_ma') !!}</em></div> @endif

                     @if(Session::has('flash'))
                    <div class="alert alert-danger msg" ><span class="glyphicon glyphicon-remove"></span><em>
                      {!! session('flash') !!}</em></div> @endif

                      @if(Session::has('edit'))
                    <div class="alert alert-success msg" ><span class="glyphicon glyphicon-ok"></span><em>
                      {!! session('edit') !!}</em></div> @endif
                   
    </section>


    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-sm-3">
      
        </div>
      <div class="col-sm-3">
     
        </div>
      
<div class="col-sm-6" style="padding-left: 106px;">
  <a href="{{url('/Download-User')}}"><button type="button" class="btn btn-success">Download User's</button></a>
   <a href="{{url('/Download-User-Sample')}}"><button type="button" class="btn btn-info">Export excel sample</button></a>
  <button type="button" class="btn btn-primary"   data-toggle="modal" data-target="#myModal">Import excel file</button>

</div>
      </div>
 
 
  <div class="box">

            <div class="box-header">
              <h3 class="box-title">User Listing</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap"><div class="row"><div class="col-sm-12"><table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                <thead>
                <tr role="row">
				<th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 164px;">Name</th>
				<th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 203px;">Mobile</th>
				<th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" style="width: 180px;">Email</th>
				<th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending" style="width: 140px;">Add Date</th>
        <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending" style="width: 140px;">Add Time</th>
				<th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 100px;">Action</th>
				
				</tr>
                </thead>
                <tbody>
                
                
		        @foreach($userdata as $key=>$data) 
                <tr role="row" class="even">
                  <td class="sorting_1">{{$data->user_name}}</td>
                  <td>{{$data->contact}}</td>
                  <td>{{$data->email}}</td>
                  <td>{{date("d-m-Y",strtotime($data->created_at))}}</td>
                  <td>{{date("H:i:s",strtotime($data->created_at))}}</td>
                  <td>
                    <a href="{{url('/Delete/User/Onlist')}}/{{$data->id}}"><i class="fa fa-fw fa-trash-o total pointer red " onclick="return confirm('Are you sure to delete this record')" ></i></a>
                    
				            <a href="{{url('/User/Detail/')}}/{{$data->id}}">
                      <i class="fa fa-info-circle total pointer " aria-hidden="true">
                       </i></a>
                  </td>
                </tr>
                @endforeach
              </tbody>
                <tfoot>
              </tr>
                </tfoot>
              </table></div></div></div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Upload User</h4>
        </div>
        <div class="modal-body">
          <form action="{{ url('/Upload/User/Byexcel') }}" method="POST" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
             
          <div class="form-group">
          <label for="inputEmail3" class="col-sm-3 control-label">Upload File</label>

          <div class="col-sm-8">
          <input type="file" class="form-control" name="uploadfile" placeholder="Fees" required="required"><br>
          </div>
          </div> 

            <div class="box-footer col-md-offset-5">
                 <button type="submit" class="btn btn-info">Upload</button>
             </div>

          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script type="text/javascript">
 
</script> 
   
    </section>
    <!-- /.content -->
  </div>

   @endsection 