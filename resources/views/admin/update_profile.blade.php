@extends('admin_dash.design')
@section('content')
<div class="content-wrapper" style="min-height: 946px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Update Profile
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('/home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{url('/Admin/Update/Profile/')}}">Update Profile</a></li>
   
      </ol>
        
   
                  @if(Session::has('flash_message'))
                    <div class="alert alert-success msg" ><span class="glyphicon glyphicon-ok"></span><em>
                      {!! session('flash_message') !!}</em></div> @endif

                      @if(Session::has('flash_ma'))
                    <div class="alert alert-danger msg" ><span class="glyphicon glyphicon-ok"></span><em>
                      {!! session('flash_ma') !!}</em></div> @endif

                     @if(Session::has('flash'))
                    <div class="alert alert-danger msg" ><span class="glyphicon glyphicon-ok"></span><em>
                      {!! session('flash') !!}</em></div> @endif

                      @if(Session::has('edit'))
                    <div class="alert alert-success msg" ><span class="glyphicon glyphicon-ok"></span><em>
                      {!! session('edit') !!}</em></div> @endif
                   
    </section>


    <!-- Main content -->
    <section class="content">
 
      <div class="row">
     
        <!-- right column -->
        <div class="col-md-10 col-md-offset-1">
          <!-- Horizontal Form -->
          <div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title"></h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" action="{{ url('/Update/admin') }}/{{$admindata->id}}" method="POST" enctype="multipart/form-data">
               <input type="hidden" name="_token" value="{{csrf_token()}}">
                 
              <div class="box-body">
                 

                  <div class="form-group">
                  <label for="inputEmail3" class="col-sm-3 control-label">Name<span class="point">*</span></label>

                  <div class="col-sm-7">
                   <input type="text" class="form-control no-drop" placeholder="Enter Full Name Here.." value="{{$admindata->name}}" name="name" id="name" required="required">
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-3 control-label">Mobile<span class="point">*</span></label>

                  <div class="col-sm-7">
                   <input type="number" value="{{$admindata->mobile}}" class="form-control" name="mobile" id="mobile" placeholder="Enter Mobile Number Here.." value="" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"   maxlength = "10"  required="required">
                   <span id="message"></span>
                  </div>
                </div>

                  <div class="form-group">
                  <label for="inputEmail3" class="col-sm-3 control-label">Email<span class="point">*</span></label>

                  <div class="col-sm-7">
                   <input type="email" class="form-control no-drop" name="email" id="email" placeholder="Enter Email id Here.." value="{{$admindata->email}}" required="required">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-3 control-label">City</label>

                  <div class="col-sm-7">
                   <input type="text" class="form-control" name="city" id="city" placeholder="Enter City Here.." value="{{$admindata->city}}">
                  </div>
                </div> 
                  <div class="form-group">
                  <label for="inputEmail3" class="col-sm-3 control-label">State</label>

                  <div class="col-sm-7">
                   <input type="text" class="form-control" name="state" id="state" placeholder="Enter State Here.." value="{{$admindata->state}}">
                  </div>
                </div>  


                 <div class="form-group">
                  <label for="inputEmail3" class="col-sm-3 control-label">Country </label>

                  <div class="col-sm-7">
                   <input type="text" class="form-control" name="country" id="country" placeholder="Enter Country Here.." value="{{$admindata->country}}">
                  </div>
                </div>

                   <div class="form-group">
                  <label for="inputEmail3" class="col-sm-3 control-label">Address</label>

                  <div class="col-sm-7">
                   <textarea name="address" class="form-control" id="address" placeholder="Enter Address Here..">{{$admindata->address}}</textarea>
                  </div>
                </div>
             
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-3 control-label">Upload Image</label>

                  <div class="col-sm-5">
                   <input type="file" class="form-control pointer" name="image" id="image" placeholder="Academy Name" value="{{$admindata->image}}" >
                  </div>
                  <div class="col-sm-3">
                   <img src="{{ asset('image')}}/{{ Auth::user()->image }}" class="user-image" alt="User Image" style="height: 50px;width: 50px;">
               
              		</div>
                </div>




              </div>
              <!-- /.box-body -->
              <div class="box-footer col-md-offset-4">
                 <button type="submit" class="btn btn-info ">Update</button>
                <a href="{{url('/home')}}"><button type="reset" class="btn btn-danger">Reset</button></a>
               
              </div>
          
            </form>
          </div>
      
        </div>

      </div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script type="text/javascript">
 
</script> 
   
    </section>
    <!-- /.content -->
  </div>

   @endsection 