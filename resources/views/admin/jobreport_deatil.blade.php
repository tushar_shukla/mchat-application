@extends('admin_dash.design')
@section('content')
<div class="content-wrapper" style="min-height: 946px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Job report detail
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('/home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
         <li><a href="{{url('/User/job/Listing')}}">Post job Listing</a></li>
    <li><a href="">Job report detail</a></li>
   
      </ol>
        
   
                  @if(Session::has('flash_message'))
                    <div class="alert alert-success msg" ><span class="glyphicon glyphicon-ok"></span><em>
                      {!! session('flash_message') !!}</em></div> @endif

                      @if(Session::has('flash_ma'))
                    <div class="alert alert-danger msg" ><span class="glyphicon glyphicon-ok"></span><em>
                      {!! session('flash_ma') !!}</em></div> @endif

                     @if(Session::has('flash'))
                    <div class="alert alert-danger msg" ><span class="glyphicon glyphicon-ok"></span><em>
                      {!! session('flash') !!}</em></div> @endif

                      @if(Session::has('edit'))
                    <div class="alert alert-success msg" ><span class="glyphicon glyphicon-ok"></span><em>
                      {!! session('edit') !!}</em></div> @endif
                   
    </section>


   <section class="content">

      <div class="row">
       <div class="col-md-9">
          <div class="nav-tabs-custom">
           <!--  <ul class="nav nav-tabs">
              <li class="active"><a href="#activity" data-toggle="tab">Personal</a></li>
              <li><a href="#timeline" data-toggle="tab">Educational</a></li>
              <li><a href="#settings" data-toggle="tab">Professional</a></li>
              <li><a href="#jobpost" data-toggle="tab">Posted Jobs</a></li>
              <li><a href="#applyjob" data-toggle="tab">Applied Jobs</a></li>
            </ul> -->
            <div class="tab-content">
            
              <!-- /.tab-pane -->
              <div class="" id="">
                <!-- The timeline -->
                   <div class="post">
                  <div class="user-block">
                   
                  </div>
                 
                  <!-- END timeline item -->
                  <!-- timeline item -->
                  <table class="table table-user-information">
                  
                  @foreach($data as $jobdata)
                    <tbody>
                    
                      <tr>
                        <td>Report by</td>
                       
                        <td>{{$jobdata->username}}</td>
                        
                      </tr>
                      <tr>
                        <td>Comment</td>
                       
                        <td>{{$jobdata->comment}}</td>
                      </tr> 

                      <tr>
                        <td>Report date</td>
                       
                        <td>{{$jobdata->reportdate}}</td>
                      </tr>
                       <tr>
                        <td>Job description</td>
                       
                        <td>{{$jobdata->description}}</td>
                      </tr>
                       <tr>
                        <td>Posted job user name</td>
                       
                        <td>{{$jobdata->postjobuser}}</td>
                      </tr>
                   
                  
                     
                    </tbody>
                @endforeach
                    
                  </table>
                 
                </div>

                 
                  
                 
              </div>
          
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
<!-- Modal -->

    </section>
    <!-- /.content -->
  </div>

   @endsection 