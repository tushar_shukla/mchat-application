@extends('admin_dash.design')
@section('content')
<div class="content-wrapper" style="min-height: 946px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Update Password
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Update Password</a></li>
   
      </ol>
        <style type="text/css">
        .msg{
          font-size: 18px;
          text-align: center;
          width: 50%;
          margin: auto;
        }

      </style>
   
       @if(Session::has('flash_message'))
                    <div class="alert alert-success msg" ><span class="glyphicon glyphicon-ok"></span><em>
                      {!! session('flash_message') !!}</em></div> @endif

                      @if(Session::has('flash_ma'))
                    <div class="alert alert-danger msg" ><span class="glyphicon glyphicon-ok"></span><em>
                      {!! session('flash_ma') !!}</em></div> @endif

                     @if(Session::has('flash'))
                    <div class="alert alert-danger msg" ><span class="glyphicon glyphicon-ok"></span><em>
                      {!! session('flash') !!}</em></div> @endif

                      @if(Session::has('edit'))
                    <div class="alert alert-warning msg" ><span class="glyphicon glyphicon-ok"></span><em>
                      {!! session('edit') !!}</em></div> @endif
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row" style="margin-top: 102px;">
     
        <!-- right column -->
        <div class="col-md-8 col-md-offset-2">
          <!-- Horizontal Form -->
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title"></h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" action="{{ url('/change/password') }}" method="POST" id="form" name="form" onsubmit="return check()">
              <input type="hidden" name="_token" value="{{csrf_token()}}">
               <input type="hidden" name="acedamy_code" value="{{session()->get('acedamy_code')}}">
              <div class="box-body">
            

                
         

                <!-- <div class="form-group">
                  <label for="inputEmail3" class="col-sm-3 control-label">Email</label>

                  <div class="col-sm-9">
                    <input type="email" name="email"  class="form-control" id="email" placeholder="Enter Email Address" required="required">
                    
                    
                  </div>
                </div> -->


                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-3 control-label">Current Password</label>

                  <div class="col-sm-9">
                    <input type="password" name="currentpassword"  class="form-control" id="currentpassword" placeholder="Enter Current Password" required="required">
                    
                    
                  </div>
                </div>


                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-3 control-label">New Password</label>

                  <div class="col-sm-9">
                    <input type="password" name="newpassword"  class="form-control" id="password" placeholder="Enter New Password" required="required" minlength="6">
                    
                    
                  </div>
                </div>


                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-3 control-label">Confirm Password</label>

                  <div class="col-sm-9">
                    <input type="password" name="confirmpassword"  class="form-control" id="confirm_password" placeholder="Re Enter New Password" required="required" minlength="6">
                    <span id='message'></span>
                    
                    
                  </div>
                </div>
           
               
                
                </div>
              <!-- /.box-body -->
              <div class="box-footer col-md-offset-4">
                 <button type="submit" class="btn btn-success ">Update</button>
                <button type="reset" class="btn btn-danger">Reset</button>
               
              </div>
          
            </form>
          </div>
      
        </div>

      </div>
      <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

    <script type="text/javascript">
      
       $('#password, #confirm_password').on('keyup', function () {
  if ($('#password').val() == $('#confirm_password').val()) {
    $('#message').html('Password Matching').css('color', 'green');
  } else 
    $('#message').html('Password Not Matching').css('color', 'red');
});
           
    $("#form").submit(function(){
     if($("#password").val()!=$("#confirm_password").val())
     {
         alert("New password And Confirm Password should be same");
         return false;
     }
    })

    </script>
    </section>
    <!-- /.content -->
  </div>

   @endsection