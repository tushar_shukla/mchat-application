<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Binaries
    |--------------------------------------------------------------------------
    |
    | Paths to ffmpeg nad ffprobe binaries
    |
    */

    'binaries' => [
        'ffmpeg'  => env('FFMPEG', 'C:\xampp\htdocs\Smileloft\public\ffmpeg\bin\ffmpeg.exe'),
        'ffprobe' => env('FFPROBE', 'C:\xampp\htdocs\Smileloft\public\ffmpeg\bin\ffprobe.exe')
    ]
];