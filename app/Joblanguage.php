<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Joblanguage extends Model
{
  
protected $table='user_jobpost_language';

protected $fillable=['user_id','job_id','job_language','status','language_position'];


}
