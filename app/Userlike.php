<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Userlike extends Model
{
    
protected $table='user_total_like';

protected $fillable=['user_id','like_user_id','total_like'];


}
