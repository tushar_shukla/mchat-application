<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
 
protected $table='country';

protected $fillable=['Code','Name'];

}
