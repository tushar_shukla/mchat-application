<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Useremployment extends Model
{
    

protected $table='user_employement';

protected $fillable=['user_id','company','designation','fromdate','todate','status','exp'];

}
