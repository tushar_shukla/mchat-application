<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Userpersonal extends Model
{
    

protected $table='user_personal_info';

protected $fillable=['user_id','first_name','middel_name','last_name','dob','gender','meterial_status','weight','height','nationality','pet_love','ca1','ca2','ca3','ccity_id','is_permanent','pa1','pa2','pa2','pa3','pcity_id','image','status','contact','c_country','p_country'];

public function petname()
    {
        return $this->belongsTo(Petsadd::class, 'pet_love','id');
    }

public function national(){

	return $this->belongsTo(Location::class, 'nationality','id');
}

public function city(){

	return $this->belongsTo(Cityname::class, 'ccity_id','id');
}
public function pcity(){

	return $this->belongsTo(Cityname::class, 'pcity_id','id');
}
}
