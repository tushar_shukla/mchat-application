<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contacthelp extends Model
{
  
  protected $table='contact_help';

  protected $fillable=['user_id','message'];


}
