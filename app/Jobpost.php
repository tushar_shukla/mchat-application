<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jobpost extends Model
{
   
protected $table='user_job_post';

protected $fillable=['user_id','user_name','description','nationality','work_type','salery','salery_range','text','voice','video','location','public_holiday','leave_option','if_option_other','chareterstics','start_date','status','published','draft','working_time','trip_home_other','trip_home','leaves','active','make_private','video_thumb'];


}
