<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;
use Session;
use View;
use Validator;
use stdClass;
use App\Userpersonal;
use App\Usereducation;
use App\Useremployment;
use App\Userlanguage;
use App\Uservideo;
use App\Userquote;
use App\Userskill;
use App\Userhobbies;
use App\Userregister;
use App\Joblanguage;
use App\Jobscreening;
use App\Jobskill;
use App\Adminnotification;
use App\Jobpost;
class SearchingController extends Controller
{
  
public function personalsearch(Request $request)
	{
    $searching=$request['searching'];
		$dataall="SELECT chat_user.user_name,group_concat(distinct user_total_like.like_user_id) as likeuser_id,count(distinct user_total_like.like_user_id) as totallike,YEAR(CURRENT_DATE)-YEAR(p.dob) age,p.user_id,p.image,p.first_name,p.middel_name,p.last_name,p.dob,p.gender,p.contact,p.meterial_status,p.weight,p.height,p.ca1,p.ca2,p.ca3,p.c_country,p.is_permanent,p.pa1,p.pa2,p.pa3,p.pa3,p.p_country,p.status,group_concat(DISTINCT hobbies.hobbies) as hobbies,pets.pets_name as petname,c.Name as currentcity,adde.Name as permanentcity,chat_user.user_name as username,chat_user.email as chat_email,group_concat(DISTINCT job.id) as jobid,group_concat(DISTINCT job.description) as job_description,group_concat(DISTINCT job.salery_range) as saleryrange,group_concat(DISTINCT user_employement.company) as usercompany,group_concat( DISTINCT user_employement.designation) as designation,group_concat(DISTINCT user_employement.fromdate) as fromdate, group_concat(DISTINCT user_employement.todate) as todate,group_concat( DISTINCT user_skill.skill_id) as userskill,group_concat( DISTINCT user_skill.language_id) as userskill_language,group_concat( DISTINCT user_skill.lang_staus) as lang_status,group_concat( DISTINCT user_skill.charecter_sticks) as charesticks,group_concat( DISTINCT user_video.id) as videoid,group_concat( DISTINCT user_video.video_url  ORDER BY user_video.status DESC) as video_url,group_concat( DISTINCT user_video.user_video_thumbnail) as user_video_thumb,group_concat( DISTINCT user_video.status ORDER BY user_video.status DESC) as videostatus,country.Code2 as country_flage,COUNT(DISTINCT YEAR(user_employement.todate)) as passjob,SUM(DISTINCT YEAR(user_employement.todate)-YEAR(user_employement.fromdate)) as experience,GROUP_CONCAT(DISTINCT language.language) as language,GROUP_CONCAT(DISTINCT user_language.level) as englishlevel,(avg(rate.discipline)+avg(rate.responsbility)+avg(rate.cleanness_and_tidy)+avg(rate.communication)+avg(rate.patient)+avg(rate.experience)+avg(rate.attitude))/7 as Average_rating,GROUP_CONCAT(DISTINCT rate.comment) as comment,SUM(DISTINCT rate.smile)as smile,SUM(DISTINCT rate.sad) as sad,SUM(DISTINCT rate.laugh) as laugh,group_concat(DISTINCT chat.user_name)as comment_user,group_concat(DISTINCT personal.image) as comment_user_imge,group_concat(DISTINCT user_education.level) as edulevel FROM user_personal_info p LEFT JOIN user_hobbies on p.user_id=user_hobbies.user_id LEFT JOIN hobbies ON hobbies.id=user_hobbies.hobbies_id LEFT JOIN pets on pets.id=p.pet_love LEFT JOIN city c ON c.ID=p.ccity_id LEFT JOIN city adde ON adde.ID=p.pcity_id LEFT JOIN chat_user ON chat_user.id=p.user_id LEFT JOIN user_job_post job ON p.user_id=job.user_id LEFT JOIN user_total_like ON user_total_like.user_id=p.user_id LEFT JOIN user_employement ON user_employement.user_id=p.user_id LEFT JOIN country ON p.c_country=country.Name LEFT JOIN user_skill ON user_skill.user_id=p.user_id LEFT JOIN user_video ON user_video.user_id=p.user_id LEFT JOIN user_language ON p.user_id=user_language.user_id LEFT JOIN language ON language.id=user_language.language_id LEFT JOIN user_total_rating rate ON rate.user_id=p.user_id LEFT JOIN chat_user chat ON chat.id=rate.rating_user_id LEFT JOIN user_personal_info personal ON personal.user_id=rate.rating_user_id LEFT JOIN user_education ON user_education.user_id=p.user_id WHERE chat_user.user_name LIKE '%".$searching."%' OR p.p_country LIKE '%".$searching."%' OR p.first_name LIKE '%".$searching."%' OR p.last_name LIKE '%".$searching."%' OR user_skill.language_id LIKE '%".$searching."%' OR user_education.level LIKE '%".$searching."%' OR user_skill.skill_id LIKE '%".$searching."%' OR user_skill.charecter_sticks LIKE '%".$searching."%' GROUP BY p.user_id,p.first_name,p.middel_name,p.last_name,p.dob,p.gender,p.contact,p.meterial_status,p.weight,p.height,p.ca1,p.ca2,p.ca3,p.c_country,p.is_permanent,p.pa1,p.pa2,p.pa3,p.pa3,p.p_country,p.status,pets.pets_name,c.Name,adde.Name,chat_user.user_name,chat_user.email,user_total_like.user_id,country.Code2,p.image";

        $userdata=DB::select($dataall);
    
           $data=new stdClass;
               $data->status="1";
               $data->message="Searching all personal data";
               $data->data=json_encode($userdata);
               return response()->json($data);



	}



public function jobsearch(Request $request){

  if(!empty($request['searching']))
  {
     $searching=$request['searching'];
  if($request['searching'] == 'Beginners')
  {
    $data="SELECT chat_user.email,group_concat(distinct job_total_like.like_user_id) as likejob_id,count(job_total_like.like_user_id) as joblike,job.id as jobid,job.user_id,chat_user.user_name as postedby,job.user_name,job.description,job.nationality, job.working_time,job.work_type,job.salery_range, job.text,job.voice,job.video,job.video_thumb,job.location,job.public_holiday,job.leave_option,job.if_option_other,job.chareterstics,job.start_date,job.created_at as jobposted_date,job.status,job.published,job.draft,job.active,job.make_private,job.trip_home,job.trip_home_other,job.leaves,job.salery,group_concat(DISTINCT userskill.skills) as userskill,group_concat(DISTINCT userlang.job_language)as joblanguage,group_concat(DISTINCT userlang.language_position)as language_position,group_concat(DISTINCT ques.id) as screening_id ,group_concat(DISTINCT ques.sreening_question)as screing_question,user_personal_info.contact as contact FROM user_job_post job LEFT JOIN user_job_skill userskill ON job.id=userskill.job_id AND job.user_id=userskill.user_id LEFT JOIN user_jobpost_language userlang ON job.id=userlang.job_id AND job.user_id=userlang.user_id LEFT JOIN job_screening_que ques ON job.id=ques.job_id AND job.user_id=ques.user_id LEFT JOIN job_total_like ON job_total_like.user_id=job.user_id LEFT JOIN user_personal_info ON job.user_id=user_personal_info.user_id LEFT JOIN chat_user ON chat_user.id=job.user_id WHERE job.published='1' AND userlang.language_position LIKE '%".$searching."%' GROUP BY job.user_id,job.description,job.nationality, job.working_time,job.work_type,job.salery_range, job.text,job.voice,job.video,job.location,job.public_holiday,job.leave_option,job.if_option_other,job.chareterstics,job.start_date,job.status,job.published,job.trip_home,job.trip_home_other,job.leaves,job.id,job.user_name,user_personal_info.contact,job.draft,job.active,job.make_private,job.salery,chat_user.user_name,job.created_at,chat_user.email,job.video_thumb";


      $jobdata=DB::select($data);
       $dataval=new stdClass;
               $dataval->status="1";
               $dataval->message="job searching data";
               $dataval->data= json_encode($jobdata);
               return response()->json($dataval);
  }
  elseif ($request['searching'] == 'Intermediate')
  {
    $data="SELECT chat_user.email,group_concat(distinct job_total_like.like_user_id) as likejob_id,count(job_total_like.like_user_id) as joblike,job.id as jobid,job.user_id,chat_user.user_name as postedby,job.user_name,job.description,job.nationality, job.working_time,job.work_type,job.salery_range, job.text,job.voice,job.video,job.video_thumb,job.location,job.public_holiday,job.leave_option,job.if_option_other,job.chareterstics,job.start_date,job.created_at as jobposted_date,job.status,job.published,job.draft,job.active,job.make_private,job.trip_home,job.trip_home_other,job.leaves,job.salery,group_concat(DISTINCT userskill.skills) as userskill,group_concat(DISTINCT userlang.job_language)as joblanguage,group_concat(DISTINCT userlang.language_position)as language_position,group_concat(DISTINCT ques.id) as screening_id ,group_concat(DISTINCT ques.sreening_question)as screing_question,user_personal_info.contact as contact FROM user_job_post job LEFT JOIN user_job_skill userskill ON job.id=userskill.job_id AND job.user_id=userskill.user_id LEFT JOIN user_jobpost_language userlang ON job.id=userlang.job_id AND job.user_id=userlang.user_id LEFT JOIN job_screening_que ques ON job.id=ques.job_id AND job.user_id=ques.user_id LEFT JOIN job_total_like ON job_total_like.user_id=job.user_id LEFT JOIN user_personal_info ON job.user_id=user_personal_info.user_id LEFT JOIN chat_user ON chat_user.id=job.user_id WHERE job.published='1' AND (userlang.language_position='Intermediate' OR userlang.language_position='Beginners') GROUP BY job.user_id,job.description,job.nationality, job.working_time,job.work_type,job.salery_range, job.text,job.voice,job.video,job.location,job.public_holiday,job.leave_option,job.if_option_other,job.chareterstics,job.start_date,job.status,job.published,job.trip_home,job.trip_home_other,job.leaves,job.id,job.user_name,user_personal_info.contact,job.draft,job.active,job.make_private,job.salery,chat_user.user_name,job.created_at,chat_user.email,job.video_thumb";


      $jobdata=DB::select($data);
       $dataval=new stdClass;
               $dataval->status="1";
               $dataval->message="job searching data";
               $dataval->data= json_encode($jobdata);
               return response()->json($dataval);
  }
  elseif ($request['searching'] == 'Advanced')
  {
    $data="SELECT chat_user.email,group_concat(distinct job_total_like.like_user_id) as likejob_id,count(job_total_like.like_user_id) as joblike,job.id as jobid,job.user_id,chat_user.user_name as postedby,job.user_name,job.description,job.nationality, job.working_time,job.work_type,job.salery_range, job.text,job.voice,job.video,job.video_thumb,job.location,job.public_holiday,job.leave_option,job.if_option_other,job.chareterstics,job.start_date,job.created_at as jobposted_date,job.status,job.published,job.draft,job.active,job.make_private,job.trip_home,job.trip_home_other,job.leaves,job.salery,group_concat(DISTINCT userskill.skills) as userskill,group_concat(DISTINCT userlang.job_language)as joblanguage,group_concat(DISTINCT userlang.language_position)as language_position,group_concat(DISTINCT ques.id) as screening_id ,group_concat(DISTINCT ques.sreening_question)as screing_question,user_personal_info.contact as contact FROM user_job_post job LEFT JOIN user_job_skill userskill ON job.id=userskill.job_id AND job.user_id=userskill.user_id LEFT JOIN user_jobpost_language userlang ON job.id=userlang.job_id AND job.user_id=userlang.user_id LEFT JOIN job_screening_que ques ON job.id=ques.job_id AND job.user_id=ques.user_id LEFT JOIN job_total_like ON job_total_like.user_id=job.user_id LEFT JOIN user_personal_info ON job.user_id=user_personal_info.user_id LEFT JOIN chat_user ON chat_user.id=job.user_id WHERE job.published='1' AND (userlang.language_position='Intermediate' OR userlang.language_position='Beginners' OR userlang.language_position='Advanced') GROUP BY job.user_id,job.description,job.nationality, job.working_time,job.work_type,job.salery_range, job.text,job.voice,job.video,job.location,job.public_holiday,job.leave_option,job.if_option_other,job.chareterstics,job.start_date,job.status,job.published,job.trip_home,job.trip_home_other,job.leaves,job.id,job.user_name,user_personal_info.contact,job.draft,job.active,job.make_private,job.salery,chat_user.user_name,job.created_at,chat_user.email,job.video_thumb";


      $jobdata=DB::select($data);
       $dataval=new stdClass;
               $dataval->status="1";
               $dataval->message="job searching data";
               $dataval->data= json_encode($jobdata);
               return response()->json($dataval);



  }else{
    $data="SELECT chat_user.email,group_concat(distinct job_total_like.like_user_id) as likejob_id,count(job_total_like.like_user_id) as joblike,job.id as jobid,job.user_id,chat_user.user_name as postedby,job.user_name,job.description,job.nationality, job.working_time,job.work_type,job.salery_range, job.text,job.voice,job.video,job.video_thumb,job.location,job.public_holiday,job.leave_option,job.if_option_other,job.chareterstics,job.start_date,job.created_at as jobposted_date,job.status,job.published,job.draft,job.active,job.make_private,job.trip_home,job.trip_home_other,job.leaves,job.salery,group_concat(DISTINCT userskill.skills) as userskill,group_concat(DISTINCT userlang.job_language)as joblanguage,group_concat(DISTINCT ques.id) as screening_id ,group_concat(DISTINCT ques.sreening_question)as screing_question,user_personal_info.contact as contact FROM user_job_post job LEFT JOIN user_job_skill userskill ON job.id=userskill.job_id AND job.user_id=userskill.user_id LEFT JOIN user_jobpost_language userlang ON job.id=userlang.job_id AND job.user_id=userlang.user_id LEFT JOIN job_screening_que ques ON job.id=ques.job_id AND job.user_id=ques.user_id LEFT JOIN job_total_like ON job_total_like.user_id=job.user_id LEFT JOIN user_personal_info ON job.user_id=user_personal_info.user_id LEFT JOIN chat_user ON chat_user.id=job.user_id WHERE job.published='1' AND job.working_time LIKE '%".$searching."%' OR job.location LIKE '%".$searching."%' OR job.user_name LIKE '%".$searching."%' OR job.description LIKE '%".$searching."%' OR job.salery_range LIKE '%".$searching."%' OR job.work_type LIKE '%".$searching."%' GROUP BY job.user_id,job.description,job.nationality, job.working_time,job.work_type,job.salery_range, job.text,job.voice,job.video,job.location,job.public_holiday,job.leave_option,job.if_option_other,job.chareterstics,job.start_date,job.status,job.published,job.trip_home,job.trip_home_other,job.leaves,job.id,job.user_name,user_personal_info.contact,job.draft,job.active,job.make_private,job.salery,chat_user.user_name,job.created_at,chat_user.email,job.video_thumb";


      $jobdata=DB::select($data);
       $dataval=new stdClass;
               $dataval->status="1";
               $dataval->message="job searching data";
               $dataval->data= json_encode($jobdata);
               return response()->json($dataval);
             }
      }else{
               $dataval->status="0";
               $dataval->message="can not blank searching";
               return response()->json($dataval);
      }
  }
  }





