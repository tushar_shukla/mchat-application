<?php

namespace App\Http\Controllers;

use App\ExcelUpload;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Session;
use View;
use Validator;
use stdClass;
use App\Userpersonal;
use App\Usereducation;
use App\Useremployment;
use App\Userlanguage;
use App\Userskill;
use App\Userhobbies;
use App\Cityname;
use App\country;
use App\Petsadd;
use App\hobbies;
use App\Userregister;

class ExcelUploadController extends Controller
{
 
public function userexcelupload(Request $request)
{
  if($request['uploadfile']>0)
   {
    $target=$request['uploadfile']; 

if(!empty($target))
{
    $filename = $_FILES['uploadfile']['name'];
    $ext1 = pathinfo($filename, PATHINFO_EXTENSION);

if ($ext1 == "csv")
{

$row = 1;
$handle = fopen($target, "r");

if ( $handle!==false)
{
$total=0;
$product_inserted=0;
$c=0;
$duplicate_cont='';

while (($data = fgetcsv($handle, 1000, ",")) !== FALSE)
{
$num = count($data);
$row++;

if($row>2)
{
  /*echo "<pre>";
  print_r($data);*/
/*$username=Userregister::select('email')
    ->where('email',$data[39])
    ->get();
$useremail='';
foreach ($username as $key => $value) {
    $useremail=$value->email;
    }
if($useremail !=$data[39])
 {
    
$status=1;
$insertuser=Userregister::create([
'user_name'       => $data[38],
'email'           => $data[39],                             
'password'         => Hash::make($data[40]),  
'status'          =>$status,

]);

$user_id= $insertuser->id;*/
/*******************pcity***********************/  
$status=1;          
    $parmanentcity = Cityname::select('ID')
    ->where('Name',$data[18])
    ->get();
    $p_city='';
    foreach ($parmanentcity as $key => $value) {
    $p_city=$value->ID;
    break;
    }
  
/*******************ccity***********************/   
    $currentcity = Cityname::select('ID')
    ->where('Name',$data[13])
    ->get();
    $c_city='';
    foreach ($currentcity as $key => $valu) {

    $c_city=$value->ID;
    break;
    }
/*******************petlove***********************/  
        $pet=Petsadd::select('id')
        ->where('pets_name',$data[9])
        ->get();

        $pet_name='';
        foreach ($pet as $key => $value) {
        $pet_name=$value->id;
        }

/*******************Hobbies***********************/  
$hobbies = explode(',', $data[20]);
$totskill=count($hobbies);
$array=array();
    for ($i=0; $i < $totskill ; $i++) { 
        $hobbies_name = hobbies::select('id')
        ->where('hobbies',$hobbies[$i])
        ->get();
        $hobbies_id='';
        foreach ($hobbies_name as $key => $value) {
        $hobbies_id=$value->id;
        array_push($array, $hobbies_id);
        }
    }
$hobbies_name=implode(",", $array); 

             
/*******************if empty p city ***********************/ 
if (empty($p_city))
    { 
        $city=Cityname::create([
        'Name'          => $data[18],
        '_token'        => $request['_token']
        ]);
        $p_city= $city->id;
    }
/**********************if empty c city*************/   

if (empty($c_city))
    { 
        $ccity=Cityname::create([
        'Name'          => $data[13],
        '_token'             => $request['_token']
        ]);
        $c_city= $ccity->id;
    }

/***************if empty pets id***********************/

if (empty($pet_name))
    { 
        $pets=Petsadd::create([
        'pets_name'          => $data[9],
        'status'             => $status,
        '_token'             => $request['_token']
        ]);
        $pet_name= $pets->id;
    }

/****************if empty Hobbies*********************/


$hobbies=Userhobbies::select('user_id')->where('user_id',$data[0])->get();
$hobb='';
foreach ($hobbies as $key => $value) {
  $hobb=$value->user_id;
  break;
} 
$personal=Userpersonal::select('user_id')->where('user_id',$data[0])->get();
$personalid='';
    foreach ($personal as $key => $value) {
      $personalid=$value->user_id;
      break;
    }
 
 $education=Usereducation::select('*')->where('user_id',$data[0])->where('level',$data[24])->get();
 $userid='';
 $level='';
foreach ($education as $key => $val) {
  $userid=$val->user_id;
  $level=$val->level;
  break;
}   

$employee=Useremployment::select('*')->where('user_id',$data[0])->where('company',$data[29])->get();
$empuserid='';
$cname='';
foreach ($employee as $key => $val) {
  $empuserid=$val->user_id;
  $cname=$val->company;
  break;
}

$dataval=Userskill::select('*')->where('user_id',$data[0])->get();
$skilluser_id='';
foreach ($dataval as $key => $value) {
$skilluser_id=$value->user_id;
break;
}
/**********************User Educational****************************/
 $date3 = str_replace('/', '-', $data[27]);
  $edufromdate=date('Y-m-d', strtotime($date3));
  $date4 = str_replace('/', '-', $data[28]);
  $edutodate=date('Y-m-d', strtotime($date4));
$date=date("Y/m/d");
if($level!=$data[24]){

          $education=Usereducation::create([

              'user_id' =>$data[0],
              'level' =>$data[24],
              'degree' =>$data[25],
              'board' =>$data[26],
              'from_year' =>$edufromdate,
              'to_year' =>$edutodate,
              'date_created' =>$date,
              'status' =>$status,

            ]);

}else if($level==$request['level']) {

     $education=Usereducation::wherelevel($data[24])->where('user_id',$data[0])->update([

              'user_id' =>$data[0],
              'level' =>$data[24],
              'degree' =>$data[25],
              'board' =>$data[26],
              'from_year' =>$edufromdate,
              'to_year' =>$edutodate,
              'date_created' =>$date,
              'status' =>$status,


            ]);
} 
/**********************User Employement*********************/
  $date1 = str_replace('/', '-', $data[32]);
  $fromdate=date('Y-m-d', strtotime($date1));
  $date2 = str_replace('/', '-', $data[33]);
  $todate=date('Y-m-d', strtotime($date2));
if($cname !=$data[29]){
        $empdata=Useremployment::create([

          'user_id'     =>$data[0],
          'company'     =>$data[29],
          'designation' =>$data[30],
           'exp' =>$data[31],
          'fromdate'    =>$fromdate,
          'todate'      =>$todate,
          'status'      =>$status,

        ]);
}else if ($cname == $data[29]) {
 
     $empdataupdate=Useremployment::where('company',$data[29])->where('user_id',$data[0])->update([

         'user_id'     =>$data[0],
          'company'     =>$data[29],
          'designation' =>$data[30],
           'exp' =>$data[31],
          'fromdate'    =>$fromdate,
          'todate'      =>$todate,
          'status'      =>$status,

        ]);
}
/**************************User Skill And Language*******************/
if (empty($skilluser_id)) {
          
$skillname = explode(',', $data[34]);
$language = explode(',', $data[36]);
$lang_level = explode(',', $data[37]);
$skillcount=count($skillname);
$languagecount=count($language);

for ($i=0; $i < $skillcount; $i++) { 

  $inserskill=Userskill::create([
              'user_id'          =>$data[0],
              'skill_id'         =>$skillname[$i],
              'language_id'      =>$language[$i],
              'lang_staus'       =>$lang_level[$i],
              'charecter_sticks' =>$data[35],
              'status'           =>$status,
          ]);
      }
}else if (!empty($skilluser_id)) {

  Userskill::whereuser_id($userid)->delete($userid);  
  $updatekill=Userskill::create([
              'user_id'          =>$data[0],
              'skill_id'         =>$data[34],
              'language_id'      =>$data[36],
              'lang_staus'       =>$data[37],
              'charecter_sticks' =>$data[35],
              'status'           =>$status,
          ]);

}
/*******************************User Personal personal****************/        
if(empty($personalid)){
  $date = str_replace('/', '-', $data[4]);
  $dob=date('Y-m-d', strtotime($date));
$insert=Userpersonal::create([
            'user_id'           =>$data[0],
            'first_name'        =>$data[1],
            'middel_name'       =>$data[2],
            'last_name'         =>$data[3],
            'dob'               =>$dob,
            'gender'            =>$data[5],
            'meterial_status'   =>$data[6],
            'weight'            =>$data[7],
            'height'            =>$data[8],
            'pet_love'          =>$pet_name,
            'ca1'               =>$data[10],
            'ca2'               =>$data[11],
            'ca3'               =>$data[12],
            'ccity_id'          =>$c_city,
            'c_country'         =>$data[22],
            'pa1'               =>$data[15],
            'pa2'               =>$data[16],
            'pa3'               =>$data[17],
            'pcity_id'          =>$p_city,
            'p_country'         =>$data[21],
            'contact'           =>$data[19],
            'status'            =>$status,
            'is_permanent'      =>$data[14]
           
            ]);
/*****************user hobbies*************************/
if(empty($hobb)){
            $hobbies = explode(',', $hobbies_name);
           $totskill=count($hobbies);
           for ($i=0; $i < $totskill ; $i++) {   
                $hobbiesdata=Userhobbies::create([
                'user_id' =>$data[0],
                'hobbies_id' =>$hobbies[$i],
                'status'    =>$status,
              ]);
              }
            }   
   
}/*elseif (!empty($personalid)) {
    
    $update=Userpersonal::whereuser_id($personalid)->update([

            'user_id'           =>$data[0],
            'first_name'        =>$data[1],
            'middel_name'       =>$data[2],
            'last_name'         =>$data[3],
            'dob'               =>$data[4],
            'gender'            =>$data[5],
            'meterial_status'   =>$data[6],
            'weight'            =>$data[7],
            'height'            =>$data[8],
            'pet_love'          =>$pet_name,
            'ca1'               =>$data[10],
            'ca2'               =>$data[11],
            'ca3'               =>$data[12],
            'ccity_id'          =>$c_city,
            'c_country'         =>$data[22],
            'pa1'               =>$data[15],
            'pa2'               =>$data[16],
            'pa3'               =>$data[17],
            'pcity_id'          =>$p_city,
            'p_country'         =>$data[21],
            'contact'           =>$data[19],
            'status'            =>$status,
            'is_permanent'      =>$data[14]
           
            ]);

$delete=Userhobbies::whereuser_id($hobb)->delete($hobb);

            $hobbies = explode(',', $hobbies_name);
           $totskill=count($hobbies);
           for ($i=0; $i < $totskill ; $i++) { 
                $hobbiesdata=Userhobbies::create([
                'user_id' =>$data[0],
                'hobbies_id' =>$hobbies[$i],
                'status'    =>$status,
              ]);
              }
            


}*/ 
    
}
        
}
if($hobbiesdata==true)
{
Session::flash('edit','File Upload successfully');
return redirect('/User/Listing/Detail');
}else{
  Session::flash('flash_ma','Some thing went wronge please check details');
  return redirect('/User/Listing/Detail');
}
}
          
} 
else{
    Session::flash('flash_ma','Please Upload csv excel file formate');
    return redirect('/User/Listing/Detail');
    }
  }
 }
}



public function downloaduser(){
  $filename = "User Record Sample";     
$file_ending = "xls";
header("Content-Type: application/xls");
header("Content-Disposition: attachment; filename=$filename.xls");
header("Pragma: no-cache"); 
header("Expires: 0");
$sep = "\t";

$schema_insert = "UserID\tFname\tMname\tLname\tDOBirth\tGender\tMarital_Status\tWeight\tHeight\tPetLove\tCurrentA1\tCurrentA2\tCurrentA3\tCcId\tIsChecked\tPermanentA1\tPermanentA2\tPermanentA3\tPcId\tContact\tHobbiesID\tp_country\tc_country\tuser_id\tlevel\tdegree\tboard\tfrom_year\tto_year\tcompany\tdesignation\texp\tfromdate\ttodate\tskill_id\tcharecter\tlanguage\tlang_status\n";

         /*   $userid='';
             $schema_insert .= $userid.$sep;
            $fname='first_name';
            $schema_insert .= $fname.$sep;
            $mname='last_name';
             $schema_insert .= $mname.$sep;
            $lname='email';
             $schema_insert .= $lname.$sep;
            $dobbirth='father_name';
             $schema_insert .= $dobbirth.$sep;
            $gender='mobile';
             $schema_insert .= $gender.$sep;
            $merital_status='class_name';
             $schema_insert .= $merital_status.$sep;
            $weight='sec_name';
             $schema_insert .= $weight.$sep;
            $height='group_name';
             $schema_insert .= $height.$sep;
            $petlove='roll_number';
             $schema_insert .= $petlove.$sep;
            $currenta1='gender';
             $schema_insert .= $currenta1.$sep;
            $currenta2='dob';
             $schema_insert .= $currenta2.$sep;
            $currenta3='fees';
             $schema_insert .= $currenta3.$sep;
             $ccid='marks_previous';
             $schema_insert .= $ccid.$sep;
            $IsChecked='address';
             $schema_insert .= $IsChecked.$sep;
             $PermanentA1='marks_previous';
             $schema_insert .= $PermanentA1.$sep;
             $PermanentA2='marks_previous';
             $schema_insert .= $PermanentA2.$sep;
             $PermanentA3='marks_previous';
             $schema_insert .= $PermanentA3.$sep;
             $PcId='marks_previous';
             $schema_insert .= $PcId.$sep;
             $Contact='marks_previous';
             $schema_insert .= $Contact.$sep;
             $HobbiesID='marks_previous';
             $schema_insert .= $HobbiesID.$sep;
             $p_country='marks_previous';
             $schema_insert .= $p_country.$sep;
             $c_country='marks_previous';
             $schema_insert .= $c_country.$sep;
            $schema_insert .= "\r\n";
    */
echo "$schema_insert";
  /*  $schema_insert = str_replace($sep."$", "", $schema_insert);
    $schema_insert = preg_replace("/\r\n|\n\r|\n|\r/", " ", $schema_insert);
    $schema_insert .= "\t";
    print(trim($schema_insert));
    print "\n";*/


}


public function mainuser(){

$filename = "All User List";     
$file_ending = "xls";
header("Content-Type: application/xls");
header("Content-Disposition: attachment; filename=$filename.xls");
header("Pragma: no-cache"); 
header("Expires: 0");
$sep = "\t";
$userdata=Userregister::select('*')->get();
$schema_insert = "UserID\tUser Name\tEmail\n";
foreach ($userdata as $key => $value) {
            $userid=$value->id;
             $schema_insert .= $userid.$sep;
            $username=$value->user_name;
            $schema_insert .= $username.$sep;
            $email=$value->email;
             $schema_insert .= $email.$sep;
            $schema_insert .= "\r\n";
  } 
echo "$schema_insert";
  /*  $schema_insert = str_replace($sep."$", "", $schema_insert);
    $schema_insert = preg_replace("/\r\n|\n\r|\n|\r/", " ", $schema_insert);
    $schema_insert .= "\t";
    print(trim($schema_insert));
    print "\n";*/
}


}
