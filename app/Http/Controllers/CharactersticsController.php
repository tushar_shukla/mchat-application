<?php

namespace App\Http\Controllers;

use App\Characterstics;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;
use Validator;
use stdClass;

class CharactersticsController extends Controller
{
   
    public function index()
    {
        $data=Characterstics::select('*')->get();
        return view('admin.add_characteristics',compact('data'));
    }
   
    public function store(Request $request)
    {
    
        $characterstics=$request['characterstics'];
        $status=1;
        $token=$request['_token'];
    $select=Characterstics::select('*')->where('characterstics',$characterstics)->get();
    $data='';
    foreach ($select as $key => $value) {
            
            $data=$value->characterstics;
            break;
    }

if ($data != $characterstics) {

    if (!empty($characterstics)) {

            $insert=Characterstics::create([

                    'characterstics'=>$characterstics,
                    'status'=>$status,
                    '_token'=>$token,
              ]);

                if ($insert==true) {
                   Session::flash('flash_message','Characterstics Added Succsessfully');
                            return redirect('/Add/Characteristics');
                }
       }else{

            Session::flash('flash_ma','Can not empty Characterstics field');
            return redirect('/Add/Characteristics');

        }
    
}else{
    Session::flash('flash_ma','This Characterstics Already Exist');
            return redirect('/Add/Characteristics');
}
     

    }


   
    public function edit(Request $request,$id)
    {
        $data=Characterstics::select('*')->whereid($id)->first();
        return view('admin.update_characterstics',compact('data'));
    }

    
    public function update(Request $request,$id)
    {
        $characterstics=$request['characterstics'];
        $status=1;
        $token=$request['_token'];
            $update=Characterstics::whereid($id)->update([

                    'characterstics'=>$characterstics,
                    'status'=>$status,
                    '_token'=>$token,
              ]);

                if ($update==true) {
                   Session::flash('flash_message','Characterstics Update Succsessfully');
                    return redirect('/Add/Characteristics');
                }


    }

 
    public function destroy($id)
    {
      Characterstics::whereid($id)->delete($id); 
      Session::flash('flash_ma','Characterstics Deleted Succsessfully');
        return redirect('/Add/Characteristics'); 
    }


/************************charecterstics for api***********/


public function charactersticsapi(Request $request){
   
        $data=Characterstics::select('*')->get();
        $obj = new stdClass();

        if($data==true){
            $obj->status="1";
            $obj->message="Characterstics List";
            $obj->data=$data;    

        }else{

             $obj->status="1";
             $obj->message="Characterstics not found";
        }
        return json_encode($obj);

}



}
