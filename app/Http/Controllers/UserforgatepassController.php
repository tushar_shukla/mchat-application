<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Session;
use View;
use Validator;
use Mail;
use App\Userregister;
use App\Userforgatepass;
use stdClass;
class UserforgatepassController extends Controller
{


public function send_email_otp(Request $request){
    
				$random =rand(1000*45, 10000*98);
				$email=$request['email'];
		$data=Userregister::select('email')->where('email',$email)->get();

		$user='';
		foreach ($data as $key => $value) {
					$user=$value->email;

					break;
		}
		$stdClass=new stdClass;
	if(!empty($request['email'])){
		if ($user ==$request['email']){

			$usersubmit=Userforgatepass::create([
                    'email'         => $request['email'],               
                    'otp'				=>$random
                    
                ]);
			 $useremail= $usersubmit->email;
			 $userotp=$usersubmit->otp;
			 if(!empty($useremail) && !empty($userotp))
			 {
	            $to      = $useremail;
				$subject = 'Mchat-Password Recovery';
				$message = 'Dear User,'."\r\n".'your One-Time-Password to reset password is'.' '. $userotp;
				$headers = 'From: vuongdaonghe@yahoo.com' . "\r\n" .
				    'Reply-To: vuongdaonghe@yahoo.com' . "\r\n" .
				    'X-Mailer: PHP/' . phpversion();

				mail($to, $subject, $message, $headers);
				
				/*return response()->json("{message :OTP sent on your registerd Email Please check your email}");*/
			   $stdClass->status="1";
		       $stdClass->message="OTP sent on your registerd Email Please check your email";
		       return response()->json($stdClass);
				
			}else{

				//return response()->json("{message :Mail Not send some problem please try again}");
				$stdClass->status="0";
		       $stdClass->message="Mail Not send some problem please try again";
		       return response()->json($stdClass);
				}
			}
			else{
				//return response()->json("{message :This Email not found our record <br>Please Inter valid email'}");
				$stdClass->status="0";
		       $stdClass->message="This Email is not found in our record Please Inter valid email";
		       return response()->json($stdClass);
				}
	
		   }else{
		   		//return response()->json("{message :Can not blank email'}");
		   			$stdClass->status="0";
		       $stdClass->message="Can not blank email";
		       return response()->json($stdClass);
				
				}
}





public function reset_password(Request $request){

	$results = Userregister::where('email',$request['email'] )->get();

 	$otpverification = Userforgatepass::where('email',$request['email'] )->get();


 			$otpvalue='';
 			$email='';
 			$otpid='';
 		foreach ($otpverification as $key => $value) {

 			$otpvalue= $value->otp;
 			$email= $value->email;
 			$otpid= $value->id;
 			break;
 			
 		}

 		  $username='';
          $userpassword='';	
          $id='';
		foreach ($results as $key => $val) {
				$userpassword= $val->password;
				$username= $val->email;
				$id= $val->id;
					break;
      		}
      	$stdClass=new stdClass;

	if ($email==$request['email'] && $otpvalue==$request['otp'])
      		{
      			if($username==$request['email']){

      						$update=Userregister::whereid($id)->update([

			              'email'         => $request['email'],
			              'password'          => Hash::make($request['confirm_pass']),
			              
					        ]);

					        if($update==true){

					        	Userforgatepass::whereid($otpid)->delete($otpid);
 	 							//return response()->json("{message :Your password has been changed successfully Please Login'}");
 	 		   $stdClass->status="1";
		       $stdClass->message="Your password has been changed successfully Please Login";
		       return response()->json($stdClass);
					        }

      					}else{

      						//return response()->json("{message :Something went wrong please check input data'}");
      			$stdClass->status="0";
		       $stdClass->message="Something went wrong please check input data";
		       return response()->json($stdClass);
      					
      					}
			      		}else{

			      			//return response()->json("{message :Your Email Or OTP does Not match'}");
			      			$stdClass->status="0";
		                 $stdClass->message="Your Email Or OTP does Not match";
		                return response()->json($stdClass);
			      			
			      		}

				}
					        


}
