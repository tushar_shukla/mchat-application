<?php

namespace App\Http\Controllers;

use App\Applyjob;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;
use View;
use Validator;
use App\Applyjobscreen;
use App\Jobpost;
use stdClass;
use App\Userregister;
use VideoThumbnail;

class ApplyjobController extends Controller
{
    

public function applyjob(Request $request){
            $status=1;
            $video=$request['user_video'];  
            $answers=$request['answer'];
            $question=$request['question_id'];
            $apply_user=$request['apply_user_id'];
        //$videopath = "job_ans_video/".TIME().".mp4";
          $validation=Applyjobscreen::select('*')->where('question_id',$question)->where('apply_user_id',$apply_user)->get();
          $questionid='';
          $applyuser='';
          foreach ($validation as $key => $value) {
                  
                  $questionid=$value->question_id;
                  $applyuser=$value->apply_user_id;
                  break;
          }
if ($questionid !=$question && $applyuser !=$apply_user) {
 
 $query="SELECT chat_user.firebase_token FROM chat_user LEFT JOIN user_job_post ON user_job_post.user_id=chat_user.id LEFT JOIN job_screening_que ON job_screening_que.job_id=user_job_post.id AND job_screening_que.user_id=chat_user.id WHERE job_screening_que.id='$question'";
            $userdata=DB::select($query);
            foreach ($userdata as $key => $value) {
                  
                  $token=$value->firebase_token;
                  break;
            }

      $likeusername=Userregister::select('user_name')->where('id',$apply_user)->get();
        foreach ($likeusername as $key => $val) {
            $username=$val->user_name;
            break;
        }

         //$username='';
         //$token='';
         //echo $token;
          $title="MChat";
        $message=$username." apply your posted job";
        /*echo $message;
         die();*/

    if(!empty($request['apply_user_id']) && !empty($request['user_video'])) {
        $storageUrl=public_path('job_ans_video/apply_thumbnail');
         $filename=time().".png"; 
          $videoname = "job_ans_video/".time().'.'.request()->user_video->getClientOriginalExtension();
          request()->user_video->move(public_path('job_ans_video'), $videoname);
         $homevideo=public_path($videoname);
       
        VideoThumbnail::createThumbnail($homevideo,$storageUrl,$filename,2,640,480);
   

                     $applyjob=Applyjobscreen::create([
                        
                        'apply_user_id'     => $request['apply_user_id'],
                        'question_id'       => $request['question_id'],
                        'screening_ans'     => $videoname,
                        'apply_thumbnail'   => $filename,
                        'given_ans'         => $answers,
                        'status'            => $status,

                    ]);
                 

                    $newmsg= new stdClass;
                 if ($applyjob==true) {

                     if ($token !='' && $message !='') {


                           $msg=$this->notification($token,$title,$message);
                        }

                     $newmsg->status="1";
                     $newmsg->message="Job Apply successfully";
                 }else{

                        $newmsg->status="0";
                        $newmsg->message="Job not Apply please try again";
                 }
                 return response()->json($newmsg);
        }else{
                $newmsg= new stdClass;
                $newmsg->status="0";
                $newmsg->message="Job not Apply please try again";
                  return response()->json($newmsg);
        }




}else{
                $newmsg= new stdClass;
                $newmsg->status="2";
                $newmsg->message="You have alerdy apply this job";
                  return response()->json($newmsg);
}
           
 
               
}

/*********************job apply notification code*********************************/
public function notification($token, $title,$message)
    {
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
        $token=$token;
    
        $message=$message;

        $notification = [
            'title' => $title,
            'message' => $message,
            'sound' => true,
        ];
        
        $extraNotificationData = ["message" => $notification,"moredata" =>'dd'];

        $fcmNotification = [
            //'registration_ids' => $tokenList, //multple token array
            'to'        => $token, //single token
            'notification' => $notification,
            'data' => $extraNotificationData
        ];

        $headers = [
            'Authorization: key=AIzaSyAuqmMsfQtmxrwoujo5qcu5Ix5FQG-4TEU',
            'Content-Type: application/json'
        ];


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        
        /*echo json_encode($fcmNotification);
        die();*/
        $result = curl_exec($ch);
         if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
        //dd($token);
        /*dd($notification);
        die();*/
        return $result;
    }
/***********************end notification*********************/

public function jobpublish(Request $request){

       $jobid=$request['job_id'];
       $published=$request['published'];
       $draff=$request['draff'];  
       $active=$request['active'];  
       $make_private=$request['make_private'];  
    
    
    if ($published==1) {
        $publish=1;
        $draffdata=0;
        $activedata=0;
        $private=0;
        $update=Jobpost::where('id',$jobid)->update([

            'published'     =>$publish,
            'draft'         =>$draffdata,
            'active'        =>$activedata,
            'make_private'  =>$private,
       ]);
       $important=new stdClass;
       if ($update==true) {
          $important->status="1";
          $important->message="your job successfully published";
       }else{
            $important->status="0";
            $important->message="your job not published";
       }

       return response()->json($important);

    }elseif ($draff==1) {
        $publish=0;
        $draffdata=1;
        $activedata=0;
        $private=0;
       $update=Jobpost::where('id',$jobid)->update([

            'published'  =>$publish,
            'draft'      =>$draffdata,
            'active'        =>$activedata,
            'make_private'  =>$private,
       ]);


         $important=new stdClass;
       if ($update==true) {
          $important->status="1";
          $important->message="your job successfully in draff";
       }else{
            $important->status="0";
            $important->message="your job not save in draff";
       }

       return response()->json($important);


    }elseif ($active==1) {
         $publish=0;
        $draffdata=0;
        $activedata=1;
        $private=0;
       $update=Jobpost::where('id',$jobid)->update([

            'published'  =>$publish,
            'draft'      =>$draffdata,
            'active'        =>$activedata,
            'make_private'  =>$private,
       ]);

       $important=new stdClass;
       if ($update==true) {
          $important->status="1";
          $important->message="your job successfully active";
       }else{
            $important->status="0";
            $important->message="your job not active";
       }

       return response()->json($important);

    }elseif ($make_private==1) {
        $publish=0;
        $draffdata=0;
        $activedata=0;
        $private=1;
       $update=Jobpost::where('id',$jobid)->update([

            'published'  =>$publish,
            'draft'      =>$draffdata,
            'active'        =>$activedata,
            'make_private'  =>$private,
       ]);

       $important=new stdClass;
       if ($update==true) {
          $important->status="1";
          $important->message="your job successfully in make private";
       }else{
            $important->status="0";
            $important->message="your job not save in make private";
       }

       return response()->json($important);
    }else{
            $important=new stdClass;
       
            $important->status="0";
            $important->message="somethinw went wrong please try again";
     

       return response()->json($important);
}

}
/*********************************apply user data***************************/
public function applyjobuserlist(Request $request){
    $job_id=$request['job_id'];
   $user_id=$request['user_id'];
if(!empty($job_id) && !empty($user_id)){
  
   $data="SELECT chat_user.id as userid,chat_user.user_name,job_screening_que.sreening_question,apply.question_id,apply.screening_ans,apply.apply_thumbnail,apply.given_ans from screen_job_apply apply LEFT JOIN job_screening_que ON job_screening_que.id=apply.question_id LEFT JOIN user_job_post ON job_screening_que.job_id=user_job_post.id LEFT JOIN chat_user ON chat_user.id=apply.apply_user_id WHERE user_job_post.id='$job_id' AND apply.apply_user_id='$user_id'";
   //apply.question_id='$job_id' AND apply.apply_user_id='$user_id'
/*user_job_post.id*/
  $alluser=DB::select($data);
  $newdata= new stdClass;

if($alluser==true){
    $newdata->status="1";
    $newdata->message="data all fetch";
    $newdata->data=$alluser;
}else{
     $newdata->status="0";
     $newdata->message="data not found";
}
return json_encode($newdata);

}else if (!empty($job_id)) {
  
  $data="SELECT chat_user.id as userid,chat_user.user_name,job_screening_que.sreening_question,apply.question_id,apply.screening_ans,apply.apply_thumbnail,apply.given_ans from screen_job_apply apply LEFT JOIN job_screening_que ON job_screening_que.id=apply.question_id LEFT JOIN user_job_post ON job_screening_que.job_id=user_job_post.id LEFT JOIN chat_user ON chat_user.id=apply.apply_user_id WHERE user_job_post.id='$job_id'";
//apply.question_id
  $alluser=DB::select($data);
  $newdata= new stdClass;

if($alluser==true){
    $newdata->status="1";
    $newdata->message="data all fetch";
    $newdata->data=$alluser;
}else{
     $newdata->status="0";
    $newdata->message="data not found ";
}
return json_encode($newdata);
}else if($user_id !='') {
/*echo"dfshjksdhksd";
  die();*/
    $data="SELECT chat_user.id as userid,chat_user.user_name,job_screening_que.sreening_question,apply.question_id,apply.screening_ans,apply.apply_thumbnail,apply.given_ans from screen_job_apply apply LEFT JOIN job_screening_que ON job_screening_que.id=apply.question_id LEFT JOIN user_job_post ON job_screening_que.job_id=user_job_post.id LEFT JOIN chat_user ON chat_user.id=apply.apply_user_id WHERE apply.apply_user_id='$user_id'";

  $alluser=DB::select($data);
  $newdata= new stdClass;

if($alluser==true){
    $newdata->status="1";
    $newdata->message="data all fetch";
    $newdata->data=$alluser;
}else{
     $newdata->status="0";
    $newdata->message="error";
}
return json_encode($newdata);
}else{
      $newdata= new stdClass;
     $newdata->status="0";
    $newdata->message="can not blank job id";
    return response()->json($newdata);
}

}


public function userapplyjoblist(Request $request)
{

        $applydata= new stdClass;
      if(!empty($request['login_user_id']))
      {
        $login_user_id=$request['login_user_id'];
          $query="SELECT user_job_post.user_name as job_title,user_job_post.id as job_id,DATE(user_job_post.created_at) as job_posted_date,DATE(screen_job_apply.created_at) as job_apply_date FROM user_job_post INNER JOIN job_screening_que ON job_screening_que.job_id=user_job_post.id INNER JOIN screen_job_apply ON screen_job_apply.question_id=job_screening_que.id WHERE screen_job_apply.apply_user_id='$login_user_id'";

             $data=DB::select($query);
            if($data==true)
            {
              $applydata->status="1";
              $applydata->message="Job title,job posted date and apply date";
              $applydata->applylist=$data;
            }else{

                 $applydata->status="0";
                 $applydata->message="Job title not found ";
            }
            return response()->json($applydata);

      }
      else{

          $applydata->status="0";
          $applydata->message="Can not blank login user id ";
          return response()->json($applydata);
      }

}


}
