<?php

namespace App\Http\Controllers;

use App\Updateprofile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Session;
use View;
use Validator;

class UpdateprofileController extends Controller
{
   
    public function index()
    {
        $admindata=Updateprofile::select('*')->first();
        
        return view('admin.update_profile',compact('admindata'));

    }



    public function update_profile($id,Request $request){

           $validation=$this->validate($request, [
            'name' => 'required|max:90',
            'email' => 'required',
            'mobile' => 'required',
            ]);

           if($validation==true && empty($request['image'])){

            Updateprofile::whereid($id)->update([

                    'name'         => $request['name'],
                    'email'          => $request['email'],
                    'mobile'        => $request['mobile'],
                    'city'        => $request['city'],
                    'state'        => $request['state'],
                    'country'        => $request['country'],
                    'address'        => $request['address'],
            ]);
            Session::flash('edit',' Profile Update successfully');
            return redirect('/Admin/Update/Profile/');
           }elseif ($validation==true &&!empty($request['image'])) {

            $imageName = time().'.'.request()->image->getClientOriginalExtension();
             request()->image->move(public_path('image'), $imageName);


             Updateprofile::whereid($id)->update([

                    'image'              => $imageName,
                    'name'         => $request['name'],
                    'email'          => $request['email'],
                    'mobile'        => $request['mobile'],
                    'city'        => $request['city'],
                    'state'        => $request['state'],
                    'country'        => $request['country'],
                    'address'        => $request['address'],

        ]);
             Session::flash('edit',' Profile Update successfully');
            return redirect('/Admin/Update/Profile/');
               
           }else{

            Session::flash('flash_ma','Profile not updated check the details entered and try again');
                return redirect('/Admin/Update/Profile/');
           }


    }


}
