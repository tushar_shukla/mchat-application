<?php

namespace App\Http\Controllers;

use App\Changepassword;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Hash;
use Session;
use Auth;

class ChangepasswordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        return view('admin.updatepassword');

    }

    
    public function store(Request $request)
    {
     $email=Auth::user()->email;
        $results = Changepassword::where('email',$email )->get();
         $username='';
          $userpassword=''; 
          $id='';
        foreach ($results as $key => $val) {
                $userpassword= $val->password;
                $username= $val->email;
                $id= $val->id;
                    break;
            }
            $isOk  = password_verify($request['currentpassword'], $userpassword);

            if ($username==$email)
            {
                    if($isOk==true){

                        if($username==$email && $isOk=='true'){

                            Changepassword::whereid($id)->update([
                                'password'          => Hash::make($request['confirmpassword']),
                                'remember_token'    => $request['_token']
                            ]);

                            Session::flash('flash_message','Your password has been changed successfully ');
                            return back();

                        }else{

                        Session::flash('flash_ma','Something went wrong your password not update');
                        return back();
                        }

                    }else{

                        Session::flash('flash_ma','Your current Password Does not match Our Record');
                        return back();
                    }
            }else{

                Session::flash('flash_ma','Your Email Does Not match Our Record');
               return back();
            }



    }

    
}
