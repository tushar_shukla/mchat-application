<?php

namespace App\Http\Controllers;

use App\Contactus;
use Illuminate\Http\Request;
use Session;
use stdClass;
use App\Contacthelp;
use Illuminate\Support\Facades\DB;

class ContactusController extends Controller
{
  
public function contact(){
    $contact=Contactus::select('*')->where('id','1')->first();
    $data=DB::table('contact_help')
            ->leftjoin('chat_user','chat_user.id','=','contact_help.user_id')
            ->select('contact_help.*','chat_user.user_name')
            ->get();
    return view('admin.contact_us',compact('contact','data'));

} 

public function contact_us(Request $req){
$contact=Contactus::select('*')->whereid('1')->get();
$id='';
foreach ($contact as $key => $value) {
    $id=$value->id;
}
if (!empty($req['email']) && !empty($req['phone'])) {
    
 Contactus::whereid($id)->update([

        'email' =>$req['email'],
        'phone' =>$req['phone'],
        '_token' =>$req['_token'],

    ]);
Session::flash('flash_message','Contact Update successfully');
return redirect('/Update/Contact/Us');
}
else{
Session::flash('flash_ma','Can Not Blank Email And Phone');
return redirect('/Update/Contact/Us');
}
   

}


/**********************contact Us To Api**************************/

public function getcontact(){

   $contact=Contactus::select('*')->get();
     $data=new stdClass();
       if($contact==true)
            {
                $data->status="1";
                $data->message="Succsessfully";
                $data->data=json_encode($contact);
                       
            }
            else
            {
                $data->status="0";
                $data->message="Not Succsessfully";
            
            }
       return response()->json($data);

}


/*************contact help from user to admin api***********************/


public function contacthelp(Request $request){

if(!empty($request['message'])){
    $help=Contacthelp::create([

        'user_id'   =>$request['user_id'],
        'message'   =>$request['message'],

    ]);

    $data=new stdClass;
     if($help==true)
            {
                $data->status="1";
                $data->message="Succsessfully";
            }
            else
            {
                $data->status="0";
                $data->message="Not Succsessfully";
            }
    return response()->json($data);
}else{
        
     return response()->json('Can Not Empty Comment Field');
}

}

}
