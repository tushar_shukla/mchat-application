<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;
use Session;
use View;
use Validator;
use stdClass;
use App\Userpersonal;
use App\Usereducation;
use App\Useremployment;
use App\Userlanguage;
use App\Uservideo;
use App\Userquote;
use App\Userskill;
use App\Userhobbies;

class UpdateprofileapiController extends Controller
{

  public function update_data(Request $request){

    $userid='1';

    $userdata = DB::table('chat_user_personal_info')
            ->leftjoin('chat_user_education', 'chat_user_personal_info.user_id', '=', 'chat_user_education.user_id')

            ->leftjoin('chat_user_employement', 'chat_user_personal_info.user_id', '=', 'chat_user_employement.user_id')

            ->select('chat_user_personal_info.*', 'chat_user_education.degree','chat_user_education.bord', 'chat_user_employement.company','chat_user_employement.designation','chat_user_employement.fromdate','chat_user_employement.todate')
            ->where('chat_user_personal_info.user_id',$userid)
            ->get();



            return response()->json($userdata);

      }

/***********************Upadte Personal info By user id******************************/

public function update_personal_info(Request $request){   
  
        $user_id=$request['UserID'];
        $fname=$request['Fname'];
        $mname=$request['Mname'];
        $lname=$request['Lname'];
        $dob=$request['DOBirth'];
        $gender=$request['Gender'];
        $merried=$request['Marital_Status'];
        $waight=$request['Weight'];
        $hight=$request['Height'];
        $petlove=$request['PetLove'];
        $ca1=$request['CurrentA1'];
        $ca2=$request['CurrentA2'];
        $ca3=$request['CurrentA3'];
        $ccid=$request['CcId'];
        $ischek=$request['IsChecked'];
        $pa1=$request['PermanentA1'];
        $pa2=$request['PermanentA2'];
        $pa3=$request['PermanentA3'];
        $pcid=$request['PcId'];
        $contact=$request['Contact'];
        $hobbiesname=$request['HobbiesID'];
        $p_country=$request['p_country'];
        $c_country=$request['c_country'];
        

 $hobbies=Userhobbies::select('user_id')->where('user_id',$user_id)->get();
$hobb='';
foreach ($hobbies as $key => $value) {
  $hobb=$value->user_id;
  break;
} 


$personal=Userpersonal::select('user_id')->where('user_id',$user_id)->get();
$personalid='';
    foreach ($personal as $key => $value) {
      $personalid=$value->user_id;
      break;
    }
    
    $status=1;
/***************personal****************/        
if(empty($personalid)){
  
 
   $insert=Userpersonal::create([
            'user_id'           =>$user_id,
            'first_name'        =>$fname,
            'middel_name'       =>$mname,
            'last_name'         =>$lname,
            'dob'               =>$dob,
            'gender'            =>$gender,
            'meterial_status'   =>$merried,
            'weight'            =>$waight,
            'height'            =>$hight,
            'pet_love'          =>$petlove,
            'ca1'               =>$ca1,
            'ca2'               =>$ca2,
            'ca3'               =>$ca3,
            'ccity_id'          =>$ccid,
            'c_country'         =>$c_country,
            'pa1'               =>$pa1,
            'pa2'               =>$pa2,
            'pa3'               =>$pa3,
            'pcity_id'          =>$pcid,
            'p_country'         =>$p_country,
            'contact'           =>$contact,
            'status'            =>$status,
            'is_permanent'      =>$ischek
           
            ]);
     
   if(empty($hobb)){
            $rr1= str_replace('[','',$hobbiesname);
            $rr= str_replace(']','',$rr1);
            $hobbies = explode(',', $rr);
           $totskill=count($hobbies);
           for ($i=0; $i < $totskill ; $i++) { 
                $hobbiesdata=Userhobbies::create([
                'user_id' =>$user_id,
                'hobbies_id' =>$hobbies[$i],
              ]);
              }
            }
      
   
       
       $inserted=new stdClass();
       if($hobbiesdata==true)
            {
            $inserted->status="1";
            $inserted->message="Personal Info Inserted";
            //$updated->data=json_encode($data); 
            
            }
            else
            {
            $inserted->status="0";
            $inserted->message="Personal Info Not Inserted";
            
            }
       return response()->json($inserted);
       
   
}elseif (!empty($personalid)) {
    
    $update=Userpersonal::whereuser_id($personalid)->update([

            'user_id'           =>$user_id,
            'first_name'        =>$fname,
            'middel_name'       =>$mname,
            'last_name'         =>$lname,
            'dob'               =>$dob,
            'gender'            =>$gender,
            'meterial_status'   =>$merried,
            'weight'            =>$waight,
            'height'            =>$hight,
            'pet_love'          =>$petlove,
            'ca1'               =>$ca1,
            'ca2'               =>$ca2,
            'ca3'               =>$ca3,
            'ccity_id'          =>$ccid,
            'c_country'         =>$c_country,
            'pa1'               =>$pa1,
            'pa2'               =>$pa2,
            'pa3'               =>$pa3,
            'pcity_id'          =>$pcid,
            'p_country'         =>$p_country,
            'contact'           =>$contact,
            'status'            =>$status,
            'is_permanent'      =>$ischek

    ]);

$delete=Userhobbies::whereuser_id($hobb)->delete($hobb);

            $rr1= str_replace('[','',$hobbiesname);
            $rr= str_replace(']','',$rr1);
            $hobbies = explode(',', $rr);
           $totskill=count($hobbies);
           for ($i=0; $i < $totskill ; $i++) { 
                $hobbiesdata=Userhobbies::create([
                'user_id' =>$user_id,
                'hobbies_id' =>$hobbies[$i],
              ]);
              }


  
       $updated=new stdClass();
       if($hobbiesdata==true)
            {
            $updated->status="1";
            $updated->message="Personal Info Updated";
            //$updated->data=json_encode($data); 
            
            }
            else
            {
            $updated->status="0";
            $updated->message="Personal Info Not Updated";
            
            }
       return response()->json($updated);
   
  
}

}
/******************update profile pic*************************/
/*______________________________________________________________________________*/

public function showimage(Request $request){
$user_id=$request['user_id'];
$url='http://18.220.35.110/UAT1/chatprofile/public/';
$image=Userpersonal::select('image','id')->where('user_id',$user_id)->get();
$id='';
foreach ($image as $key => $value) {
  $id=$value->id;
}
if (!empty($id)) {
 $mainurl=$url.$image;
 $data=new stdClass();
       if($image==true)
            {
            $data->status="1";
            $data->message="Personal Info Updated";
            $data->data=json_encode($image); 
            //$data->data=json_encode($url); 
            
            }
            else
            {
            $data->status="0";
            $data->message="Personal Info Not Updated";
            
            }
return response()->json($data);
}else{
              $data=new stdClass();
             $data->status="0";
            $data->message="User not Found";
            return response()->json($data);
}



}

public function image_update(Request $request){
  $personal=Userpersonal::select('*')->where('user_id',$request['user_id'])->get();
  $personalid='';
  $personalimage='';
  $flag=0;
    foreach ($personal as $key => $value) {
      $personalid=$value->user_id;
      $personalimage=$value->image;
      $flag=1;
      break;
    }


 $path = "image/".TIME().".png";
    $actualpath =  public_path('')."/".$path;
   $status=1; 

if($flag==0){
    $images= $request['image'];
   $insert=Userpersonal::create([

            'user_id'           =>$request['user_id'],
            'image'             =>$path,
            'status'            =>$status,
           
            ]);
   if($insert==true){
       file_put_contents($path,base64_decode($images));
       $inserted=new stdClass();
       if($insert)
            {
            $inserted->status="1";
            $inserted->message="Image insert";
            //$updated->data=json_encode($insert); 
            
            }
            else
            {
            $inserted->status="0";
            $inserted->message="Image not Upload";
            
            }
       return response()->json($inserted);
       
   }else{
      return response()->json('not inserted');
   }
    //return response()->json('inserted');
 }else if($flag==1) {
    $images= $request['image'];
   $update=Userpersonal::whereuser_id($request['user_id'])->update([

            'user_id'           =>$request['user_id'],
            'image'             =>$path,
            
           
            ]);

     if($update==true){
       file_put_contents($path,base64_decode($images));
       $updated=new stdClass();
       if($update)
            {
            $updated->status="1";
            $updated->message="Image  Updated";
            
         }
            else
            {
            $updated->status="0";
            $updated->message="Image Not Updated";
            
            }
       return response()->json($updated);
   }else{
      return response()->json('not update');
   }
   //return response()->json('Update');
}else{

    return response()->json('Not update Plz try again');
}



}
/*_________________________________________________________________________________*/
/********************Quote Updating And Inserting By user Id***********************/

public function update_quote(Request $request){
        $status=1;
        $stausnull=0;
    $quote=Userquote::select('user_id')->where('user_id',$request['user_id'])->get();
    $userquote='';
        foreach ($quote as $key => $value) {
                $userquote=$value->user_id;
                break;
        }

if(empty($userquote) ){
             $data=Userquote::create([

                 'user_id'      =>$request['user_id'],
                 'quote'        =>$request['quote'],
                 'date'         =>$request['date'],
                 'status'       =>$status,

        ]);
                $insert=new stdClass();
                        if($data)
                            {
                            $insert->status="1";
                            $insert->data=json_encode($data); 
                            
                            }
                            else
                            {
                            $insert->status="0";
                            
                            }
                    return response()->json($insert);
    //return response()->json(1);
    }elseif (!empty($userquote)) {

         Userquote::whereuser_id($request['user_id'])->update([
                 'status'       =>$stausnull,

        ]);
        $insertdata=Userquote::create([

                 'user_id'      =>$request['user_id'],
                 'quote'        =>$request['quote'],
                 'date'         =>$request['date'],
                 'status'       =>$status,

        ]);
      //return response()->json(1); 
      $insert=new stdClass();
                        if($insertdata)
                            {
                            $insert->status="1";
                            $insert->data=json_encode($insertdata); 
                            
                            }
                            else
                            {
                            $insert->status="0";
                            
                            }
                    return response()->json($insert); 
    }

}

/************************Quote LIsting By User Id******************************/

public function userquotelist(Request $request){

          $quotelist=Userquote::select('*')->whereuser_id($request['user_id'])->orderBy('quote_id', 'desc')->get();
          $getdata=new stdClass;
          if($quotelist==true){

            $getdata->status="1";
            $getdata->data=json_encode($quotelist);
          }else{
            $getdata->status="0";
            $getdata->message="Quote Not Available";
          }
          return response()->json($getdata);

}


/*******************Personal Listing Api By Usre id***********************/

public function user_personal_list(Request $request){
  $userid=$request['user_id'];
 
$data="SELECT group_concat(distinct user_total_like.like_user_id) as likeuser_id,count(distinct user_total_like.like_user_id) as totallike,YEAR(CURRENT_DATE)-YEAR(p.dob) age,p.user_id,p.first_name,p.middel_name,p.last_name,p.dob,p.gender,p.contact,p.meterial_status,p.weight,p.height,p.ca1,p.ca2,p.ca3,p.c_country,p.is_permanent,p.pa1,p.pa2,p.pa3,p.pa3,p.p_country,p.status,group_concat(DISTINCT hobbies.hobbies) as hobbies,pets.pets_name as petname,c.Name as currentcity,adde.Name as permanentcity,chat_user.user_name as username,group_concat(DISTINCT job.id) as jobid,group_concat(DISTINCT job.description) as job_description,group_concat(DISTINCT job.salery_range) as saleryrange,group_concat(DISTINCT user_employement.company) as usercompany,group_concat( DISTINCT user_employement.designation) as designation,group_concat(DISTINCT user_employement.fromdate) as fromdate, group_concat(DISTINCT user_employement.todate) as todate,group_concat( DISTINCT user_skill.skill_id) as userskill,group_concat( DISTINCT user_skill.language_id) as userskill_language,group_concat( DISTINCT user_skill.lang_staus) as lang_status,group_concat( DISTINCT user_skill.charecter_sticks) as charesticks,group_concat( DISTINCT user_video.id) as videoid,group_concat( DISTINCT user_video.video_url) as video_url,group_concat( DISTINCT user_video.user_video_thumbnail) as user_video_thumb,group_concat( DISTINCT user_video.status ORDER BY user_video.status DESC) as videostatus,LOWER(country.Code2) as country_flage,COUNT(DISTINCT YEAR(user_employement.todate)) as passjob,SUM(DISTINCT YEAR(user_employement.todate)-YEAR(user_employement.fromdate)) as experience,GROUP_CONCAT(DISTINCT language.language) as language,GROUP_CONCAT(DISTINCT user_language.level) as englishlevel,chat_user.id as userid,chat_user.email as email,(ifnull(avg(rate.discipline),0)+ifnull(avg(rate.responsbility),0)+ifnull(avg(rate.cleanness_and_tidy),0)+ifnull(avg(rate.communication),0)+ifnull(avg(rate.patient),0)+ifnull(avg(rate.experience),0)+ifnull(avg(rate.attitude),0))/7 as Average_rating,ifnull(SUM(DISTINCT rate.smile),0)as smile,ifnull(SUM(DISTINCT rate.sad),0) as sad,ifnull(SUM(DISTINCT rate.laugh),0) as laugh,p.pet_love as pet_id,p.ccity_id as currentcity_id,p.pcity_id as permanent_city_id,group_concat(DISTINCT user_hobbies.hobbies_id) as user_hobbies_id FROM chat_user  LEFT JOIN user_personal_info p ON chat_user.id=p.user_id LEFT JOIN user_hobbies on chat_user.id=user_hobbies.user_id LEFT JOIN hobbies ON hobbies.id=user_hobbies.hobbies_id LEFT JOIN pets on pets.id=p.pet_love LEFT JOIN city c ON c.ID=p.ccity_id LEFT JOIN city adde ON adde.ID=p.pcity_id LEFT JOIN user_job_post job ON p.user_id=job.user_id LEFT JOIN user_total_like ON user_total_like.user_id=p.user_id LEFT JOIN user_employement ON user_employement.user_id=p.user_id LEFT JOIN country ON p.c_country=country.Name LEFT JOIN user_skill ON user_skill.user_id=p.user_id LEFT JOIN user_video ON user_video.user_id=chat_user.id LEFT JOIN user_language ON p.user_id=user_language.user_id LEFT JOIN language ON language.id=user_language.language_id LEFT JOIN user_total_rating rate ON rate.user_id=p.user_id where chat_user.id='$userid' GROUP BY p.user_id,p.first_name,p.middel_name,p.last_name,p.dob,p.gender,p.contact,p.meterial_status,p.weight,p.height,p.ca1,p.ca2,p.ca3,p.c_country,p.is_permanent,p.pa1,p.pa2,p.pa3,p.pa3,p.p_country,p.status,pets.pets_name,c.Name,adde.Name,chat_user.user_name,user_total_like.user_id,country.Code2,chat_user.id,chat_user.email,p.ccity_id,p.pcity_id,p.pet_love ORDER BY user_video.status DESC";

/*echo $data;
die();*/
           
          $userdata=DB::select($data);
            $useriddata='';
             foreach ($userdata as $key => $value) {
                  $useriddata=$value->userid;
            }

            /*$userdata=$userdata->toArray();
            $userhobbies=$hobb->toArray();
            $datavideo=$video->toArray();
            $result = array_merge($userdata, $userhobbies,$datavideo);*/
 
$data=new stdClass;
if($useriddata !=''){

$data->status="1";
$data->message="user personal all data";
$data->data= json_encode($userdata);
return response()->json($data);

}else{

        $dataall="SELECT group_concat(distinct user_total_like.like_user_id) as likeuser_id,count(distinct user_total_like.like_user_id) as totallike,YEAR(CURRENT_DATE)-YEAR(p.dob) age,p.user_id,p.first_name,p.middel_name,p.last_name,p.dob,p.gender,p.contact,p.meterial_status,p.weight,p.height,p.ca1,p.ca2,p.ca3,p.c_country,p.is_permanent,p.pa1,p.pa2,p.pa3,p.pa3,p.p_country,p.status,group_concat(DISTINCT hobbies.hobbies) as hobbies,pets.pets_name as petname,c.Name as currentcity,adde.Name as permanentcity,chat_user.user_name as username,chat_user.email as chat_email,group_concat(DISTINCT job.id) as jobid,group_concat(DISTINCT job.description) as job_description,group_concat(DISTINCT job.salery_range) as saleryrange,group_concat(DISTINCT user_employement.company) as usercompany,group_concat( DISTINCT user_employement.designation) as designation,group_concat(DISTINCT user_employement.fromdate) as fromdate, group_concat(DISTINCT user_employement.todate) as todate,group_concat( DISTINCT user_skill.skill_id) as userskill,group_concat( DISTINCT user_skill.language_id) as userskill_language,group_concat( DISTINCT user_skill.lang_staus) as lang_status,group_concat( DISTINCT user_skill.charecter_sticks) as charesticks,group_concat( DISTINCT user_video.id) as videoid,group_concat( DISTINCT user_video.video_url  ORDER BY user_video.status DESC) as video_url,group_concat( DISTINCT user_video.user_video_thumbnail) as user_video_thumb,group_concat( DISTINCT user_video.status ORDER BY user_video.status DESC) as videostatus,LOWER(country.Code2) as country_flage,COUNT(DISTINCT YEAR(user_employement.todate)) as passjob,SUM(DISTINCT YEAR(user_employement.todate)-YEAR(user_employement.fromdate)) as experience,GROUP_CONCAT(DISTINCT language.language) as language,GROUP_CONCAT(DISTINCT user_language.level) as englishlevel,(avg(rate.discipline)+avg(rate.responsbility)+avg(rate.cleanness_and_tidy)+avg(rate.communication)+avg(rate.patient)+avg(rate.experience)+avg(rate.attitude))/7 as Average_rating,GROUP_CONCAT(DISTINCT rate.comment) as comment,SUM(DISTINCT rate.smile)as smile,SUM(DISTINCT rate.sad) as sad,SUM(DISTINCT rate.laugh) as laugh,group_concat(DISTINCT chat.user_name)as comment_user,group_concat(DISTINCT personal.image) as comment_user_imge FROM user_personal_info p LEFT JOIN user_hobbies on p.user_id=user_hobbies.user_id LEFT JOIN hobbies ON hobbies.id=user_hobbies.hobbies_id LEFT JOIN pets on pets.id=p.pet_love LEFT JOIN city c ON c.ID=p.ccity_id LEFT JOIN city adde ON adde.ID=p.pcity_id LEFT JOIN chat_user ON chat_user.id=p.user_id LEFT JOIN user_job_post job ON p.user_id=job.user_id LEFT JOIN user_total_like ON user_total_like.user_id=p.user_id LEFT JOIN user_employement ON user_employement.user_id=p.user_id LEFT JOIN country ON p.c_country=country.Name LEFT JOIN user_skill ON user_skill.user_id=p.user_id LEFT JOIN user_video ON user_video.user_id=chat_user.id LEFT JOIN user_language ON p.user_id=user_language.user_id LEFT JOIN language ON language.id=user_language.language_id LEFT JOIN user_total_rating rate ON rate.user_id=p.user_id LEFT JOIN chat_user chat ON chat.id=rate.rating_user_id LEFT JOIN user_personal_info personal ON personal.user_id=rate.rating_user_id GROUP BY p.user_id,p.first_name,p.middel_name,p.last_name,p.dob,p.gender,p.contact,p.meterial_status,p.weight,p.height,p.ca1,p.ca2,p.ca3,p.c_country,p.is_permanent,p.pa1,p.pa2,p.pa3,p.pa3,p.p_country,p.status,pets.pets_name,c.Name,adde.Name,chat_user.user_name,chat_user.email,user_total_like.user_id,country.Code2";

      /*  foreach ($dataall as $key => $value) {
            $liked=$value->like_user_id;
        }*/
        
        $userdata=DB::select($dataall);
    
           $data=new stdClass;
               $data->status="1";
               $data->message="personal all data";
               $data->data=json_encode($userdata);
               return response()->json($data);
             


  }







}

public function uservideo(Request $request)
  {
      $user_id=$request['user_id'];
      $query="SELECT GROUP_CONCAT(id) VideoID,GROUP_CONCAT(video_url) VideoURL,group_concat(user_video_thumbnail) thumbnail,group_concat(status) VideoStatus ,user_id FROM `user_video` WHERE user_id='$user_id' group by user_id";
  
        $data=DB::select($query);
      if ($data==true) {
                $obj=new stdClass;
               $obj->status="1";
               $obj->message="personal all video";
               $obj->data=json_encode($data);
               return response()->json($obj);
      }else{
        $obj=new stdClass;
               $obj->status="0";
               $obj->message="User Data not found";
               
               return response()->json($obj);
      }
          

  }
/***************user hobbies listing********/

public function hobbieslist(Request $request){

  $userhobb=Userhobbies::select('*')->whereuser_id($request['user_id'])->get();

  $data=new stdClass;
  $userid='';
  foreach ($userhobb as $key => $value) {
    $userid=$value->user_id;
  }

  if ($userid==$request['user_id']) {
    
        $data->status="1";
        $data->data=json_encode($userhobb);
  }else{

      $data->status="0";
      $data->message="Data not Available";

  }
  return response()->json($data);

}


/***********************User Educational Api******************/
/*_________________________________________________________________________________*/

public function usereducation(Request $request){

$education=Usereducation::select('*')->where('user_id',$request['user_id'])->where('level',$request['level'])->get();
 $userid='';
 $level='';
foreach ($education as $key => $val) {
  $userid=$val->user_id;
  $level=$val->level;
  break;
}


$status=1;
$date=date("Y/m/d");
if($level!=$request['level']){

          $education=Usereducation::create([

              'user_id' =>$request['user_id'],
              'level' =>$request['level'],
              'degree' =>$request['degree'],
              'board' =>$request['board'],
              'from_year' =>$request['from_year'],
              'to_year' =>$request['to_year'],
              'date_created' =>$date,
              'status' =>$status,

            ]);
            if($education==true){
              
               $inserted=new stdClass();
               if($education)
                    {
                    $inserted->status="1";
                    $inserted->message="Education Info Inserted";
                    
                    
                    }
                    else
                    {
                    $inserted->status="0";
                    $inserted->message="Education Info Not Inserted";
                    
                    }
               return response()->json($inserted);
               
           }else{
              return response()->json('not inserted');
           }

}else if($level==$request['level']) {

     $education=Usereducation::wherelevel($request['level'])->where('user_id',$request['user_id'])->update([

              'user_id' =>$request['user_id'],
              'level' =>$request['level'],
              'degree' =>$request['degree'],
              'board' =>$request['board'],
              'from_year' =>$request['from_year'],
              'to_year' =>$request['to_year'],
              'date_created' =>$date,
              'status' =>$status,

            ]);
            if($education==true){
              
               $inserted=new stdClass();
               if($education)
                    {
                      $inserted->status="1";
                      $inserted->message="Education Info Update";
                    }
                    else
                    {
                      $inserted->status="0";
                      $inserted->message="Education Info Not Update";
                    
                    }
               return response()->json($inserted);
               
           }else{
              return response()->json('not inserted');
           }

}


}
/************************User Educational List Api*************************/
public function educationlist(Request $request){

          $education=Usereducation::select('*')->whereuser_id($request['user_id'])->orderBy('level', 'desc')->get();
          $getdata=new stdClass;
          if($education==true){

            $getdata->status="1";
            $getdata->data=json_encode($education);
          }else{
            $getdata->status="0";
            $getdata->message="Quote Not Available";
          }
          return response()->json($getdata);

}




/*************************User Employement Update Api****************/





public function useremployement(Request $request){
$status=1;
$employee=Useremployment::select('*')->where('user_id',$request['user_id'])->where('company',$request['company'])->get();

$userid='';
$cname='';
foreach ($employee as $key => $val) {
  $userid=$val->user_id;
  $cname=$val->company;
  break;
}

if($cname !=$request['company']){
        $empdata=Useremployment::create([

          'user_id'     =>$request['user_id'],
          'company'     =>$request['company'],
          'designation' =>$request['designation'],
           'exp' =>$request['exp'],
          'fromdate'    =>$request['fromdate'],
          'todate'      =>$request['todate'],
          'status'      =>$status,

        ]);
          $inserted=new stdClass();
         if($empdata==true){
              
             if($empdata)
                {
                $inserted->status="1";
                $inserted->message="Employement data Inserted";
                }else{
                  $inserted->status="0";
                  $inserted->message="Employement data Not Inserted";
                  
                  }
               return response()->json($inserted);
               
           }else{
                    $inserted->status="0";
                    $inserted->message="Employement data Not Inserted";
                    return response()->json($updateed);
           }

}else if ($cname == $request['company']) {
 
     $empdataupdate=Useremployment::where('company',$request['company'])->where('user_id',$request['user_id'])->update([

          'user_id'     =>$request['user_id'],
          'company'     =>$request['company'],
          'designation' =>$request['designation'],
          'exp' =>$request['exp'],
          'fromdate'    =>$request['fromdate'],
          'todate'      =>$request['todate'],
          'status'      =>$status,

        ]);
          $updateed=new stdClass();
         if($empdataupdate==true){
              
             if($empdataupdate)
                {
                $updateed->status="1";
                $updateed->message="Employement data update";
                }else{
                  $updateed->status="0";
                  $updateed->message="Employement data Not update";
                  
                  }
               return response()->json($updateed);
               
           }else{
                    $updateed->status="0";
                    $updateed->message="Something went wrong please check details";
                    return response()->json($updateed);
           }

}else{


}
     

}


/****************employeement List Api ********/

public function employeelist(Request $request){

          $employee=Useremployment::select('*')->whereuser_id($request['user_id'])->orderBy('company', 'desc')->get();
          $getdata=new stdClass;
          if($employee==true){

            $getdata->status="1";
            $getdata->data=json_encode($employee);
          }else{
            $getdata->status="0";
            $getdata->message="employee data Not Available";
          }
          return response()->json($getdata);

}





/*********************User Skill Update*****************************/
public function userskillupdate(Request $request){

        
$userid=$request['user_id'];
$skill_id=$request['skill_id'];
$charecter=$request['charecter'];
$languagename=$request['language'];
$langstatus=$request['lang_status'];
$status=1;
            
            $skillname = explode(',', $skill_id);
            $language = explode(',', $languagename);
            $lang_level = explode(',', $langstatus);
            $skillcount=count($skillname);
            $languagecount=count($language);
$data=Userskill::select('*')->where('user_id',$userid)->get();

$user_id='';
foreach ($data as $key => $value) {
 
            $user_id=$value->user_id;
            break;
}

if (empty($user_id)) {


for ($i=0; $i < $skillcount; $i++) { 

  
  $inserskill=Userskill::create([
              'user_id'          =>$userid,
              'skill_id'         =>$skillname[$i],
              'language_id'      =>$language[$i],
              'lang_staus'       =>$lang_level[$i],
              'charecter_sticks' =>$charecter,
              'status'           =>$status,
          ]);
      }

      $insert= new stdClass;
      if($inserskill==true){
                $insert->status="1";
                $insert->message="user skill inserted";
      }else{

                $insert->status="1";
                $insert->message="user skill not inserted";
      }
      return response()->json($insert);
}else if (!empty($user_id)) {
  
  echo $userid=$request['user_id'];
   echo $skill_id=$request['skill_id'];
   echo $charecter=$request['charecter'];
   echo $languagename=$request['language'];
   echo $langstatus=$request['lang_status'];
  /*die();*/

  Userskill::whereuser_id($userid)->delete($userid);

 /* for ($i=0; $i < $skillcount; $i++) { 
  
  $updatekill=Userskill::create([
              'user_id'          =>$userid,
              'skill_id'         =>$skillname[$i],
              'language_id'      =>$language[$i],
              'lang_staus'       =>$lang_level[$i],
              'charecter_sticks' =>$charecter,
              'status'           =>$status,
          ]);
      }*/
    
  
  $updatekill=Userskill::create([
              'user_id'          =>$userid,
              'skill_id'         =>$skill_id,
              'language_id'      =>$languagename,
              'lang_staus'       =>$langstatus,
              'charecter_sticks' =>$charecter,
              'status'           =>$status,
          ]);
      

      $insert= new stdClass;
      if($updatekill==true){
                $insert->status="1";
                $insert->message="user skill update";
      }else{

                $insert->status="1";
                $insert->message="user skill not update";
      }
      return response()->json($insert);

}


}




/*****************User Skill list Api******************/

public function userskillapi(Request $request){

  $user_id=$request['user_id'];
  /*$totalskill=Userskill::select('skill_id','language_id','lang_staus','charecter_sticks','user_id')->where('user_id',$user_id)->get(); */
  $totalskill=Userskill::selectRaw('GROUP_CONCAT(skill_id) as skill_id')->selectRaw('GROUP_CONCAT(language_id) as language_id')->selectRaw('GROUP_CONCAT(lang_staus) as lang_staus')->selectRaw('GROUP_CONCAT(charecter_sticks) as charecter_sticks')->where('user_id',$user_id)->groupBy('user_id')->get();

      $user='';
    $data=Userskill::select('user_id')->where('user_id',$user_id)->get();
        foreach ($data as $key => $value) {
          $user=$value->user_id;

        }
    $newrecord= new stdClass;
  if ($user==$user_id) {
    $newrecord->status="1";
    $newrecord->message="User skill all record";
    $newrecord->data= json_encode($totalskill);
  }else{

    $newrecord->status="0";
    $newrecord->message="this user record not fount our data";
  }

  return response()->json($newrecord);


}

/***********************Delete employeement detail by user *********************/

public function deleteedu(Request $request){

  $user_id=$request['user_id'];
  $userlevel=$request['level'];

$data=Usereducation::select('*')->where('user_id',$user_id)->where('level',$userlevel)->get();

$level='';
$userid='';
$id='';
foreach ($data as $key => $value) {
  $level=$value->level;
  $userid=$value->user_id;
  $id=$value->id;
  break;
}
if ($level==$userlevel && $userid==$user_id) {

    $delete=Usereducation::where('user_id',$user_id)->where('level',$userlevel)->delete($id);

        $newdelete= new stdClass;
      if ($delete==true) {
          
            $newdelete->status="1";
            $newdelete->message="record deleted succsessfully";
      }else{
            $newdelete->status="0";
            $newdelete->message="Something went wrong please try again";
      }
      return response()->json($newdelete);

}else{
            $newdelete= new stdClass;
            $newdelete->status="0";
            $newdelete->message="data not found in this education level";
            return response()->json($newdelete);
}


}


/*************delete employeement by user*****************/

public function deleteemp(Request $request){

  $user_id=$request['user_id'];
  $company=$request['company'];

$data=Useremployment::select('*')->where('user_id',$user_id)->where('company',$company)->get();

$level='';
$userid='';
$id='';
foreach ($data as $key => $value) {
  $level=$value->company;
  $userid=$value->user_id;
  $id=$value->id;
  break;
}
if ($level==$company && $userid==$user_id) {

    $delete=Useremployment::where('user_id',$user_id)->where('company',$company)->delete($id);

        $newdelete= new stdClass;
      if ($delete==true) {
          
            $newdelete->status="1";
            $newdelete->message="record deleted succsessfully";
      }else{
            $newdelete->status="0";
            $newdelete->message="Something went wrong please try again";
      }
      return response()->json($newdelete);

}else{
            $newdelete= new stdClass;
            $newdelete->status="0";
            $newdelete->message="data not found in this Employement";
            return response()->json($newdelete);
}


}




}
