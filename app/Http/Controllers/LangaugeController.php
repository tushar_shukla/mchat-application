<?php

namespace App\Http\Controllers;

use App\Langauge;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;
use Validator;
use App\Addskills;
use stdClass;

class LangaugeController extends Controller
{
    
public function index()
    {
    $lang=Langauge::select('*')->get();
    return view('admin.add_language',compact('lang'));

    }

 public function addlanguage(Request $request){

$language=Langauge::select('*')->where('language',$request['language'])->get();
$lang='';
foreach ($language as $key => $value) {
    $lang=$value->language;
    break;
}

        $status=1;
       $validation=$this->validate($request, [
            'language' => 'required|max:100',
            ]);
if($validation==true){

    if($lang !=$request['language']){

        Langauge::create([
            'language'  =>ucfirst($request['language']),
            'status'    =>$status,
            '_token'    =>$request['_token'],

        ]);

        Session::flash('flash_message','This Langauge Added Succsessfully');
       return redirect('/Add/Langauge/');

    }else{

          Session::flash('flash_ma','This Langauge Already Exist');
            return redirect('/Add/Langauge/');
    }

}else{
    return redirect('/Add/Langauge/');
}

 }




public function edit($id)
    {
        $lang=Langauge::select('*')->whereid($id)->first();
        return view('admin.update_language',compact('lang'));
    }


public function update_language($id,Request $request){
        $language=Langauge::select('*')->where('language',$request['language'])->get();
        $lang='';
        foreach ($language as $key => $value) {
            $lang=$value->language;
            break;
        }

        $status=1;
           if($lang !=$request['language']){

               Langauge::whereid($id)->update([
                    'language'  =>ucfirst($request['language']),
                    'status'    =>$status,
                    '_token'    =>$request['_token'],

                ]);

                Session::flash('flash_message','Langauge Update Succsessfully');
               return back();

            }else{

                  Session::flash('flash_ma','This Langauge Already Exist');
                    return back();
            }


}

public function delete_language($id){

Langauge::whereid($id)->delete($id);
Session::flash('flash_ma','Job Category Deleted Succsessfully');
return redirect('/Add/Langauge/');
}

/*******************Skill Manegment************************/
public function skillindex(){
    $skill=Addskills::select('*')->get();
    return view('admin.add_skill',compact('skill'));
}


public function addskill(Request $request){
    $status=1;
 $skilldata=Addskills::select('*')->where('skill',ucfirst($request['skill']))->get();

$skillvalue='';
 foreach ($skilldata as $key => $value) {
     $skillvalue=$value->skill;
     break;
 }

 if($skillvalue !=ucfirst($request['skill'])){
    Addskills::create([

        'skill' =>ucfirst($request['skill']),
        'status' =>$status,
        '_token' =>$request['_token'],

    ]);
    Session::flash('flash_message','Skill Added Succsessfully');
     return redirect('/Add/Skill/');
}else{

    Session::flash('flash_ma','This Skill Already Exist');
    return redirect('/Add/Skill/');
}
}


public function editskill($id){
    $skill=Addskills::select('*')->whereid($id)->first();
    return view('admin.update_skill',compact('skill'));
}


public function updateskill($id,Request $request){

 $status=1;
 $skilldata=Addskills::select('*')->where('skill',ucfirst($request['skill']))->get();

$skillvalue='';
 foreach ($skilldata as $key => $value) {
     $skillvalue=$value->skill;
     break;
 }

 if($skillvalue !=ucfirst($request['skill'])){
    Addskills::whereid($id)->update([

        'skill' =>ucfirst($request['skill']),
        'status' =>$status,
        '_token' =>$request['_token'],

    ]);
    Session::flash('flash_message','Skill Update Succsessfully');
     return back();
}else{

    Session::flash('flash_ma','This Skill Already Exist');
    return back();
}



}



public function delete_skill($id){

    Addskills::whereid($id)->delete($id);
    Session::flash('flash_ma','Skill Deleted Succsessfully');
    return redirect('/Add/Skill/');
}
/************************Api Coding **********************/


public function languageapi(){

    $data=Langauge::select('id','language','status')->get();
    //return response()->json($data);
    $obj=new stdClass();
        if(count($data)>0)
        {   
            $obj->message="successfully";
            $obj->status="1";
            $obj->data=json_encode($data);    
        }
        else
        {
            $obj->message="Unsuccess";
            $obj->status="0";
            $obj->data=null;
        }

        return response()->json($obj);

}



public function skillapi(){

    $skilldata=Addskills::select('id','skill','status')->get();

    //return response()->json($skilldata);
    $skills=new stdClass();
        if(count($skilldata)>0)
        {   
            $skills->message="successfully";
            $skills->status="1";
            $skills->data=json_encode($skilldata);    
        }
        else
        {
            $skills->message="Unsuccess";
            $skills->status="0";
            $skills->data=null;
        }

        return response()->json($skills);

}

}
