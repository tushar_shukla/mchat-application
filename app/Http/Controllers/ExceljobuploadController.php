<?php

namespace App\Http\Controllers;

use App\Exceljobupload;
use Illuminate\Http\Request;
use Session;
use View;
use Validator;
use App\Addskills;
use App\Jobpost;
use App\Joblanguage;
use App\Jobscreening;
use App\Jobskill;

class ExceljobuploadController extends Controller
{


public function jobuploadfromexcel(Request $request){

 if($request['uploadfile']>0)
   {
    $target=$request['uploadfile']; 

if(!empty($target))
{
    $filename = $_FILES['uploadfile']['name'];
    $ext1 = pathinfo($filename, PATHINFO_EXTENSION);

if ($ext1 == "csv")
{

$row = 1;
$handle = fopen($target, "r");

if ( $handle!==false)
{
$total=0;
$product_inserted=0;
$c=0;
$duplicate_cont='';

while (($data = fgetcsv($handle, 1000, ",")) !== FALSE)
{
$num = count($data);
$row++;

if($row>2)
{
$status=1;  
$published=0;  
$draff=1;  
$active=0;  
$make_private=0;

/*******************Job skills***********************/           
    $jobskill = explode(',', $data[19]);
    $totskill=count($jobskill);
    $array=array();
    for ($i=0; $i < $totskill ; $i++) { 
        $job_skills = Addskills::select('id')
        ->where('skill',$jobskill[$i])
        ->get();
        $skill_id='';
        foreach ($job_skills as $key => $value) {
        $skill_id=$value->id;
        array_push($array, $skill_id);
        }
    }
$skilltotal=implode(",", $array);
/*******************if empty skill name in database then insert *********************/
    if(empty($skilltotal)){
        $countskill=explode(',', $data[19]);
        $totskill=count($countskill);
        $array=array();
            for ($i=0; $i < $totskill ; $i++) {
                $getid=Addskills::create([
                'skill'  =>$countskill[$i],
                '_token' =>$request['_token'],
                'status' =>$status,

                ]);
            $skillid= $getid->id;
            array_push($array, $skillid);
            }
        $skilltotal=implode(",", $array);
    }

/**********date formate ******/
$date = str_replace('/', '-', $data[15]);
$startdate=date('Y-m-d', strtotime($date));

/*******************Insert Job In database*************************/     
if(!empty($data[0]) && !empty($data[9]))
 {
    $baseurl = pathinfo($data[9], PATHINFO_EXTENSION);
/*********************Job Post From Video**********************/
if ($baseurl== 'mp4')
     {
            $path="postjobvice/".$data[9];
            $job=Jobpost::create([
            'user_id'           =>$data[0],
            'user_name'         =>$data[1],
            /*'description'       =>$data[2],*/
            'nationality'       =>$data[3],
            'work_type'         =>$data[4],
            'working_time'      =>$data[5],
            'salery'            =>$data[6],
            'salery_range'      =>$data[7],
            'video'             =>$path,
            'location'          =>$data[10],
            'public_holiday'    =>$data[11],
            'leave_option'      =>$data[12],
            'if_option_other'   =>$data[13],
            'chareterstics'     =>$data[14],
            'start_date'        =>$startdate,
            'status'            =>$status,
            'published'         =>$published,
            'draft'             =>$draff,
            'active'            =>$active,
            'make_private'      =>$make_private,
            'trip_home'         =>$data[16],
            'trip_home_other'   =>$data[17],
            'video'             =>$path,
            ]);

            $jobid= $job->id;
            if ($jobid > 0) {
            /********************insert job skill*********************/
            $skilldata = explode(',', $skilltotal);
            $totskill=count($skilldata);
            for ($i=0; $i < $totskill ; $i++) { 
            $jobskill=Jobskill::create([

            'job_id'         =>$jobid,
            'user_id'        =>$data[0],
            'skills'         =>$skilldata[$i],
            'status'         =>$status,
            ]); 
            }
            /*****************Insert Job Language************************/
            $language = explode(',', $data[20]);
            $level = explode(',', $data[21]);
            $jobcount=count($language);
            for ($i=0; $i < $jobcount ; $i++) { 
            $joblang=Joblanguage::create([
            'job_id'            =>$jobid,
            'user_id'           =>$data[0],
            'language_position' =>$level[$i],
            'job_language'      =>$language[$i],
            'status'            =>$status,

            ]);  
            }

            /*******************screening Question Insert***********************/
            $screrendata= explode(',', $data[22]);
            $totscreen=count($screrendata);
            for ($i=0; $i < $totscreen; $i++) { 
            $jobscreen=Jobscreening::create([
            'job_id'               =>$jobid,
            'user_id'              =>$data[0],
            'sreening_question'    =>$screrendata[$i],
            'status'               =>$status,
            ]); 
            }
            }
            
     }else if($baseurl=='mp3')
        {
        /*****************Job Post From Mp3 wise**********************/
        $path="postjobvice/".$data[9];
        $job=Jobpost::create([
        'user_id'           =>$data[0],
        'user_name'         =>$data[1],
        /*'description'       =>$data[2],*/
        'nationality'       =>$data[3],
        'work_type'         =>$data[4],
        'working_time'      =>$data[5],
        'salery'            =>$data[6],
        'salery_range'      =>$data[7],
        'voice'             =>$path,
        'location'          =>$data[10],
        'public_holiday'    =>$data[11],
        'leave_option'      =>$data[12],
        'if_option_other'   =>$data[13],
        'chareterstics'     =>$data[14],
        'start_date'        =>$startdate,
        'status'            =>$status,
        'published'         =>$published,
        'draft'             =>$draff,
        'active'            =>$active,
        'make_private'      =>$make_private,
        'trip_home'         =>$data[16],
        'trip_home_other'   =>$data[17],
        ]);

        $jobid= $job->id;
        if ($jobid > 0) {
        /********************insert job skill*********************/
        $skilldata = explode(',', $skilltotal);
        $totskill=count($skilldata);
        for ($i=0; $i < $totskill ; $i++) { 
        $jobskill=Jobskill::create([

        'job_id'         =>$jobid,
        'user_id'        =>$data[0],
        'skills'         =>$skilldata[$i],
        'status'         =>$status,
        ]); 
        }
        /*****************Insert Job Language************************/
        $language = explode(',', $data[20]);
        $level = explode(',', $data[21]);
        $jobcount=count($language);
        for ($i=0; $i < $jobcount ; $i++) { 
        $joblang=Joblanguage::create([
        'job_id'            =>$jobid,
        'user_id'           =>$data[0],
        'language_position' =>$level[$i],
        'job_language'      =>$language[$i],
        'status'            =>$status,

        ]);  
        }

        /*******************screening Question Insert***********************/
        $screrendata= explode(',', $data[22]);
        $totscreen=count($screrendata);
        for ($i=0; $i < $totscreen; $i++) { 
        $jobscreen=Jobscreening::create([
        'job_id'               =>$jobid,
        'user_id'              =>$data[0],
        'sreening_question'    =>$screrendata[$i],
        'status'               =>$status,
        ]); 
        }
        }

    }
}else{
 /***************************Job POst From Text*************************/ 
if(!empty($data[0])) 
    {   
        $job=Jobpost::create([
        'user_id'           =>$data[0],
        'user_name'         =>$data[1],
        'description'       =>$data[2],
        'nationality'       =>$data[3],
        'work_type'         =>$data[4],
        'working_time'      =>$data[5],
        'salery'            =>$data[6],
        'salery_range'      =>$data[7],
        'location'          =>$data[10],
        'public_holiday'    =>$data[11],
        'leave_option'      =>$data[12],
        'if_option_other'   =>$data[13],
        'chareterstics'     =>$data[14],
        'start_date'        =>$startdate,
        'status'            =>$status,
        'published'         =>$published,
        'draft'             =>$draff,
        'active'            =>$active,
        'make_private'      =>$make_private,
        'trip_home'         =>$data[16],
        'trip_home_other'   =>$data[17],

        ]);

        $jobid= $job->id;
        if ($jobid > 0) {
        /********************insert job skill*********************/
        $skilldata = explode(',', $skilltotal);
        $totskill=count($skilldata);
        for ($i=0; $i < $totskill ; $i++) { 
        $jobskill=Jobskill::create([

        'job_id'         =>$jobid,
        'user_id'        =>$data[0],
        'skills'         =>$skilldata[$i],
        'status'         =>$status,
        ]); 
        }
        /*****************Insert Job Language************************/
        $language = explode(',', $data[20]);
        $level = explode(',', $data[21]);
        $jobcount=count($language);
        for ($i=0; $i < $jobcount ; $i++) { 
        $joblang=Joblanguage::create([
        'job_id'            =>$jobid,
        'user_id'           =>$data[0],
        'language_position' =>$level[$i],
        'job_language'      =>$language[$i],
        'status'            =>$status,

        ]);  
        }

        /*******************screening Question Insert***********************/
        $screrendata= explode(',', $data[22]);
        $totscreen=count($screrendata);
        for ($i=0; $i < $totscreen; $i++) { 
        $jobscreen=Jobscreening::create([
        'job_id'               =>$jobid,
        'user_id'              =>$data[0],
        'sreening_question'    =>$screrendata[$i],
        'status'               =>$status,
        ]); 
        }
        }
    }else{
         Session::flash('flash_ma','Can not blank userid in excel sheet');
        return redirect('/User/job/Listing');
    }
}

}
}
        if($jobscreen==true)
        {
        Session::flash('edit','File Upload successfully');
        return redirect('/User/job/Listing');
        }else{
        Session::flash('flash_ma','Some thing went wronge please check details');
        return redirect('/User/job/Listing');
        }         
}
} 
    else{
    Session::flash('flash_ma','Please Upload csv excel file formate');
    return redirect('/User/job/Listing');
    }
  }
 }

}


public function jobsample(){
$filename = "Sample For Upload";     
$file_ending = "xls";
header("Content-Type: application/xls");
header("Content-Disposition: attachment; filename=$filename.xls");
header("Pragma: no-cache"); 
header("Expires: 0");
$sep = "\t";

$schema_insert = "UserID\tJob Title\tJob Description\tNationality\tWork Type\tWorking Time\tSalary\tSalary Range\tVoice\tVideo\tLocation\tPublic Holiday\tLeave Option\tOption Other\tCharacteristics\tJob Start Date\tTrip Home\tOther Trip Home\tLeaves\tJob Skill\tJob Language\tLanguage Status\tScreening Question\n";

echo "$schema_insert";
}

public function uploadmedia(Request $request)
{

$extension=array("mp4","mp3");
    $file_name=$_FILES["video"]["name"];
    $total=count($file_name);
    $path=public_path('postjobvice/');
    for ($i=0; $i < $total; $i++) { 
         echo $file_name=$_FILES["video"]["name"][$i];
        $ext=pathinfo($file_name,PATHINFO_EXTENSION); 
         if(in_array($ext,$extension))
        {
            $upload=move_uploaded_file($_FILES["video"]["tmp_name"][$i],$path.$file_name);

           
        }else{
             Session::flash('flash_ma','Please select mp4 and mp3 file formate');
             return redirect('/User/job/Listing');
        }

    }
    if($upload==true){
        Session::flash('edit','Media Upload successfully');
        return redirect('/User/job/Listing');
    }else{
        Session::flash('flash_ma','Something went wrong please try agian');
        return redirect('/User/job/Listing');
    }
}



}
