<?php

namespace App\Http\Controllers;

use App\Abusereport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;
use View;
use Validator;
use stdClass;

class AbusereportController extends Controller
{
   

public function abusereport(Request $request){

    $jobid=$request['job_id'];
    $user_id=$request['report_user'];
    $date=$request['date'];
    $comment=$request['comment'];
    $status=1;
    $data=Abusereport::select('*')->where('job_id',$jobid)->where('user_id_given_report',$user_id)->get();
    $jobname='';
    $username='';
    foreach ($data as $key => $value) {
        $jobname=$value->job_id;
        $username=$value->user_id_given_report;
        break;
    }
if ($username !=$user_id && $jobname != $jobid) {

    if (!empty($jobid) && !empty($user_id)) {

     $insert=Abusereport::create([

                    'job_id'                 =>$jobid,
                    'report_date'            =>$date,
                    'user_id_given_report'   =>$user_id,
                    'comment'                =>$comment,
                    'status'                 =>$status,

                ]);
        $new= new stdClass;
         if ($insert==true) {
             $new->status="1";
             $new->message="report send successfully";
         }else{

            $new->status="0";
            $new->message="report not send try agian";
         }
      return response()->json($new);
   
}else{

        $new= new stdClass;
        $new->status="0";
        $new->message="can not black job and report user name";
        return response()->json($new);
}
   
}else{
        $new= new stdClass;
        $new->status="0";
        $new->message="you have alredy reported this job";
        return response()->json($new);
}

               

}



public function abouselist(Request $request,$id){

    $data=DB::table('abuse_job_report')
    ->leftjoin('chat_user','chat_user.id','=','abuse_job_report.user_id_given_report')
    ->leftjoin('user_job_post','user_job_post.id','=','abuse_job_report.job_id')
    ->leftjoin('chat_user as userdata','userdata.id','=','user_job_post.user_id')
    ->where('abuse_job_report.job_id',$id)
    ->select('abuse_job_report.comment as comment','abuse_job_report.report_date as reportdate','chat_user.user_name as username','user_job_post.description','userdata.user_name as postjobuser')
    ->get();
/*    echo "<pre>";
print_r($data);
die();*/
return view('admin.jobreport_deatil',compact('data'));

    
}






}
