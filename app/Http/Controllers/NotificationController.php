<?php

namespace App\Http\Controllers;

use App\Notification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests;
use Session;
use Validator;
use App\Userregister;
use stdClass;

class NotificationController extends Controller
{
    
    
    public function index()
    {
        $notification=Notification::select('*')->get();
        return view('admin.add_notification',compact('notification'));

    }

    
public function store(Request $request)
    {
     $title=$request['notification'];
     $message=$request['message'];
/*
     $token="e8352dWjF80:APA91bEm0-1FF_eDJdGu3zPkdRIMkxRWp3lwBSLDk9KJ2U81JP1AS3viigBj7vHUpBZquc0PXxBEmmLJ5gS8-TFImlNspY356yWSCmCP9a2RyWRtIg4uuLilc_Azw8DXKjKfSpGZgQinVq4TVx6U-7m-yFHp8cm2PQ";*/
      $userdata=Userregister::select('firebase_token')->get();
     $status=1;
       $array = array();
    foreach ($userdata as $key => $value) {
            $token=$value->firebase_token;
            array_push($array, $token);
    }
 if ($message !='' && $title !='') {
        /* print_r($array);
         die();*/
                $msg=$this->notification($array,$title,$message);

            Notification::create([
                'notification'  =>$request['notification'],
                'title'         =>$message,
                'status'        =>$status,
                '_token'        =>$request['_token'],

            ]);

                   

                Session::flash('flash_message','This Notification Sent Succsessfully');
                return redirect('/Send/User/Notification/');
      
    }else{

        Session::flash('flash_ma','Cant not blank title and message field');
        return redirect('/Send/User/Notification/');
    }       


}

public function notification($token, $title,$message)
    {
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
        $token=$token;
    /*print_r($token);
    die();*/
        $message=$message;

        $notification = [
            'title' => $title,
            'message' => $message,
            'sound' => true,
        ];
        
        $extraNotificationData = ["message" => $notification,"moredata" =>'dd'];

        $fcmNotification = [
            'registration_ids' => $token, //multple token array
            //'to'        => $token, //single token
            'data' => $extraNotificationData,
           
        ];

        $headers = [
            'Authorization: key=AIzaSyAuqmMsfQtmxrwoujo5qcu5Ix5FQG-4TEU',
            'Content-Type: application/json'
        ];


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        
        
        $result = curl_exec($ch);
       /* echo $result;
        die();*/
         if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
       
        return $result;
    }


/************************************************************************************/


 public function destroy($id,Notification $notification)
    {
        
  Notification::whereid($id)->delete($id);
    Session::flash('flash_ma','Notification Deleted Succsessfully');
 return redirect('/Send/User/Notification/');

    }




/*************************mobile sms notification*************************************************/
public function chatsendnotification(Request $request){
    $title=$request['text'];
    //$user_id=$request['user_id'];
    $sender=$request['idSender'];
    $receiver=$request['idReceiver'];
    $file=$request['file'];
    $sendername=$request['senderName'];
    $timestamp=time();
    $type=$request['type'];
    $cuurenttime= date('Y-m-d G:i:s');

   /* echo $receiver;
    echo $file;
    echo $timestamp;
    die();*/
     $user_id = explode(',',$request['user_id']);
    $idcount=count($user_id);
    $array=array();
for ($i=0; $i < $idcount; $i++) { 
        if (!empty($user_id[$i])) {
        $usertoken=Userregister::select('firebase_token')->where('email',$user_id[$i])->get();
        

        foreach ($usertoken as $key => $value) {
            $token=$value->firebase_token;
            array_push($array, $token);
        }
    }
}
        
$message=$title;
     if ($message !='') {
      // print_r($token);
            $sendconfirm=$this->chatnotification($array,$title,$message,$sender,$receiver,$file,$type, $sendername, $cuurenttime,$timestamp);
            $inserted= new stdClass();
            if ($sendconfirm==true) {
                    $inserted->status="1";
                    $inserted->message="notification send ";
                    return response()->json($inserted);
                }else{
                $inserted->status="0";
                $inserted->message="notification not send ";
                return response()->json($inserted);
                }
        }else{
                $inserted= new stdClass();
                 $inserted->status="0";
                $inserted->message="message or title empty";
                 return response()->json($inserted);
        }

                            
     
        
    }       


    
 /*  echo $token;
   die();*/
    




public function chatnotification($token, $title, $message, $sender, $receiver, $file, $type, $sendername, $cuurenttime,$timestamp)
    {

        /*, $sender, $receiver, $file, $timestamp*/
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
        $token=$token;
    
        $message=$message;
        $sender=$sender;
        $receiver=$receiver;
        $file=$file;
        $timestamp=$timestamp;
        $type=$type;
        $sendername=$sendername;
        $cuurenttime=$cuurenttime;
       // print_r($token);
     
        $notification = [
            'title' => $sendername,
            'message' => $message,
            'sender' => $sender,
            'receiver' => $receiver,
            'file' => $file,
            'type' => $type,
            'priority'=>'high',
            'timestamp'=>$timestamp,
            'currenttime' => $cuurenttime,
            'sound' => True,
        ];
        
      
        $extraNotificationData = ["message" => $notification,"moredata" =>'dd'];

        $fcmNotification = [
            'registration_ids' => $token, //multple token array
            //'to'        => $token, //single token
            'data' => $notification,
            'data' => $extraNotificationData
        ];

        $headers = [
            'Authorization: key=AIzaSyAuqmMsfQtmxrwoujo5qcu5Ix5FQG-4TEU',
            'Content-Type: application/json'
        ];


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        

       
        $result = curl_exec($ch);

        /* echo $result;
        die();*/
         if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
       
       
        return $result;
    }





}
