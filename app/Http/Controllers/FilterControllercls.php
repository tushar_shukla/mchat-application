<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use stdClass;

class FilterControllercls extends Controller
{
 

public function allpersonalfilter(Request $request){
$age=$request['age'];
$language=$request['language'];
$level=$request['level'];
$country=$request['country'];
$edu_level=$request['edu_level'];
$marital=$request['marital_status'];
$skill=trim($request['skill'],',');;
$hobbies=$request['hobbies'];
$charaterstics=$request['charaterstics'];
$experience=$request['experience'];
$experience_in=$request['work_experience_in'];
$rating=$request['rating'];
$discipline=$request['discipline'];
$responsbility=$request['responsbility'];
$clenners=$request['clenners'];
$communication=$request['communication'];
$patient=$request['patient'];
$experiencerate=$request['experiencerate'];
$attitude=$request['attitude'];
$max=$request['max'];
$min=$request['min'];


   $dobdata="SELECT group_concat(distinct user_total_like.like_user_id) as likeuser_id,count(distinct user_total_like.like_user_id) as totallike,YEAR(CURRENT_DATE)-YEAR(p.dob) age,p.user_id,p.first_name,p.middel_name,p.last_name,p.dob,p.gender,p.contact,p.meterial_status,p.weight,p.height,p.ca1,p.ca2,p.ca3,p.c_country,p.is_permanent,p.pa1,p.pa2,p.pa3,p.pa3,p.p_country,p.p_country as nationality,p.image,p.status,group_concat(DISTINCT hobbies.hobbies) as hobbies,pets.pets_name as petname,c.Name as currentcity,adde.Name as permanentcity,chat_user.user_name as username,group_concat(DISTINCT job.id) as jobid,group_concat(DISTINCT job.description) as job_description,group_concat(DISTINCT job.salery_range) as saleryrange,group_concat(DISTINCT job.salery) as salery,group_concat(DISTINCT user_employement.company) as usercompany,group_concat( DISTINCT user_employement.designation) as designation,group_concat(DISTINCT user_employement.fromdate) as fromdate, group_concat(DISTINCT user_employement.todate) as todate,group_concat( DISTINCT user_skill.skill_id) as userskill,group_concat( DISTINCT user_skill.language_id) as userskill_language,group_concat( DISTINCT user_skill.lang_staus) as lang_status,group_concat( DISTINCT user_skill.charecter_sticks) as charesticks,group_concat( DISTINCT user_video.id) as videoid,group_concat( DISTINCT user_video.video_url) as video_url,group_concat( DISTINCT user_video.user_video_thumbnail) as user_video_thumb,group_concat( DISTINCT user_video.status) as videostatus,country.Code2 as country_flage,COUNT(DISTINCT YEAR(user_employement.todate)) as passjob,SUM(DISTINCT YEAR(user_employement.todate)-YEAR(user_employement.fromdate)) as experience,GROUP_CONCAT(DISTINCT language.language) as language,GROUP_CONCAT(DISTINCT user_language.level) as englishlevel,GROUP_CONCAT( DISTINCT user_education.level) as education_level,(avg(rate.discipline)+avg(rate.responsbility)+avg(rate.cleanness_and_tidy)+avg(rate.communication)+avg(rate.patient)+avg(rate.experience)+avg(rate.attitude))/5 as Average_rating,GROUP_CONCAT( DISTINCT job_profession.profession) as working_exp,avg(rate.discipline)/5 as discipline,avg(rate.responsbility)/5 as responsbility,
    avg(rate.cleanness_and_tidy)/5 as cleanneress,avg(rate.communication)/5 as communication,avg(rate.patient)/5 as patient,avg(rate.experience) as rateexperience,avg(rate.attitude)/5 as attitude,GROUP_CONCAT(DISTINCT rate.comment) as comment,SUM(DISTINCT rate.smile)as smile,SUM(DISTINCT rate.sad) as sad,SUM(DISTINCT rate.laugh) as laugh,group_concat(DISTINCT chat.user_name)as comment_user,group_concat(DISTINCT personal.image) as comment_user_imge FROM user_personal_info p LEFT JOIN user_hobbies on p.user_id=user_hobbies.user_id LEFT JOIN hobbies ON hobbies.id=user_hobbies.hobbies_id LEFT JOIN pets on pets.id=p.pet_love LEFT JOIN city c ON c.ID=p.ccity_id LEFT JOIN city adde ON adde.ID=p.pcity_id LEFT JOIN chat_user ON chat_user.id=p.user_id LEFT JOIN user_job_post job ON p.user_id=job.user_id LEFT JOIN user_total_like ON user_total_like.user_id=p.user_id LEFT JOIN user_employement ON user_employement.user_id=p.user_id LEFT JOIN country ON p.c_country=country.Name LEFT JOIN user_skill ON user_skill.user_id=p.user_id LEFT JOIN user_video ON user_video.user_id=p.user_id LEFT JOIN user_language ON p.user_id=user_language.user_id LEFT JOIN language ON language.id=user_language.language_id LEFT JOIN user_total_rating rate ON rate.user_id=p.user_id LEFT JOIN user_education ON user_education.user_id=p.user_id  LEFT JOIN job_profession ON job_profession.profession=user_employement.designation LEFT JOIN chat_user chat ON chat.id=rate.rating_user_id LEFT JOIN user_personal_info personal ON personal.user_id=rate.rating_user_id WHERE 1=1";
    if($language!='')
    {
        $dobdata=$dobdata." AND user_skill.language_id='$language'";
    }

   if($level!='')
    {
        $dobdata=$dobdata." AND user_skill.lang_staus='$level'";
    }  
   
    if ($country!='') {
      
      $dobdata=$dobdata." AND p.c_country LIKE '%".$country."%'";
    }
         
   if ($marital !='') {
         
          $dobdata=$dobdata." AND p.meterial_status='$marital'";
        
      }   

    if($experience!='')
    {
        $dobdata=$dobdata." AND YEAR(user_employement.todate)-YEAR(user_employement.fromdate) >='$experience'";
     }  
        
    if ($edu_level !='') {
         
          $dobdata=$dobdata." AND user_education.level LIKE'%".$edu_level."%'";
       }   

    if ($skill !='') {
         $dobdata=$dobdata." AND user_skill.skill_id LIKE '%".$skill."%'";
    }
        
    if ($hobbies!='') {
       $dobdata=$dobdata." AND hobbies.hobbies LIKE'%".$hobbies."%'";
     } 

    if ($charaterstics!='') {
        $dobdata=$dobdata." AND user_skill.charecter_sticks LIKE '%".$charaterstics."%'";
      }

 if ($experience_in!='') {
        $dobdata=$dobdata." AND job_profession.profession LIKE '%".$experience_in."%'";
      }
   if ($rating!='') {
        $dobdata=$dobdata." AND Exists
      ( 
        select (avg(rate.comment)+avg(rate.discipline)+avg(rate.responsbility)+avg(rate.cleanness_and_tidy)+avg(rate.communication)+avg(rate.patient)+avg(rate.experience)+avg(rate.attitude))/5 as Average FROM user_total_rating rate HAVING (avg(rate.comment)+avg(rate.discipline)+avg(rate.responsbility)+avg(rate.cleanness_and_tidy)+avg(rate.communication)+avg(rate.patient)+avg(rate.experience)+avg(rate.attitude))/5 LIKE '%".$rating."%'  
          
      )";
      } 
      if ($discipline !='' || $responsbility !='' || $clenners !='' || $communication !='' || $patient !='' || $experience !='' || $attitude !='') {
         $dobdata=$dobdata." AND Exists
          ( 
            select avg(rate.discipline)/5 as discipline,avg(rate.responsbility)/5 as responsbility, avg(rate.cleanness_and_tidy)/5 as cleanneress,avg(rate.communication)/5 as communication,avg(rate.patient)/5 as patient,avg(rate.experience) as rateexperience,avg(rate.attitude)/5 as attitude from user_total_rating rate HAVING avg(rate.responsbility) LIKE '%$responsbility%' OR (avg(rate.cleanness_and_tidy)) LIKE '%$clenners%' OR (avg(rate.communication)) LIKE '%$communication%' OR (avg(rate.patient)) LIKE '%$patient%' OR (avg(rate.experience)) LIKE '%$experiencerate%' OR (avg(rate.attitude)) LIKE '%$attitude%'  OR (avg(rate.attitude)) LIKE '%$attitude%' OR (avg(rate.discipline)) LIKE '%$discipline%'
          
         )";
      }

   $dobdata=$dobdata." GROUP BY p.id,p.user_id,p.first_name,p.middel_name,p.last_name,p.dob,p.gender,p.contact,p.meterial_status,p.weight,p.height,p.ca1,p.ca2,p.ca3,p.c_country,p.is_permanent,p.pa1,p.pa2,p.pa3,p.pa3,p.p_country,p.status,p.nationality,p.pet_love,p.ccity_id,p.pcity_id,p.image,p.created_at,p.updated_at,p._token,user_employement.user_id,pets.pets_name,c.Name,adde.Name,chat_user.user_name,country.Code,country.Code2,p.image";

   if($min!='' && $max !='')
   {          
     $dobdata=$dobdata." HAVING YEAR(CURRENT_DATE)-YEAR(p.dob) BETWEEN '$min' AND '$max'";  
   }
 
    /*$dobdata="SELECT first_name,YEAR(CURRENT_DATE)-YEAR(p.dob) age from user_personal_info p  where YEAR(CURRENT_DATE)-YEAR(dob)='$age'";*/
      /*echo $dobdata;
      die(); */
    $list_data=DB::select($dobdata);
     $obj= new stdClass;
     $obj->status="1";
     //$obj->data=$list_data;
     $obj->data=json_encode($list_data);
     return response()->json($obj);
 
}


/******************************All Job filter*****************************/


public function alljobfilter(Request $request){

$job_type=$request['job_type'];
$location=$request['location'];
$salery_range=$request['salery_range'];
$work_with=$request['work_with'];
$nationality=$request['nationality'];
$working_exp=$request['working_exp'];
$language=$request['language'];
$language_level=$request['language_level'];


   $datajob="SELECT job.id as jobid,job.user_id,job.user_name,job.description,job.nationality, job.working_time,job.work_type,job.salery_range,job.salery,job.text,job.voice,job.video,job.video_thumb,job.location,job.public_holiday,job.leave_option,job.if_option_other,job.chareterstics,job.start_date,job.status,job.published,job.draft,job.active,job.make_private,job.trip_home,job.trip_home_other,job.leaves,chat_user.user_name as postedby,job.created_at as jobposted_date,SUM(DISTINCT YEAR(user_employement.todate)-YEAR(user_employement.fromdate)) as experience,GROUP_CONCAT( DISTINCT job_profession.profession) as working_exp,GROUP_CONCAT(user_jobpost_language.job_language) as language,GROUP_CONCAT(user_jobpost_language.language_position) as language_level FROM user_job_post job LEFT JOIN user_employement ON user_employement.user_id=job.user_id  LEFT JOIN job_profession ON job_profession.profession=user_employement.designation LEFT JOIN chat_user ON chat_user.id=job.user_id LEFT JOIN user_jobpost_language ON user_jobpost_language.user_id=job.user_id AND job.id=user_jobpost_language.job_id WHERE 1=1 AND job.published='1' ";
  if ($job_type !='') {
        
        $datajob=$datajob." AND job.working_time='$job_type'"; 
  }

  if ($language !='') {
        
        $datajob=$datajob." AND user_jobpost_language.job_language='$language'"; 
  }

  if ($language_level !='') {
        
        $datajob=$datajob." AND user_jobpost_language.language_position='$language_level'"; 
  }

  if ($location !='') {
        
        $datajob=$datajob." AND job.location LIKE '%".$location."%'"; 

  }

  if ($salery_range !='') {
        $datajob=$datajob." AND job.salery_range='$salery_range'";  
  }

  if ($work_with !='') {
        $datajob=$datajob." AND job.work_type='$work_with'"; 
  }

  if ($nationality !='') {
        $datajob=$datajob." AND job.nationality LIKE '%".$nationality."%'"; 
  }

  if ($working_exp !='') {
      $datajob=$datajob." AND job_profession.profession LIKE '%".$working_exp."%'";
  }
     
$datajob=$datajob." GROUP BY job.user_id,job.id,job.user_id,job.user_name,job.description,job.nationality, job.working_time,job.work_type,job.salery_range, job.text,job.voice,job.video,job.location,job.public_holiday,job.leave_option,job.if_option_other,job.chareterstics,job.start_date,job.status,job.published,job.draft,job.active,job.make_private,job.trip_home,job.trip_home_other,job.leaves,job.salery,job.video_thumb,chat_user.user_name,job.created_at";

  /*   echo $datajob;
      die(); */
     $list_data=DB::select($datajob);
     $obj= new stdClass;
     $obj->status="1";
     $obj->data=$list_data;
     //$obj->data=json_encode($list_data);
     return response()->json($obj);
    

}


/**********************character stick list for personal filter************************/

public function characterlist(){

  $query="SELECT DISTINCT charecter_sticks,id FROM user_skill";
  $alldata=DB::select($query);
 $obj=new stdClass;

 if($alldata==true){
  $obj->status="1";
   $obj->message="character stick list for filter";
   $obj->characterlist=json_encode($alldata);
}else{
    $obj->status="0";
   $obj->message="data not found";
}

return response()->json($obj);

}


}
