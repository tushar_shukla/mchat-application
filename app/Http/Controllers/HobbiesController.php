<?php

namespace App\Http\Controllers;

use App\Hobbies;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;
use Validator;
use stdClass;

class HobbiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $hobbies=Hobbies::select('*')->get();
        return view('admin.add_hobbies',compact('hobbies'));

    }

  
    public function store(Request $request)
    {
       $status=1;
       $validation=$this->validate($request, [
            'hobbies' => 'required|max:100',
            ]);
       $hobbies=Hobbies::select('*')->where('hobbies',$request['hobbies'])->get();

       $hobb='';
       foreach ($hobbies as $key => $val) {
                $hobb=$val->hobbies;
                break;
       }
if($validation==true){

    if($hobb != $request['hobbies']){
            Hobbies::create([

                    'hobbies'   =>ucfirst($request['hobbies']),
                    '_token'    =>$request['_token'],
                    'status'    =>$status,
                    ]);

            Session::flash('flash_message','Hobbies Added Succsessfully');
             return redirect('/Add/User/Hobbies/');
         }else{
            Session::flash('flash_ma','This Hobbies Already Exist');
                 return redirect('/Add/User/Hobbies/');
         }
        }else{
                //Session::flash('flash_ma','This Hobbies Already Exist');
                 return redirect('/Add/User/Hobbies/');
            }

    }

    
    public function edit($id)
    {
        $edit_hobbies=Hobbies::select('*')->whereid($id)->first();
        return view('admin.update_hobbies',compact('edit_hobbies'));
    }

 
    public function update($id,Request $request, Hobbies $hobbies)
    {
      $status=1;
      if(!empty($request['hobbies'])){
    $hobbies=Hobbies::select('*')->where('hobbies',$request['hobbies'])->get();

       $hobb='';
       foreach ($hobbies as $key => $val) {
                $hobb=$val->hobbies;
                break;
       }
if($hobb !=$request['hobbies']){
        Hobbies::whereid($id)->update([

            'hobbies' =>$request['hobbies'],
            'status'  =>$status,
        ]);

             Session::flash('flash_message','Hobbies Update Succsessfully');
             return back();
         }else{
            Session::flash('flash_ma','This hobbies Already Exist');
            return back(); 
         }
    }else{

             Session::flash('flash_ma','Can not blank field');
            return back();
    }


    }


    public function destroy($id,Hobbies $hobbies)
    {
        Hobbies::whereid($id)->delete($id);
        Session::flash('flash_ma','Hobbies Delete Succsessfully');
        return redirect('/Add/User/Hobbies');
    }



    /*************************for Api code************/

    public function Hobbieslist(){

        $hobbies=Hobbies::select('id','hobbies','status')->get();
        $obj=new stdClass();
        if(count($hobbies)>0)
        {
            $obj->status="1";
            $obj->data=json_encode($hobbies);    
        }
        else
        {
            $obj->status="0";
            $obj->data=null;
        }
        
        return response()->json($obj);
    }
}
