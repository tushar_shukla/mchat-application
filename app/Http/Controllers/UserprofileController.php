<?php

namespace App\Http\Controllers;

use App\Userprofile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;
use View;
use Validator;
use App\Userpersonal;
use App\Usereducation;
use App\Useremployment;
use App\Userlanguage;
use App\Userquote;
use App\Userskill;
use App\Jobpost;
use App\Applyjob;
use App\Applyjobscreen;
use App\Userregister;

class UserprofileController extends Controller
{


    public function index()
    {
        $userdata= DB::table('chat_user')
            ->leftjoin('user_personal_info', 'chat_user.id', '=', 'user_personal_info.user_id')
            ->leftjoin('country','user_personal_info.nationality','=','country.id')
            ->select('chat_user.*', 'user_personal_info.nationality','user_personal_info.contact','country.Name')
            ->get();
                      
        return view('admin.userlisting',compact('userdata'));

    }

    public function detailusers($id){


        $user=Userprofile::select('user_name','email')->whereid($id)->first();
        $employement=Useremployment::select('*')->where('user_id',$id)->get();
        $employee=Useremployment::select('*')->where('user_id',$id)->first();
        $personal=Userpersonal::select('*')->where('user_id',$id)->first();
        $quote=Userquote::select('*')->whereuser_id($id)->first();
        $education=Usereducation::select('*')->where('user_id',$id)->get();
        $useredu=Usereducation::select('*')->where('user_id',$id)->first();
        $language=Userlanguage::select('*')->whereuser_id($id)->first();
        $skill=Userskill::select('*')->where('user_id',$id)->get();
        $jobpost=Jobpost::select('*')->where('user_id',$id)->get();
        /*$jobapply=Applyjob::select('*')->where('apply_user_id',$id)->get();*/
        $jobapply=Applyjobscreen::select('*')->where('apply_user_id',$id)->orderBy('created_at','dec')->get();
       /* $skill=$skilldata->toArray();*/
     
        return view('admin.User_profile_deatil',compact('user','employement','personal','quote','education','language','skill','jobpost','jobapply','employee','useredu'));
    }

   public function readmoredetail(Request $request,$id){
        $readmore = DB::table('user_job_post')
            ->where('user_job_post.id',$id)
            ->select('user_job_post.*')
            ->first();
/*echo"<pre>";
print_r($readmore);
die();*/
        $skill=DB::table('user_job_skill')
            ->where('user_job_skill.job_id',$id)
            ->select('user_job_skill.skills')
            ->get(); 
        $language=DB::table('user_jobpost_language')
            ->where('user_jobpost_language.job_id',$id)
            ->select('user_jobpost_language.job_language')
            ->get();
     return view('admin.readmore_deatil',compact('readmore','skill','language'));   


   }

public function deleteuser(Request $request,$id)
   {
       Userregister::whereid($id)->delete($id);
       Userpersonal::whereuser_id($id)->delete($id);
       Useremployment::whereuser_id($id)->delete($id);
       Userlanguage::whereuser_id($id)->delete($id);
       Userquote::whereuser_id($id)->delete($id);
       Userskill::whereuser_id($id)->delete($id);
        Session::flash('flash_ma','Record Deleted Succsessfully');
        return redirect('/User/Listing/Detail');
   }

}
