<?php

namespace App\Http\Controllers;

use App\Petsadd;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;
use Validator;
use stdClass;

class PetsaddController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $petname=Petsadd::select('*')->get();
        return view('admin.add_pets',compact('petname'));
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            $status=1;
            $data=Petsadd::select('pets_name')->where('pets_name',ucfirst($request['pet']))->get();
            $petname='';
            foreach ($data as $key => $value) {
                $petname=$value->pets_name;
                break;
            }
if($petname !=ucfirst($request['pet'])){
            Petsadd::create([

                'pets_name' =>ucfirst($request['pet']),
                '_token'    =>$request['_token'],
                'status'    =>$status,


            ]);
            Session::flash('flash_message','Pet Added Succsessfully');
            return redirect('/Pet/Add');
}else{
    Session::flash('flash_ma','This Pet Already Exist');
    return redirect('/Pet/Add');
}


    }

    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Petsadd  $petsadd
     * @return \Illuminate\Http\Response
     */
    public function edit($id,Petsadd $petsadd)
    {
        $edit_pet=Petsadd::select('*')->whereid($id)->first();
        return view('admin.update_pet',compact('edit_pet'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Petsadd  $petsadd
     * @return \Illuminate\Http\Response
     */
    public function update($id,Request $request, Petsadd $petsadd)
    {
        
 $status=1;
            $data=Petsadd::select('pets_name')->where('pets_name',ucfirst($request['pet']))->get();
            $petname='';
            foreach ($data as $key => $value) {
                $petname=$value->pets_name;
                break;
            }
if($petname !=ucfirst($request['pet'])){
            Petsadd::whereid($id)->update([

                'pets_name' =>ucfirst($request['pet']),
                '_token'    =>$request['_token'],
                'status'    =>$status,


            ]);
            Session::flash('flash_message','Pet Added Succsessfully');
            return back();
}else{
    Session::flash('flash_ma','This Pet Already Exist');
    return back();
}




    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Petsadd  $petsadd
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,Petsadd $petsadd)
    {
        Petsadd::whereid($id)->delete($id);
        Session::flash('flash_ma','Pets Deleted Succsessfully');
         return redirect('/Pet/Add');

    }


    /*************************for Api****************/

    public function petlist(){

        $select=Petsadd::select('id','pets_name','status')->get();
       // return response()->json($select);
    $obj=new stdClass();
        if(count($select)>0)
        {   
            $obj->message="successfully";
            $obj->status="1";
            $obj->data=json_encode($select);    
        }
        else
        {
            $obj->message="Unsuccess";
            $obj->status="0";
            $obj->data=null;
        }

        return response()->json($obj);
    }
}
