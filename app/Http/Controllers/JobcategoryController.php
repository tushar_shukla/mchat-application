<?php

namespace App\Http\Controllers;

use App\Jobcategory;
use App\Profession;
use App\Jobpost;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;
use Validator;
use stdClass;

class JobcategoryController extends Controller
{
    
    public function index()
    {
        $job=Jobcategory::select('*')->get();
        return view('admin.add_jobcategory',compact('job'));

    }


public function addjob(Request $request){
 $status=1;
       $validation=$this->validate($request, [
            'jobname' => 'required|max:100',
            ]);
    $data=Jobcategory::select('*')->where('job_category',$request['jobname'])->get();
    $job='';
    foreach ($data as $key => $val) {
        $job=$val->job_category;
        break;
    }
if($validation==true){
        if($job != $request['jobname']){
    Jobcategory::create([

        'job_category'   =>$request['jobname'],
        'status'   =>$status,


    ]);
        Session::flash('flash_message','Job Category Added Succsessfully');
        return redirect('/Add/Job/Category/');
     }else{
        Session::flash('flash_ma','Job Category Already Exist');
        return redirect('/Add/Job/Category/');
        }
}else{
    return redirect('/Add/Job/Category/');
}


}
    public function edit($id)
    {
         $job=Jobcategory::select('*')->whereid($id)->first();
        return view('admin.update_jobcategory',compact('job'));
    }


public function update_job($id,Request $request){

 $data=Jobcategory::select('*')->where('job_category',$request['jobname'])->get();
    $job='';
    foreach ($data as $key => $val) {
        $job=$val->job_category;
        break;
    }
if(!empty($request['jobname'])){
        $status=1;
     if($job != $request['jobname']){

        Jobcategory::whereid($id)->update([

              'job_category'   =>$request['jobname'],
               'status'   =>$status,


        ]);
    Session::flash('flash_message','Job Category Added Succsessfully');
    return back();
     }else{
        Session::flash('flash_ma','Job Category Already Exist');
        return back();
     }

}else{

         Session::flash('flash_ma','Can Not Blank Field Check Details');
        return back();
}

}


public function delete_id($id){
Jobcategory::whereid($id)->delete($id);
Session::flash('flash_ma','Job Category Deleted Succsessfully');
return back();

}

/*****************************profession Add*****************************/    
    public function professionindex(){
        $jobcat=Jobcategory::select('*')->get();
        $profession=Profession::select('*')->get();
        return view('admin.profession',compact('jobcat','profession'));
    }


/**************Add Prossion**********************/

public function addprofession(Request $request){
    $status=1;
    $data=Profession::select('*')->where('job_category_id',$request['jobname'])->where('profession',$request['professionname'])->get();
        $cate='';
        $profe='';
    foreach ($data as $key => $val) {
        $cate=$val->job_category_id;
        $profe=$val->profession;
        break;
    }

$validation=$this->validate($request, [
            'jobname' => 'required|max:40',
            'professionname' => 'required|max:150',
           
            ]);
if($validation==true){

    if($cate !=$request['jobname'] && $profe !=$request['professionname']){

        Profession::create([

            'job_category_id'=> $request['jobname'],
            'profession'     => ucfirst($request['professionname']),
            'status'           =>$status,
            '_token'           =>$request['_token'],

        ]);

         Session::flash('flash_message','Profession Added Succsessfully');
         return redirect('/Add/Job/Profession/');

    }else{
        Session::flash('flash_ma','Profession Already Exist');
         return redirect('/Add/Job/Profession/');
    }

}else{

    return redirect('/Add/Job/Profession/');
}

}

/***********************Edit &update******************************/

    public function viewprofession($id){
        $jobcat=Jobcategory::select('*')->get();
        $profession=Profession::select('*')->whereid($id)->first();
        return view('admin.update_profession',compact('profession','jobcat'));
    }


public function update_recode($id,Request $request){
    $status=1;
    $data=Profession::select('*')->where('job_category_id',$request['jobname'])->where('profession',$request['professionname'])->get();
        $cate='';
        $profe='';
    foreach ($data as $key => $val) {
        $cate=$val->job_category_id;
        $profe=$val->profession;
        break;
    }
  if($cate !=$request['jobname'] && $profe !=$request['professionname']){

        Profession::whereid($id)->update([
            'job_category_id'=> $request['jobname'],
            'profession'     => ucfirst($request['professionname']),
            'status'           =>$status,
            '_token'           =>$request['_token'],

        ]);

         Session::flash('flash_message','Profession Update Succsessfully');
         return back();

    }else{
        Session::flash('flash_ma','Profession Already Exist');
         return back();
    }  
       
}


    /********************delete profession*****************/

public function delete_profession($id){
        Profession::whereid($id)->delete($id);
        Session::flash('flash_ma','Profession Deleted Succsessfully');
         return redirect('/Add/Job/Profession/');
}

/******************Code For Api *********************/

public function joblist(){

$jobcat=Jobcategory::select('id','job_category','status')->get();

//return response()->json($jobcat);
$obj=new stdClass();
        if(count($jobcat)>0)
        {   
            $obj->message="successfully";
            $obj->status="1";
            $obj->data=json_encode($jobcat);    
        }
        else
        {
            $obj->message="Unsuccess";
            $obj->status="0";
            $obj->data=null;
        }

        return response()->json($obj);

}


public function professionapi(Request $request){

$jobpro=Profession::select('id','job_category_id','profession','status')->where('job_category_id',$request['job_category'])->get();
//return response()->json($jobpro);
$profession=new stdClass();
        if(count($jobpro)>0)
        {   
            $profession->message="successfully";
            $profession->status="1";
            $profession->data=json_encode($jobpro);    
        }
        else
        {
            $profession->message="Unsuccess";
            $profession->status="0";
            $profession->data=null;
        }

        return response()->json($profession);
}


/**********************All job Listing In admin pannel********************/

public function joblisting(){

    /*echo"rohit mishra";
    die();*/

    $joblist = DB::table('user_job_post')
            ->leftjoin('chat_user', 'user_job_post.user_id', '=', 'chat_user.id')
            ->leftjoin('abuse_job_report','user_job_post.id', '=', 'abuse_job_report.job_id')
            ->select('user_job_post.*','chat_user.user_name as username','abuse_job_report.job_id as abusejob')
            ->get();
  
 return view('admin.job_listing',compact('joblist'));

}



/*****************Job block by admin **********************/

protected function jobblock(Request $request,$id){
$status=0;
$publish=0;
$unstatus=1;
$unpublish=0;

$dataimp=Jobpost::select('*')->where('id',$id)->get();

$datastatus='';
$publish='';
foreach ($dataimp as $key => $value) {
    $datastatus=$value->status;
    $publish=$value->published;
    break;
}
if ($datastatus==1) {

    $update=Jobpost::whereid($id)->update([

        'published'=>$publish,
        'status'=>$status,

   ]);
   if ($update==true) {
       Session::flash('flash_message','Job block Succsessfully');
        return redirect('/User/job/Listing');
   }else{
         Session::flash('flash_ma','Something went wrong job not block');
         return redirect('/User/job/Listing');
   }
    
}elseif ($datastatus==0) {

    $update=Jobpost::whereid($id)->update([

        'published'=>$unpublish,
        'status'=>$unstatus,

   ]);
   if ($update==true) {
       Session::flash('flash_message','Job unblock Succsessfully');
        return redirect('/User/job/Listing');
   }else{
         Session::flash('flash_ma','Something went wrong job not unblock');
         return redirect('/User/job/Listing');
   }
    
}else{
            Session::flash('flash_ma','Something went wrong please try again');
         return redirect('/User/job/Listing');
}
   
}

}
