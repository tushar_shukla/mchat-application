<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Hash;
use Session;
use Auth;
use App\Userregister;
use App\Adminnotification;
use stdClass;

class UserloginapiController extends Controller
{
 

public function userlogin(Request $request){

			$firebase=$request['firebase_token'];
			$results=Userregister::select('*')->where('email',$request['email'])->get();
			$useremail='';
          $password='';	
          $status='';
		foreach ($results as $key => $val) {
				$password= $val->password;
				$useremail= $val->email;
				$username= $val->user_name;
				$status=$val->status;
					break;
      		}

      		
		
      	$isOk  = Hash::check($request['password'], $password);
if(!empty($request['email'])){
	if(!empty($request['password'])){
		if($status==1)
		{
	      if ($useremail==$request['email'] && $isOk==true) 
				{
					Userregister::whereemail($useremail)->update([
		 				 	 'firebase_token'	=>$firebase,	
		 				 ]);

					$data= Userregister::select('*')->where('email',$request['email'])->get();
			
		 				return response()->json($data);

				
		 		
				}else{
				    return response()->json("{message : Invalid user or password}");
					} 

						}else{

							 return response()->json("{message : Please verify your email id}");
						}
				}else{

					return response()->json("{message :Can not blank password}");
				}
		}else{

				return response()->json("{message :Can not blank email}");
			}
}



public function sociallogin(Request $request){

$results=Userregister::select('*')->where('email',$request['email'])->get();
			$useremail='';
          $password='';	
          $status='';
          $social_id='';
          $notistatus=1;
		foreach ($results as $key => $val) {
				$password= $val->password;
				$useremail= $val->email;
				$username= $val->user_name;
				$status=$val->status;
				$social_id=$val->social_id;
					break;
      		}
  $status=1;
			if($useremail ==$request['email'] && empty($social_id))
			{
					$value=Userregister::whereemail($request['email'])->update([              
                        'social_id'         => $request['social_id'],               
                    
                     ]);
				$insert=new stdClass();
						if($value==true)
							{
							 
								$data= Userregister::select('*')->where('email',$request['email'])->get();
							$obj=new stdClass();
							if(count($data)>0)
								{
								$obj->status="1";
								$obj->message="Thanks for login";
								$obj->data=json_encode($data);    
								}
								else
								{
								$obj->status="0";
								$obj->message="user data not found";
								}
							return response()->json($obj);
							
							}
							else
							{
							$insert->status="0";
							
							}
					
					
					
			}
	      elseif($useremail !=$request['email'] && $social_id != $request['social_id']) {
					
					$value=Userregister::create([
                    'user_name'       => $request['username'],
                    'email'         => $request['email'],               
                    'social_id'         => $request['social_id'],               
                    'password'         => Hash::make($request['password']),  
                    'status'          =>$status,
                 ]);
				$last_id= $value->id;
					
				$insert=new stdClass();
						if($value==true)
							{
							 
								$data= Userregister::select('*')->where('email',$request['email'])->get();
							$obj=new stdClass();
							if(count($data)>0)
								{
								$obj->status="1";
								$obj->message="Thanks for login";
								$obj->data=json_encode($data);    
								}
								else
								{
								$obj->status="0";
								$obj->message="user data not found";
								}
							return response()->json($obj);
							/* $notification=Adminnotification::create([
								'user_id'		  =>$last_id,
			                    'user_name'       =>$request['username'],
			                    'status'          =>$status,
			                    'not_seen'		  =>$notistatus
			                    
			                ]);
							$insert->status="1";
							$insert->data=json_encode($value); */ 
							
							}
							else
							{
							$insert->status="0";
							
							}
					return response()->json($insert);

				}elseif($useremail==$request['email'] && !empty($social_id) && $status==1) 
				{
					
					$data= Userregister::select('*')->where('email',$request['email'])->get();
					$obj=new stdClass();
						if(count($data)>0)
							{
							$obj->status="1";
							$obj->data=json_encode($data);    
							}
							else
							{
							$obj->status="0";
							$obj->data=null;
							}
					return response()->json($obj);
		 		}
				else{
					$obj=new stdClass();
					$obj->status="0";
					$obj->message='something went wrong please check deatils';
				    return response()->json($obj);
					} 
				
}


}
