<?php

namespace App\Http\Controllers;

use App\Jobpost;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;
use Session;
use View;
use Validator;
use App\Joblanguage;
use App\Jobscreening;
use App\Jobskill;
use App\Adminnotification;
use stdClass;
use App\Userregister;
use VideoThumbnail;

class JobpostController extends Controller
{

public function store(Request $request)
    {

    $userjob=Jobpost::select('*')->where('user_id',$request['user_id'])->get();
    
            $userid='';
    foreach ($userjob as $key => $value) {
             $userid=$value->user_id;
             break;
         }
        $status=1;
        $notistatus=1;
        $type='job';
       //$voice=$request['voice'];
       $video=$request['video'];   
       $published=0;  
       $draff=1;  
       $active=0;  
       $make_private=0; 

      $likeusername=Userregister::select('user_name')->where('id',$request['user_id'])->get();
        foreach ($likeusername as $key => $val) {
            $username=$val->user_name;
            break;
        }
        /*echo $username;
        die();*/
        $title="MChat";
        //$message=$username."posted new job in MChat";
       //echo $message;
       $message=$username." "."posted new job in MChat";
       /*echo $message;
        die();*/

if(!empty($request['user_id']) && !empty($request['video'])){
    /*$videoname = "postjobvideo/".time().'.'.request()->video->getClientOriginalExtension();*/
    $storageUrl=public_path('postjobvice/job_post_thumb');
    $filename=time().".png"; 
    $videoname = "postjobvice/".time().'.'.request()->video->getClientOriginalExtension();
    $baseurl = pathinfo($videoname, PATHINFO_EXTENSION);
    if($baseurl=='mp4'){
      request()->video->move(public_path('postjobvice'), $videoname);
      $homevideo=public_path($videoname);
      VideoThumbnail::createThumbnail($homevideo,$storageUrl,$filename,2,640,480);
      
            $job=Jobpost::create([

                'user_id'           =>$request['user_id'],
                'user_name'         =>$request['user_name'],
                /*'description'       =>$request['description'],*/
                'nationality'       =>$request['nationality'],
                'work_type'         =>$request['work_type'],
                'working_time'      =>$request['working_time'],
                'salery_range'      =>$request['salery_range'],
                'salery'            =>$request['salery'],
                'video'             =>$videoname,
                'video_thumb'       =>$filename,
                'location'          =>$request['location'],
                'public_holiday'    =>$request['public_holiday'],
                'leave_option'      =>$request['leave_option'],
                'if_option_other'   =>$request['if_option_other'],
                'chareterstics'     =>$request['chareterstics'],
                'start_date'        =>$request['start_date'],
                'status'            =>$status,
                'published'         =>$published,
                'draft'             =>$draff,
                'active'            =>$active,
                'make_private'      =>$make_private,
                'trip_home'         =>$request['trip_home'],
                'trip_home_other'   =>$request['trip_home_other'],
                'leaves'            =>$request['leaves'],
                

            ]);
            $jobid= $job->id;
          /*  if($job==true){
                 file_put_contents($path,base64_decode($voice));
                 file_put_contents($videopath,base64_decode($video));
                 //$file->move($path, $filename);
             }else{
                return response()->json("not upload");
             }*/
      if ($jobid > 0) {

            $notification=Adminnotification::create([
                    'post_job_id'     =>$jobid,
                    'post_job_user_id' =>$request['user_id'],
                    'status'          =>$status,
                    'not_seen'      =>$notistatus,
                    'type'          =>$type
                    
                ]);



            $data=$request['job_language'];
            $language = explode(',', $data);
            $leveldata=$request['lang_level'];
            $level = explode(',', $leveldata);
            
            $jobcount=count($language);
            for ($i=0; $i < $jobcount ; $i++) { 
               
               $joblang=Joblanguage::create([
                
                'job_id'            =>$jobid,
                'user_id'           =>$request['user_id'],
                'language_position' =>$level[$i],
                'job_language'      =>$language[$i],
                'status'            =>$status,
                
                 ]);  
            }
              
        
            $dataskills=$request['skills'];
            
            $rr1= str_replace('[','',$dataskills);
            $rr= str_replace(']','',$rr1);
            $skilldata = explode(',', $rr);
            

            $totskill=count($skilldata);
        for ($i=0; $i < $totskill ; $i++) { 
            $jobskill=Jobskill::create([

                'job_id'         =>$jobid,
                'user_id'        =>$request['user_id'],
                'skills'         =>$skilldata[$i],
                'status'         =>$status,
            ]); 
        }
     
           $screen=$request['sreening_question'];
            $screrendata= explode(',', $screen);
            $totscreen=count($screrendata);
        for ($i=0; $i < $totscreen; $i++) { 
          
                $jobscreen=Jobscreening::create([

                'job_id'               =>$jobid,
                'user_id'              =>$request['user_id'],
                'sreening_question'    =>$screrendata[$i],
                'status'               =>$status,

        ]); 



        }
      
             $inserted=new stdClass();
         if($jobscreen==true){

             $userdata=Userregister::select('firebase_token')->get();

                $array=array();
                foreach ($userdata as $key => $value) {
                    $token=$value->firebase_token;
                    array_push($array,$token);
                }
                 $message=$username." "."posted new job in MChat";
                  if ($message !='') {

                     $msg=$this->notification($array,$title,$message);
                  }
                                
                $inserted->status="1";
                $inserted->message="Job post data inserted";
                
               return response()->json($inserted);
               
           }else{
                    $inserted->status="0";
                    $inserted->message="job post data Not Inserted";
                    return response()->json($updateed);
           }
          
      }
    }elseif($baseurl=='mp3'){
      request()->video->move(public_path('postjobvice'), $videoname);
            $job=Jobpost::create([

                'user_id'           =>$request['user_id'],
                'user_name'         =>$request['user_name'],
               /* 'description'       =>$request['description'],*/
                'nationality'       =>$request['nationality'],
                'work_type'         =>$request['work_type'],
                'working_time'      =>$request['working_time'],
                'salery_range'      =>$request['salery_range'],
                'salery'            =>$request['salery'],
                'voice'             =>$videoname,
                'location'          =>$request['location'],
                'public_holiday'    =>$request['public_holiday'],
                'leave_option'      =>$request['leave_option'],
                'if_option_other'   =>$request['if_option_other'],
                'chareterstics'     =>$request['chareterstics'],
                'start_date'        =>$request['start_date'],
                'status'            =>$status,
                'published'         =>$published,
                'draft'             =>$draff,
                'active'            =>$active,
                'make_private'      =>$make_private,
                'trip_home'         =>$request['trip_home'],
                'trip_home_other'   =>$request['trip_home_other'],
                'leaves'            =>$request['leaves'],
                

            ]);
            $jobid= $job->id;
          /*  if($job==true){
                 file_put_contents($path,base64_decode($voice));
                 file_put_contents($videopath,base64_decode($video));
                 //$file->move($path, $filename);
             }else{
                return response()->json("not upload");
             }*/
      if ($jobid > 0) {

            $notification=Adminnotification::create([
                    'post_job_id'     =>$jobid,
                    'post_job_user_id' =>$request['user_id'],
                    'status'          =>$status,
                    'not_seen'      =>$notistatus,
                    'type'          =>$type
                    
                ]);



            $data=$request['job_language'];
            $language = explode(',', $data);
            $leveldata=$request['lang_level'];
            $level = explode(',', $leveldata);
            
            $jobcount=count($language);
            for ($i=0; $i < $jobcount ; $i++) { 
               
               $joblang=Joblanguage::create([
                
                'job_id'            =>$jobid,
                'user_id'           =>$request['user_id'],
                'language_position' =>$level[$i],
                'job_language'      =>$language[$i],
                'status'            =>$status,
                
                 ]);  
            }
              
        
            $dataskills=$request['skills'];
            
            $rr1= str_replace('[','',$dataskills);
            $rr= str_replace(']','',$rr1);
            $skilldata = explode(',', $rr);
            

            $totskill=count($skilldata);
        for ($i=0; $i < $totskill ; $i++) { 
            $jobskill=Jobskill::create([

                'job_id'         =>$jobid,
                'user_id'        =>$request['user_id'],
                'skills'         =>$skilldata[$i],
                'status'         =>$status,
            ]); 
        }
     
           $screen=$request['sreening_question'];
            $screrendata= explode(',', $screen);
            $totscreen=count($screrendata);
        for ($i=0; $i < $totscreen; $i++) { 
          
                $jobscreen=Jobscreening::create([

                'job_id'               =>$jobid,
                'user_id'              =>$request['user_id'],
                'sreening_question'    =>$screrendata[$i],
                'status'               =>$status,

        ]); 



        }
      
             $inserted=new stdClass();
         if($jobscreen==true){

               $userdata=Userregister::select('firebase_token')->get();
                $array=array();
                foreach ($userdata as $key => $value) {
                    $token=$value->firebase_token;
                    array_push($array,$token);
                }
                 $message=$username." "."posted new job in MChat";
                  if ($message !='') {

                     $msg=$this->notification($array,$title,$message);
                  }
            
                $inserted->status="1";
                $inserted->message="Job post data inserted";
                
               return response()->json($inserted);
               
           }else{
                    $inserted->status="0";
                    $inserted->message="job post data Not Inserted";
                    return response()->json($updateed);
           }
          
      }
    }
    }else{
        $job=Jobpost::create([

                'user_id'           =>$request['user_id'],
                'user_name'         =>$request['user_name'],
                'description'       =>$request['description'],
                'nationality'       =>$request['nationality'],
                'work_type'         =>$request['work_type'],
                'working_time'      =>$request['working_time'],
                'salery_range'      =>$request['salery_range'],
                'salery'            =>$request['salery'],
                'location'          =>$request['location'],
                'public_holiday'    =>$request['public_holiday'],
                'leave_option'      =>$request['leave_option'],
                'if_option_other'   =>$request['if_option_other'],
                'chareterstics'     =>$request['chareterstics'],
                'start_date'        =>$request['start_date'],
                'status'            =>$status,
                'published'         =>$published,
                'draft'             =>$draff,
                'active'            =>$active,
                'make_private'      =>$make_private,
                'trip_home'         =>$request['trip_home'],
                'trip_home_other'   =>$request['trip_home_other'],
                'leaves'            =>$request['leaves'],
                

            ]);
            $jobid= $job->id;
          /*  if($job==true){
                 file_put_contents($path,base64_decode($voice));
                 file_put_contents($videopath,base64_decode($video));
                 //$file->move($path, $filename);
             }else{
                return response()->json("not upload");
             }*/
      if ($jobid > 0) {

            $notification=Adminnotification::create([
                    'post_job_id'     =>$jobid,
                    'post_job_user_id' =>$request['user_id'],
                    'status'          =>$status,
                    'not_seen'      =>$notistatus,
                    'type'          =>$type
                    
                ]);



            $data=$request['job_language'];
            $language = explode(',', $data);
            $leveldata=$request['lang_level'];
            $level = explode(',', $leveldata);
            
            $jobcount=count($language);
            for ($i=0; $i < $jobcount ; $i++) { 
               
               $joblang=Joblanguage::create([
                
                'job_id'            =>$jobid,
                'user_id'           =>$request['user_id'],
                'language_position' =>$level[$i],
                'job_language'      =>$language[$i],
                'status'            =>$status,
                
                 ]);  
            }
              
        
            $dataskills=$request['skills'];
            
            $rr1= str_replace('[','',$dataskills);
            $rr= str_replace(']','',$rr1);
            $skilldata = explode(',', $rr);
            

            $totskill=count($skilldata);
        for ($i=0; $i < $totskill ; $i++) { 
            $jobskill=Jobskill::create([

                'job_id'         =>$jobid,
                'user_id'        =>$request['user_id'],
                'skills'         =>$skilldata[$i],
                'status'         =>$status,
            ]); 
        }
     
           $screen=$request['sreening_question'];
            $screrendata= explode(',', $screen);
            $totscreen=count($screrendata);
        for ($i=0; $i < $totscreen; $i++) { 
          
                $jobscreen=Jobscreening::create([

                'job_id'               =>$jobid,
                'user_id'              =>$request['user_id'],
                'sreening_question'    =>$screrendata[$i],
                'status'               =>$status,

        ]); 



        }
      
             $inserted=new stdClass();
         if($jobscreen==true){

                $userdata=Userregister::select('firebase_token')->get();
                $array=array();
                foreach ($userdata as $key => $value) {
                    $token=$value->firebase_token;
                    array_push($array,$token);
                }
                 $message=$username." "."posted new job in MChat";
                  if ($message !='') {

                     $msg=$this->notification($array,$title,$message);
                  }
            
                $inserted->status="1";
                $inserted->message="Job post data inserted";
                
               return response()->json($inserted);
               
           }else{
                    $inserted->status="0";
                    $inserted->message="job post data Not Inserted";
                    return response()->json($updateed);
           }
          
      }
    }

}

/*************************posted job notification********************/

public function notification($token, $title,$message)
    {
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
        $token=$token;
    
        $message=$message;

        $notification = [
            'title' => $title,
            'message' => $message,
            'sound' => true,
        ];
        
        $extraNotificationData = ["message" => $notification,"moredata" =>'dd'];

        $fcmNotification = [
            'registration_ids' => $token, //multple token array
            //'to'        => $token, //single token
            'alert' => $notification,
            'data' => $extraNotificationData
        ];

        $headers = [
            'Authorization: key=AIzaSyAuqmMsfQtmxrwoujo5qcu5Ix5FQG-4TEU',
            'Content-Type: application/json'
        ];


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        
        /*echo json_encode($fcmNotification);
        die();*/
        $result = curl_exec($ch);
         if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
        //dd($token);
        /*dd($notification);
        die();*/
        return $result;
    }

/*************************end posted job notification********************/

/*******************Job Posting List Api*********************/

public function jobpstinglist(Request $request){

            $user_id=$request['user_id'];
            $jobid=$request['job_id'];
if(!empty($jobid) && !empty($user_id)){
             $data="SELECT chat_user.email,job.id as jobid,job.user_id,job.user_name,job.description,job.nationality, job.working_time,job.work_type,job.salery_range, job.text,job.voice,job.video,job.video_thumb,job.location,job.public_holiday,job.leave_option,job.if_option_other,job.chareterstics,job.start_date,job.status,job.published,job.draft,job.active,job.make_private,job.trip_home,job.trip_home_other,job.leaves,job.salery,group_concat(DISTINCT userskill.skills) as userskill,group_concat(DISTINCT userlang.job_language)as joblanguage,group_concat(DISTINCT ques.id) as screening_id,group_concat(DISTINCT ques.sreening_question)as screing_question FROM user_job_post job LEFT JOIN user_job_skill userskill ON job.id=userskill.job_id AND job.user_id=userskill.user_id LEFT JOIN user_jobpost_language userlang ON job.id=userlang.job_id AND job.user_id=userlang.user_id LEFT JOIN job_screening_que ques ON job.id=ques.job_id AND job.user_id=ques.user_id LEFT JOIN chat_user ON job.user_id=chat_user.id where job.user_id='$user_id' and job.id='$jobid' AND job.published='1' GROUP BY job.user_id,job.description,job.nationality, job.working_time,job.work_type,job.salery_range, job.text,job.voice,job.video,job.video_thumb,job.location,job.public_holiday,job.leave_option,job.if_option_other,job.location,job.public_holiday,job.leave_option,job.if_option_other,job.chareterstics,job.start_date,job.status,job.published,job.draft,job.active,job.make_private,job.trip_home,job.trip_home_other,job.leaves,job.id,job.user_name,job.salery,chat_user.email";


      $jobdata=DB::select($data);
       $data=new stdClass;
               $data->status="1";
               $data->message="personal all data";
               $data->data= json_encode($jobdata);
               return response()->json($data);
  


    }elseif(!empty($user_id)){
        $data="SELECT chat_user.email,job.id as jobid,job.user_id,job.user_name,job.description,job.nationality, job.working_time,job.work_type,job.salery_range, job.text,job.voice,job.video,job.video_thumb,job.location,job.public_holiday,job.leave_option,job.if_option_other,job.chareterstics,job.start_date,job.status,job.published,job.draft,job.active,job.make_private,job.trip_home,job.trip_home_other,job.leaves,job.salery,group_concat(DISTINCT userskill.skills) as userskill,group_concat(DISTINCT userlang.job_language)as joblanguage,group_concat(DISTINCT ques.id) as screening_id,group_concat(DISTINCT ques.sreening_question)as screing_question FROM user_job_post job LEFT JOIN user_job_skill userskill ON job.id=userskill.job_id AND job.user_id=userskill.user_id LEFT JOIN user_jobpost_language userlang ON job.id=userlang.job_id AND job.user_id=userlang.user_id LEFT JOIN job_screening_que ques ON job.id=ques.job_id AND job.user_id=ques.user_id LEFT JOIN chat_user ON job.user_id=chat_user.id where job.user_id='$user_id' GROUP BY job.user_id,job.description,job.nationality, job.working_time,job.work_type,job.salery_range, job.text,job.voice,job.video,job.location,job.public_holiday,job.leave_option,job.if_option_other,job.chareterstics,job.start_date,job.status,job.published,job.draft,job.active,job.make_private,job.trip_home,job.trip_home_other,job.leaves,job.id,job.user_name,job.salery,chat_user.email,job.video_thumb";


      $jobdata=DB::select($data);
       $data=new stdClass;
               $data->status="1";
               $data->message="personal all data";
               $data->data= json_encode($jobdata);
               return response()->json($data);
    }else{

      $data="SELECT chat_user.email,group_concat(distinct job_total_like.like_user_id) as likejob_id,count(job_total_like.like_user_id) as joblike,job.id as jobid,job.user_id,chat_user.user_name as postedby,job.user_name,job.description,job.nationality, job.working_time,job.work_type,job.salery_range, job.text,job.voice,job.video,job.video_thumb,job.location,job.public_holiday,job.leave_option,job.if_option_other,job.chareterstics,job.start_date,job.created_at as jobposted_date,job.status,job.published,job.draft,job.active,job.make_private,job.trip_home,job.trip_home_other,job.leaves,job.salery,group_concat(DISTINCT userskill.skills) as userskill,group_concat(DISTINCT userlang.job_language)as joblanguage,group_concat(DISTINCT ques.id) as screening_id ,group_concat(DISTINCT ques.sreening_question)as screing_question,user_personal_info.contact as contact FROM user_job_post job LEFT JOIN user_job_skill userskill ON job.id=userskill.job_id AND job.user_id=userskill.user_id LEFT JOIN user_jobpost_language userlang ON job.id=userlang.job_id AND job.user_id=userlang.user_id LEFT JOIN job_screening_que ques ON job.id=ques.job_id AND job.user_id=ques.user_id LEFT JOIN job_total_like ON job_total_like.user_id=job.user_id LEFT JOIN user_personal_info ON job.user_id=user_personal_info.user_id LEFT JOIN chat_user ON chat_user.id=job.user_id WHERE job.published='1' GROUP BY job.user_id,job.description,job.nationality, job.working_time,job.work_type,job.salery_range, job.text,job.voice,job.video,job.location,job.public_holiday,job.leave_option,job.if_option_other,job.chareterstics,job.start_date,job.status,job.published,job.trip_home,job.trip_home_other,job.leaves,job.id,job.user_name,user_personal_info.contact,job.draft,job.active,job.make_private,job.salery,chat_user.user_name,job.created_at,chat_user.email,job.video_thumb";


      $jobdata=DB::select($data);
     /* echo $data;
      die();*/
       $dataval=new stdClass;
               $dataval->status="1";
               $dataval->message="personal all data";
               $dataval->data= json_encode($jobdata);
               return response()->json($dataval);

    }

          

}


  /**********************delete job****************************/
public function delete_posted_job(Request $request){
$jobid=$request['job_id'];
$user_id=$request['user_id'];
$obj=new stdClass;
if (!empty($jobid) && !empty($user_id)) {

  $select="DELETE job,jobskill,joblanguage,jobscreen FROM user_job_post job LEFT JOIN user_job_skill jobskill ON job.id=jobskill.job_id AND job.user_id=jobskill.user_id LEFT JOIN user_jobpost_language joblanguage ON joblanguage.job_id=job.id AND joblanguage.user_id=job.user_id LEFT JOIN job_screening_que jobscreen ON jobscreen.job_id=job.id AND job.user_id=jobscreen.user_id WHERE job.id='$jobid' AND job.user_id='$user_id'";
  $delete=DB::delete($select);
    if($delete==true){
        $joblike="DELETE job_total_like FROM job_total_like WHERE job_id='$jobid'";
        $jobdelete=DB::delete($joblike);
        $obj->status='1';
        $obj->message='Job deleted successfully';
    }else{
        $obj->status='0';
        $obj->message='Something went wrong please try again';
    }

    return json_encode($obj);
}else{
        $obj->status='0';
        $obj->message='Can not blank user id and job id';
        return json_encode($obj);
}


}



/**********************Repost job***********************/

public function repostjob(Request $request){

  $jobid=$request['job_id'];
  $userid=$request['user_id'];
   $type='job';
   $title="MChat";
if (!empty($jobid) && !empty($userid)) {
 
  $likeusername=Userregister::select('user_name')->where('id',$request['user_id'])->get();
        foreach ($likeusername as $key => $val) {
            $username=$val->user_name;
            break;
        }
        
       $message=$username." "."posted new job in MChat";
       
   $data="SELECT job.id as jobid,job.user_id,job.user_name,job.description,job.nationality, job.working_time,job.work_type,job.salery_range, job.text,job.voice,job.video,job.video_thumb,job.location,job.public_holiday,job.leave_option,job.if_option_other,job.chareterstics,job.start_date,job.status,job.published,job.draft,job.active,job.make_private,job.trip_home,job.trip_home_other,job.leaves,job.salery,group_concat(DISTINCT userskill.skills) as userskill,group_concat(DISTINCT userlang.job_language)as joblanguage,group_concat(DISTINCT userlang.language_position) as postion,group_concat(DISTINCT ques.id) as screening_id,group_concat(DISTINCT ques.sreening_question)as screing_question FROM user_job_post job LEFT JOIN user_job_skill userskill ON job.id=userskill.job_id AND job.user_id=userskill.user_id LEFT JOIN user_jobpost_language userlang ON job.id=userlang.job_id AND job.user_id=userlang.user_id LEFT JOIN job_screening_que ques ON job.id=ques.job_id AND job.user_id=ques.user_id where job.user_id='$userid' and job.id='$jobid' GROUP BY job.user_id,job.description,job.nationality, job.working_time,job.work_type,job.salery_range, job.text,job.voice,job.video,job.location,job.public_holiday,job.leave_option,job.if_option_other,job.chareterstics,job.start_date,job.status,job.published,job.draft,job.active,job.make_private,job.trip_home,job.trip_home_other,job.leaves,job.id,job.user_name,job.salery,job.video_thumb";
 
          
      $jobdata=DB::select($data);
     
      foreach ($jobdata as $key => $value) {
          $jobid=$value->jobid;
          $userid=$value->user_id;
          $user_name=$value->user_name;
          $description=$value->description;
          $nationality=$value->nationality;
          $workingtime=$value->working_time;
          $worktype=$value->work_type;
          $salaryrange=$value->salery_range;
          $text =$value->text;
          $voice=$value->voice;
          $video=$value->video;
          $video_thumb=$value->video_thumb;
          $location=$value->location;
          $holiday=$value->public_holiday;
          $leaveoption=$value->leave_option;
          $ifother=$value->if_option_other;
          $charecterstics=$value->chareterstics;
          $startdate=$value->start_date;
          $status=$value->status;
          $publish=$value->published;
          $draff=$value->draft;
          $active=$value->active;
          $make_private=$value->make_private;
          $trip_home=$value->trip_home;
          $leaves=$value->leaves;
          $salery=$value->salery;
          $trip_home_other=$value->trip_home_other;
          $userskill=$value->userskill;
          $joblanguage=$value->joblanguage;
          $language_position =$value->postion;
          $screenid=$value->screening_id;
          $screen_question=$value->screing_question;
      }
  $notistatus=1;
  $publishjob=0;
  $draffjob=1;
  
$job=Jobpost::create([

                'user_id'           =>$userid,
                'user_name'         =>$user_name,
                'description'       =>$description,
                'nationality'       =>$nationality,
                'work_type'         =>$worktype,
                'working_time'      =>$workingtime,
                'salery_range'      =>$salaryrange,
                'salery'            =>$salery,
                'video'             =>$video,
                'video_thumb'       =>$video_thumb,
                'voice'             =>$voice,
                'location'          =>$location,
                'public_holiday'    =>$holiday,
                'leave_option'      =>$leaveoption,
                'if_option_other'   =>$ifother,
                'chareterstics'     =>$charecterstics,
                'start_date'        =>$startdate,
                'status'            =>$status,
                'published'         =>$publishjob,
                'draft'             =>$draffjob,
                'active'            =>$active,
                'make_private'      =>$make_private,
                'trip_home'         =>$trip_home,
                'trip_home_other'   =>$trip_home_other,
              ]);
            $jobid= $job->id;
if($jobid > 0)
{
            $notification=Adminnotification::create([
                    'post_job_id'     =>$jobid,
                    'post_job_user_id' =>$userid,
                    'status'          =>$status,
                    'not_seen'      =>$notistatus,
                    'type'          =>$type
                    
                ]);

            $data=$joblanguage;
            $language = explode(',', $data);
            $leveldata=$language_position;
            $level = explode(',', $leveldata);
            
            $jobcount=count($language);
            for ($i=0; $i < $jobcount ; $i++) { 
               
               $joblang=Joblanguage::create([
                
                'job_id'            =>$jobid,
                'user_id'           =>$userid,
                'language_position' =>$level[$i],
                'job_language'      =>$language[$i],
                'status'            =>$status,
                
                 ]);  
            }

            $skilldata = explode(',', $userskill);
            $totskill=count($skilldata);
        for ($i=0; $i < $totskill ; $i++) { 
            $jobskill=Jobskill::create([

                'job_id'         =>$jobid,
                'user_id'        =>$userid,
                'skills'         =>$skilldata[$i],
                'status'         =>$status,
            ]); 
        }

            $screen=$screen_question;
            $screrendata= explode(',', $screen);
            $totscreen=count($screrendata);
        for ($i=0; $i < $totscreen; $i++) { 
          
                $jobscreen=Jobscreening::create([

                'job_id'               =>$jobid,
                'user_id'              =>$request['user_id'],
                'sreening_question'    =>$screrendata[$i],
                'status'               =>$status,

        ]); 
   }

    $inserted=new stdClass();
         if($jobscreen==true){ 

          $userdata=Userregister::select('firebase_token')->get();
                $array=array();
                foreach ($userdata as $key => $value) {
                    $token=$value->firebase_token;
                    array_push($array,$token);
                }
                 $message=$username." "."posted new job in MChat";
                  if ($message !='') {

                     $msg=$this->notification($array,$title,$message);
                  }

                $inserted->status="1";
                $inserted->message="Job posted successfully";
                
               return response()->json($inserted);
               
           }else{
                    $inserted->status="0";
                    $inserted->message="Something went wrong please try again ";
                    return response()->json($updateed);
           }
    }

}

}

   
}
