<?php

namespace App\Http\Controllers;

use App\Ratingandlike;
use App\Userlike;
use App\Joblike;
use Session;
use stdClass;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;
use App\Userregister;
use VideoThumbnail;

class RatingandlikeController extends Controller
{
 

/********************add user rating****************/
public function addreating(Request $request){
    $comment=$request['comment'];        
    $user_id=$request['user_id'];
    $rating_user_id=$request['rating_user_id'];        
    $video=$request['video']; 
    $discipline=$request['discipline'];
   $responsbility=$request['responsbility'];
   $cleanness=$request['cleanness_and_tidy'];
   $communication=$request['communication'];
   $patient=$request['patient'];
   $experience=$request['experience'];
   $attitude=$request['attitude'];
   $laugh=$request['laugh'];
   $smile=$request['smile'];
   $sad=$request['sad'];
   /*echo $user_id;
   die();*/
        
       $usertoken=Userregister::select('firebase_token')->where('id',$user_id)->get();
        foreach ($usertoken as $key => $value) {
            $token=$value->firebase_token;
           
            break;
        }
       
    $likeusername=Userregister::select('user_name')->where('id',$rating_user_id)->get();
        foreach ($likeusername as $key => $val) {
            $username=$val->user_name;
            break;
        }

         //$username='';
         //$token='';
        /* echo $token;
         die();*/
        $title="MChat";
        $message=$username." given rating on your profile";
       /* echo $message;
        die();*/
       
    $rating=Ratingandlike::select('*')->where('user_id',$user_id)->where('rating_user_id',$rating_user_id)->get();
                $userid='';
                $ratinguser='';
            foreach ($rating as $key => $value) {
               $userid=$value->user_id;
               $ratinguser=$value->rating_user_id;
            }
   if ($userid==$user_id && $ratinguser==$rating_user_id)
     {

            $inserted= new stdClass();
            
            $inserted->status="0";
            $inserted->message="Alredy given rating";
             return response()->json($inserted);
    }
   else if(!empty($user_id) && !empty($rating_user_id) && !empty($request['video']))
         {
        $storageUrl=public_path('ratingvideo/rating_thumb');
        $filename=time().".png"; 
        $videoname = "ratingvideo/".time().'.'.request()->video->getClientOriginalExtension();
        $baseurl = pathinfo($videoname, PATHINFO_EXTENSION); 
         if($baseurl=='mp4')
         {
        request()->video->move(public_path('ratingvideo'), $videoname);
        $homevideo=public_path($videoname);
        VideoThumbnail::createThumbnail($homevideo,$storageUrl,$filename,2,640,480);

           $insertrating=Ratingandlike::create([
                    'user_id'             =>$user_id,
                    'rating_user_id'      =>$rating_user_id,
                    'responsbility'       =>$responsbility,
                    'cleanness_and_tidy'  =>$cleanness,
                    'communication'       =>$communication,
                    'patient'             =>$patient,
                    'experience'          =>$experience,
                    'attitude'            =>$attitude,
                    'laugh'               =>$laugh,
                    'smile'               =>$smile,
                    'sad'                 =>$sad,
                    'discipline'          =>$discipline,
                    'video'               =>$videoname,
                    'video_thumb'         =>$filename,
                    ]);
                     if($insertrating==true)
                     {
                        if ($token !='' && $message !='') {
                           $this->notification($token,$title,$message);
                        }
                        
                        $inserted= new stdClass();
                            
                            $inserted->status="1";
                            $inserted->message="rating inserted";
                             return response()->json($inserted);
        
                     }else{

                             $inserted= new stdClass();
                            
                            $inserted->status="0";
                            $inserted->message="not insert";
                             return response()->json($inserted);
                            }
                
        }elseif ($baseurl=='mp3')
                {
                    request()->video->move(public_path('ratingvideo'), $videoname);
           $insertrating=Ratingandlike::create([
                    'user_id'             =>$user_id,
                    'rating_user_id'      =>$rating_user_id,
                    'responsbility'       =>$responsbility,
                    'cleanness_and_tidy'  =>$cleanness,
                    'communication'       =>$communication,
                    'patient'             =>$patient,
                    'experience'          =>$experience,
                    'attitude'            =>$attitude,
                     'laugh'              =>$laugh,
                    'smile'               =>$smile,
                    'sad'                 =>$sad,
                    'discipline'          =>$discipline,
                    'voice'             =>$videoname,
                    ]);
                     if($insertrating==true)
                     {
                        if ($token !='' && $message !='') {
                           $this->notification($token,$title,$message);
                        }
                           $inserted= new stdClass();
                            
                            $inserted->status="1";
                            $inserted->message="rating inserted";
                             return response()->json($inserted);
        
                     }else{

                             $inserted= new stdClass();
                            
                            $inserted->status="0";
                            $inserted->message="not insert";
                             return response()->json($inserted);
                            }
                }
       

        }else{
            $insertrating=Ratingandlike::create([
                    'user_id'             =>$user_id,
                    'rating_user_id'      =>$rating_user_id,
                    'responsbility'       =>$responsbility,
                    'cleanness_and_tidy'  =>$cleanness,
                    'communication'       =>$communication,
                    'patient'             =>$patient,
                    'experience'          =>$experience,
                    'attitude'            =>$attitude,
                    'laugh'               =>$laugh,
                    'smile'               =>$smile,
                    'sad'                 =>$sad,
                    'discipline'          =>$discipline,
                    'comment'             =>$comment,
                    ]);
                     if($insertrating==true)
                     {
                         /*echo $token;
                        die();*/
                        if ($token !='' && $message !='') {


                           $msg=$this->notification($token,$title,$message);
                        }
                           $inserted= new stdClass();
                            
                            $inserted->status="1";
                            $inserted->message="rating inserted";
                             return response()->json($inserted);
        
                     }else{

                             $inserted= new stdClass();
                            
                            $inserted->status="0";
                            $inserted->message="not insert";
                             return response()->json($inserted);
                            }

                }

}

/*********************Send Notification  Rating  Comment Section******************/
public function notification($token, $title,$message)
    {
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
        $token=$token;
    
        $message=$message;

        $notification = [
            'title' => $title,
            'message' => $message,
            'sound' => true,
        ];
        
        $extraNotificationData = ["message" => $notification,"moredata" =>'dd'];

        $fcmNotification = [
            //'registration_ids' => $tokenList, //multple token array
            'to'        => $token, //single token
            'alert' => $notification,
            'data' => $extraNotificationData
        ];

        $headers = [
            /*'Authorization: key=AAAAzUDFKz4:APA91bGoodhudte22xeE776L5XOWDOaDdRL5jmcByLCv2NsJcRo0jO3vyLu1zj_sbbuIYifovyRpq7rv6RYvQP3Tt-J8VfmqtqBo8cKaJdafV8l7HQEEpJwgWm3S_8TSWsAAyQ419nmMcFTv1tCDzZRFV4DNRT8glA',*/
            'Authorization: key=AIzaSyAuqmMsfQtmxrwoujo5qcu5Ix5FQG-4TEU',
            'Content-Type: application/json'
        ];


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        
       /* echo json_encode($fcmNotification);
        die();*/
        $result = curl_exec($ch);
     /*   echo $result;
        die();*/
         if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
      /*  
        dd($notification);
        die();*/
    //echo $result;
        return $result;
    }

/********************User rating list**************/
public function totalrating(Request $request){

    $user_id=$request['user_id'];
   $query="Select GROUP_CONCAT(DISTINCT comment) as comment, (avg(comment)+avg(discipline)+avg(responsbility)+avg(cleanness_and_tidy)+avg(communication)+avg(patient)+avg(experience)+avg(attitude))/5 as Average,SUM(laugh) as laugh,SUM(smile)as smile,SUM(sad) as sad from user_total_rating WHERE user_id='$user_id' Group by user_id";
   $price=DB::select($query);
            /*$price = DB::table('user_total_rating')
                ->where('user_id', $user_id)
                ->avg('rating_value');*/
$rating= new stdClass;
        if ($user_id==true) {
            $rating->status="1";
            $rating->message="fetch data user rating wise";
            $rating->data= $price;
            $rating->data= $price;
        }else{
            $rating->status="0";
            $rating->message="not any data";
        }
    return json_encode($rating);


}


/**************************Add Like User**********************/

public function addlike(Request $request){

$user_id=$request['user_id'];
$like_user_id=$request['like_user_id'];
$usertoken=Userregister::select('firebase_token')->where('id',$user_id)->get();
        foreach ($usertoken as $key => $value) {
            $token=$value->firebase_token;
           
            break;
        }
   /* echo $token;
    die();  */
    $likeusername=Userregister::select('user_name')->where('id',$like_user_id)->get();
        foreach ($likeusername as $key => $val) {
            $username=$val->user_name;
            break;
        }

         //$username='';
         //$token='';
        /* echo $token;
         die();*/
        $title="MChat";
        $message=$username." like your profile";
       /* echo $message;
        die();*/


$userlike=Userlike::select('*')->where('user_id',$request['user_id'])->where('like_user_id',$request['like_user_id'])->get();

$userid='';
$likeuser='';
foreach ($userlike as $key => $value) {
    
   $userid=$value->user_id;
   $likeuser=$value->like_user_id;
}

if (!empty($request['user_id']) && !empty($request['like_user_id'])) {

        if($userid !=$request['user_id'] && $likeuser != $request['like_user_id']){
             $like=Userlike::create([

                 'user_id'          =>$request['user_id'],
                 'like_user_id'     =>$request['like_user_id'],

            ]);

             $inserted= new stdClass();
             if($like==true){
                if ($token !='' && $message !='') {
                           $this->notification($token,$title,$message);
                        }
                    $inserted->status="1";
                    $inserted->message="User like profile";
             }else{
                $inserted->status="0";
                $inserted->message="User like not inserted";
             }
                return response()->json($inserted);
        }else{

            $delete=Userlike::whereuser_id($request['user_id'])->wherelike_user_id($request['like_user_id'])->delete($userid);
            if ($delete==true) {
               $inserted= new stdClass();
                $inserted->status="0";
                $inserted->message="unlike prifile";
                return response()->json($inserted);
            }
                

        }
           

}
   
}

/**********************User Like total list***********************/

public function totallike(Request $request){

            $totallike=Userlike::where('user_id',$request['user_id'])->count('user_id');
            $inserted= new stdClass();
             if($totallike > 0){
                    $inserted->status="1";
                    $inserted->message="User total like ";
                    $inserted->data=json_encode($totallike);
             }else{
                $inserted->status="0";
                $inserted->message="not any like";
             }
                return response()->json($inserted);
}


/*******************like wise user listing*********************/
public function userlist_likewise(Request $request){
$user_id=$request['user_id'];

if (!empty($user_id)) {

    $usersname = DB::table('user_total_like')
            ->leftjoin('chat_user', 'user_total_like.like_user_id', '=', 'chat_user.id')
            ->leftjoin('user_personal_info','user_personal_info.user_id','=','chat_user.id')
            ->select('user_total_like.user_id','user_personal_info.image','chat_user.id', 'chat_user.user_name')
            ->where('user_total_like.user_id',$user_id)
            ->get();
    $user='';        
foreach ($usersname as $key => $value) {
    $user=$value->user_id;
    break;
}           
        $select= new stdClass();
        if ($user==$user_id) {
                $select->status="1";
                $select->message="User total like ";
                $select->data=json_encode($usersname);
        }else{
                $select->status="0";
                $select->message="user not found";
        }
           
                return response()->json($select);
   
    }else{

             $select= new stdClass();
              $select->status="0";
                $select->message="Please select user id";
                 return response()->json($select);
        }
    

}

/*********************job like api **********************/
public function joblikeapi(Request $request){

            $user_id=$request['user_id'];
            $like_user=$request['like_user_id'];
            $job_id=$request['job_id'];
$joblike=Joblike::select('*')->where('user_id',$user_id)->where('like_user_id',$like_user)->where('job_id',$job_id)->get();

$userid='';
$likeuser='';
foreach ($joblike as $key => $value) {
    
   $userid=$value->user_id;
   $likeuser=$value->like_user_id;
}

if (!empty($user_id) && !empty($like_user)) {

        if($userid !=$user_id && $likeuser != $like_user){
             $like=Joblike::create([

                 'user_id'          =>$user_id,
                 'like_user_id'     =>$like_user,
                 'job_id'           =>$job_id,

            ]);

             $inserted= new stdClass();
             if($like==true){
                    $inserted->status="1";
                    $inserted->message="job liked";
             }else{
                $inserted->status="0";
                $inserted->message="job not like";
             }
                return response()->json($inserted);
        }else{

                $delete=Joblike::whereuser_id($user_id)->wherelike_user_id($like_user)->wherejob_id($job_id)->delete($userid);
            if ($delete==true) {
               $inserted= new stdClass();
                $inserted->status="0";
                $inserted->message="unlike prifile";
                return response()->json($inserted);
            }
                

        }
           

}

}

/***********************job like total list*************/
public function totaljoblike(Request $request){
            $jobid=$request['job_id'];
            if (!empty($jobid)) {

                 $like=Joblike::where('job_id',$jobid)->count('job_id');
          
            $total= new stdClass();
             if($like > 0){
                    $total->status="1";
                    $total->message="job total like ";
                    $total->data=json_encode($like);
             }else{
                $total->status="0";
                $total->message="not any like on this job";
             }
                return response()->json($total);
               
            }
           
}




/*************************job like wise user listing***********************/

public function joblikeuser_list(Request $request){

    $job_id=$request['job_id'];

if (!empty($job_id)) {

    $usersname = DB::table('job_total_like')
            ->leftjoin('chat_user', 'job_total_like.like_user_id', '=', 'chat_user.id')
            ->leftjoin('user_personal_info', 'job_total_like.like_user_id', '=', 'user_personal_info.user_id')
            ->select('job_total_like.job_id','job_total_like.user_id','chat_user.id as like_userid', 'chat_user.user_name as likeuser_name','user_personal_info.image as likeuser_image')
            ->where('job_total_like.job_id',$job_id)
            ->get();
    $jobid='';        
foreach ($usersname as $key => $value) {
    $jobid=$value->job_id;
    break;
}           
        $select= new stdClass();
        if ($jobid==$job_id) {
                $select->status="1";
                $select->message="job  total like user list";
                $select->data=json_encode($usersname);
        }else{
                $select->status="0";
                $select->message="user not found";
        }
           
                return response()->json($select);
   
    }else{

             $select= new stdClass();
              $select->status="0";
                $select->message="Please select job id";
                 return response()->json($select);
        }
    
}


/*********************login user like none login user profile ******************/

public function nonloginuser_list(Request $request){
$user_id=$request['user_id'];

if (!empty($user_id)) {

    $usersname = DB::table('user_total_like')
            ->leftjoin('chat_user', 'user_total_like.user_id', '=', 'chat_user.id')
            ->leftjoin('user_personal_info','user_personal_info.user_id','=','chat_user.id')
            ->select('user_total_like.like_user_id','chat_user.id','user_personal_info.image', 'chat_user.user_name')
            ->where('user_total_like.like_user_id',$user_id)
            ->get();
    $user='';        
foreach ($usersname as $key => $value) {
    $user=$value->like_user_id;
    break;
}           
        $select= new stdClass();
        if ($user==$user_id) {
                $select->status="1";
                $select->message="User total like ";
                $select->data=$usersname;
        }else{
                $select->status="0";
                $select->message="user not found";
        }
           
                return json_encode($select);
   
    }else{

             $select= new stdClass();
              $select->status="0";
                $select->message="Please select user id";
                 return response()->json($select);
        }
    

}


/******************************login user job like list*************/
public function loginuser_likejob(Request $request){

    $user_id=$request['user_id'];
/*echo $user_id;
die();*/
if (!empty($user_id)) {

    $usersname = DB::table('job_total_like')
            ->leftjoin('chat_user', 'job_total_like.user_id', '=', 'chat_user.id')
            ->leftjoin('user_personal_info', 'job_total_like.user_id', '=', 'user_personal_info.user_id')
            ->leftjoin('user_job_post','user_job_post.id','=','job_total_like.job_id')
            ->select('job_total_like.job_id','user_job_post.user_name as job_title','user_job_post.created_at as jobpost_date','user_job_post.start_date as jobstart_date','job_total_like.like_user_id','job_total_like.user_id', 'chat_user.user_name as likeuser_name','user_personal_info.image as likeuser_image')
            ->where('job_total_like.like_user_id',$user_id)
            ->get();
    $likeuser_id='';        
foreach ($usersname as $key => $value) {
    $likeuser_id=$value->like_user_id;
    break;
}           
        $select= new stdClass();
        if ($likeuser_id==$user_id) {
                $select->status="1";
                $select->message="job  total like user list";
                $select->data=$usersname;
        }else{
                $select->status="0";
                $select->message="user not found";
        }
           
                return json_encode($select);
   
    }else{

             $select= new stdClass();
              $select->status="0";
                $select->message="Please select job id";
                 return response()->json($select);
        }
    
}


/********************total num of count peopele and job which is liked login user ******/


public function total_job_people_like(Request $request){
    $obj= new stdClass;
    $user_id=$request['like_user_id'];
if(!empty($user_id))
{
    $totaljob="SELECT COUNT(user_id) as totaljobfav FROM job_total_like WHERE like_user_id='$user_id'";
    $job=DB::select($totaljob);

    $totalpeople="SELECT COUNT(user_id) as totalpeoplefav FROM user_total_like WHERE like_user_id='$user_id'";
    $people=DB::select($totalpeople);

        $marge=array_merge($job,$people);
        $obj->status="1";
        $obj->message="all count people and job like";
        $obj->totaldata=$marge;
        return json_encode($obj);
}else{
       $obj->status="0";
        $obj->message="Can not blank user id";
       
        return json_encode($obj);
}
}


}
