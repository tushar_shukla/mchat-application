<?php

namespace App\Http\Controllers;

use App\Lastseen;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;
use Session;
use View;
use Validator;
use stdClass;

class LastseenController extends Controller
{
    

    public function lastseen(Request $request){

    $userlast=Lastseen::select('*')->where('user_id',$request['user_id'])->get();

        $userid='';
        $last='';
        $status='';
    foreach ($userlast as $key => $value) {
            
            $userid=$value->user_id;
            $last=$value->lastseen;
            $status=$value->status;
            break;
    }
            $lastseen= now();
    if($userid != $request['user_id'] && $status !=1){

            $status=1;
            $insert=Lastseen::create([

                'user_id'     => $request['user_id'],
                'last_seen'   => $lastseen,
                'status'      => $status

            ]);
        $userlast=Lastseen::select('*')->where('user_id',$request['user_id'])->get();

            $userdata = new stdClass();
            if($userlast != 0){

                $userdata->status='1';
                $userdata->message='user last seen';
                $userdata->data= json_encode($userlast);
            }else{
                $userdata->status='0';
                $userdata->message='not any user last seen';
            }

            return response()->json($userdata);
        }
            /*Lastseen::where('user_id',$request['user_id'])->update([

                'user_id'     => $request['user_id'],
                'last_seen'   => $lastseen,
                'status'      => $status

            ]);*/

    }





}
