<?php

namespace App\Http\Controllers;

use App\Location;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;
use Validator;
use App\Cityname;
use stdClass;

class LocationController extends Controller
{

  /*******************************Country_Coding*****************************/  
    public function index()
    {
    		
    		$country=Location::select('id','Code','Name','Code2')->get();
            return view('admin.add_country',compact('country'));


    }

public function insert_country(Request $request){



$validation=$this->validate($request, [
            'country_name' => 'required|max:40',
            'country_code' => 'required|max:3',
           
            ]);

if($validation==true){
$data=Location::select('*')->where('Name',ucfirst($request['country_name']))->where('Code',strtoupper($request['country_code']))->get();
$code='';
$name='';
$code2='';
foreach ($data as $key => $value) {
		$code=$value->Code;
		$name=$value->Name;
		$code2=$value->Code2;
		break;
	}	


	if(ucfirst($request['country_name']) ==$name && strtoupper($request['country_code']) ==$code)
			{

				Session::flash('flash_ma','This Country All Exist');
    			 return redirect('/Add/Country/');
				/*Location::create([

							'Name'	=> ucfirst($request['country_name']),
							'Code'	=> strtoupper($request['country_code']),

				]);

				Session::flash('flash_message','Country Added Succsessfully');
    			 return redirect('/Add/Country/');*/

			}else{

				Location::create([

							'Name'	=> ucfirst($request['country_name']),
							'Code'	=> strtoupper($request['country_code']),

				]);

				Session::flash('flash_message','Country Added Succsessfully');
    			 return redirect('/Add/Country/');
			}



}else{
	 Session::flash('flash_ma','Can not blank Country name or code');
     return redirect('/Add/Country/');
}


}

public function updatecountry($id){
	$edit_country=Location::select('*')->whereid($id)->first();
	return view('admin.update_country',compact('edit_country'));
}



public function update_country($id,Request $request){

	$data=Location::select('*')->where('Name',ucfirst($request['country_name']))->where('Code',strtoupper($request['country_code']))->get();
$code='';
$name='';
$code2='';
foreach ($data as $key => $value) {
		$code=$value->Code;
		$name=$value->Name;
		$code2=$value->Code2;
		break;
	}	
if(ucfirst($request['country_name']) !=$name && strtoupper($request['country_code']) !=$code)
			{

 Location::whereid($id)->update([

                   'Name'	=> ucfirst($request['country_name']),
				   'Code'	=> strtoupper($request['country_code']),
            ]);
 			Session::flash('flash_message',' Country Update Succsessfully');
    		return back();
}else{

			Session::flash('flash_ma','This Country Already Exist');
    		return back();
}

}


public function delete_country($id){

	Location::whereid($id)->delete($id);
        Session::flash('flash','Country deleted successfully');
          return redirect('/Add/Country/');
}

/********************************City_Coding**********************************/

public function citymanagement()
    {

    		$country=Location::select('id','Code','Name','Code2')->get();
        	$city=Cityname::select('ID','CountryCode','Name')->get();
            return view('admin.add_city',compact('city','country'));

    }

/***************Add City*********************/

public function addcity(Request $request){


	$valid=$this->validate($request, [
            'country_name' => 'required|max:70',
            'city_name' => 'required|max:70',
           
            ]);

if($valid==true){

	$cityval=Cityname::select('*')->where('CountryCode',$request['country_name'])->where('Name',$request['city_name'])->get();
	$state='';
	$city='';
	foreach ($cityval as $key => $value) {
		$state=$value->CountryCode;
		$city=$value->Name;
		break;
	}

if(ucfirst($request['city_name']) !=$city && $state != $request['country_name']){

	if(!empty($request['country_name']) && !empty($request['city_name'])){

		Cityname::create([
			'Name'	=> ucfirst($request['city_name']),
			'CountryCode'	=> $request['country_name'],
			'_token'		=>$request['_token'],


		]);

			Session::flash('flash_message','City Added Succsessfully');
    			 return redirect('/Add/City/');
	}else{

			Session::flash('flash_ma','Please Select Country');
			return redirect('/Add/City/');
	}

}else{
		Session::flash('flash_ma','This City Already Exist');
		return redirect('/Add/City/');
}

}else{

	return redirect('/Add/City/');
}


}

/***************delete City*******************/

public function deletecity($ID){

		Cityname::whereid($ID)->delete($ID);
        Session::flash('flash','City deleted successfully');
          return redirect('/Add/City/');

}

/***********************Update City*************/
public function updatecity($ID){
	$country=Location::select('id','Code','Name','Code2')->get();
	$edit_city=Cityname::select('*')->whereid($ID)->first();
    return view('admin.update_city',compact('edit_city','country'));
}


public function update_city($ID, Request $request){

$cityval=Cityname::select('*')->where('CountryCode',$request['country_name'])->where('Name',$request['city_name'])->get();
	$country='';
	$city='';
	foreach ($cityval as $key => $value) {
		$country=$value->CountryCode;
		$city=$value->Name;
		break;
	}
if(!empty($request['city_name']) && !empty($request['country_name'])){
if($country !=$request['country_name'] && $city !=ucfirst($request['city_name'])){
	 Cityname::whereid($ID)->update([

                   'CountryCode'	=> $request['country_name'],
				   'Name'	=> ucfirst($request['city_name']),
            ]);

	 Session::flash('flash_message','City Update Succsessfully');
    return back();
	}else{

		Session::flash('flash_ma','This City Already Exist');
		return back();

	}
}else{

	Session::flash('flash_ma','Can not Blank Fields');
	return back();
}
}

 /******************************API Code Country********************/

 public function apilist(){

$country=Location::select('id','Code','Name','Code2')->get();
//return response()->json($country);
$obj=new stdClass();
        if(count($country)>0)
        {	
        	$obj->message="successfully";
            $obj->status="1";
            $obj->data=json_encode($country);    
        }
        else
        {
        	$obj->message="Unsuccess";
            $obj->status="0";
            $obj->data=null;
        }

        return response()->json($obj);
 } 


 public function citylist(Request $request){
 $city=Cityname::select('ID','CountryCode','Name')->where('CountryCode',$request['country'])->get();
	//return response()->json($city);

 $newcity=new stdClass();
        if(count($city)>0)
        {	
        	$newcity->message="successfully";
            $newcity->status="1";
            $newcity->data=json_encode($city);    
        }
        else
        {
        	$newcity->message="Unsuccess";
            $newcity->status="0";
            $newcity->data=null;
        }

        return response()->json($newcity);
 } 
}
