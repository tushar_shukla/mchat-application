<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use stdClass;
use App\Uservideo;
use Validator;
use VideoThumbnail;

class ProfilevideoController extends Controller
{
   
public function upload(Request $request){
	$user_id=$request['user_id'];
	$status=0;
	$date=now();
	/*$file=$request['video'];
 	$videopath = "video/";
 	$target_file = $videopath.basename($file);*/
 	/*$filetype = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));*/
	
 $rules=['video' =>'mimes:mpeg,ogg,mp4,webm,3gp,mov,flv,avi,wmv,ts|max:100040|required'];

 $validator = Validator($rules);

if ($validator==true) {
	$storageUrl=public_path('video/user_video_thumb');
    $filename=time().".png"; 
	$videoname = "video/".time().'.'.request()->video->getClientOriginalExtension();
      request()->video->move(public_path('video'), $videoname);
$homevideo=public_path($videoname);
VideoThumbnail::createThumbnail($homevideo,$storageUrl,$filename,2,640,480);

	$insert=Uservideo::create([
		'user_id'	=>$user_id,
		'video_url'	=>$videoname,
		'user_video_thumbnail' =>$filename,
		'status'	=>$status,
		'date'		=>$date,
]);
$insertdata=new stdClass;
	
if ($insert==true) {
	$insertdata->status="1";
	$insertdata->message="video upload succsessfully";
	 return response()->json($insertdata);


}else{
	$insertdata->status="0";
	$insertdata->message="video not upload";
	 return response()->json($insertdata);
}
	
}else{
	$insertdata=new stdClass;
	$insertdata->status="0";
	$insertdata->message="please select proper video formate";
	return response()->json($insertdata);
}


}
/**********************defalut video***********************/
public function videodefault(Request $request){
$user_id=$request['user_id'];
$id=$request['video_id'];
if (!empty($user_id)) {
	
$data=Uservideo::select('*')->where('user_id',$user_id)->where('id',$id)->get();
$datavideo='';
$datastatus='';
$videoid='';
$newstatus=1;
$status=0;
foreach($data as $key => $value) {
		
		$datavideo=$value->video_url;
		$datastatus=$value->status;
		$videoid=$value->id;
		break;
	}
  $update=Uservideo::whereuser_id($user_id)->update([

		'status'	=>$status,
	]);
	$update=Uservideo::whereid($videoid)->update([

		'status'	=>$newstatus,
	]);

	$imp= new stdClass;
if ($update==true) {
	
	$imp->status="1";
	$imp->message="your video selected as default";
	return response()->json($imp);

}else{
	$imp->status="0";
	$imp->message="somethinq went wrong plz try again";
	return response()->json($imp);
}
	

}

}


/**********************user default video list******************/

public function uservideolist(Request $request){
$status=1;
$user_id=$request['user_id'];
if (!empty($user_id)) {
	/*$videodata=Uservideo::select('*')->where('user_id',$user_id)->where('status',$status)->get();
	$image=Userpersonal::select('image')->where('user_id',$user_id)->get();*/
	$data=DB::table('user_video')
			->leftjoin('user_personal_info','user_personal_info.user_id','=','user_video.user_id')
			->where('user_video.user_id',$user_id)
			->where('user_video.status',1)
			->select('user_video.*','user_personal_info.image')
			->get();
	$obj=new stdClass;

	if ($data==true) {
		
		$obj->status="1";
		$obj->message="default video list";
		$obj->videolist=$data;
	}else{
		$obj->status="0";
		$obj->message="data not found";
	}

	return json_encode($obj);
}else{
	//$videodata=Uservideo::select('*')->where('status',$status)->get();

	/*$data=DB::table('user_video')
			->leftjoin('user_personal_info','user_personal_info.user_id','=','user_video.user_id')
			->leftjoin('chat_user','chat_user.id','=','user_personal_info.user_id')
			->where('user_video.status',1)
			->select('user_video.*','user_personal_info.image','chat_user.user_name','user_personal_info.p_country as nationality','user_personal_info.gender')
			->get();*/
	$select="SELECT user_video.id,user_video.user_id,user_video.video_url,user_video.status,user_personal_info.gender,user_personal_info.p_country as nationality,chat_user.user_name,YEAR(CURRENT_DATE)-YEAR(user_personal_info.dob) age,(avg(user_total_rating.discipline)+avg(user_total_rating.responsbility)+avg(user_total_rating.cleanness_and_tidy)+avg(user_total_rating.communication)+avg(user_total_rating.patient)+avg(user_total_rating.experience)+avg(user_total_rating.attitude))/7 as Average_rating,user_personal_info.image FROM user_video LEFT JOIN user_personal_info ON user_video.user_id=user_personal_info.user_id LEFT JOIN chat_user ON chat_user.id=user_video.user_id LEFT JOIN user_total_rating ON user_total_rating.user_id=user_video.user_id WHERE user_video.status='1' GROUP BY user_video.id,user_video.user_id,user_video.video_url,user_video.status,user_personal_info.gender,user_personal_info.p_country,chat_user.user_name,user_personal_info.dob,user_personal_info.image";

	$data=DB::select($select);
	
	$obj=new stdClass;

	if ($data==true) {
		
		$obj->status="1";
		$obj->message="default video list";
		$obj->videolist=$data;
	}else{
		$obj->status="0";
		$obj->message="data not found";
	}

	return json_encode($obj);
}


}

/*******************delete User profile video*****************/

public function delete_user_profile_video(Request $request){

	$videoid=$request['video_id'];
	$user_id=$request['user_id'];
	$obj = new stdClass;
	if(!empty($videoid) && !empty($user_id)){

		 $delete=Uservideo::where('user_id',$user_id)->where('id',$videoid)->delete();

		 if($delete==true){
		 	$obj->status='1';
		    $obj->message='video delete succsessfully';
		 }else{
		 	$obj->status='0';
		    $obj->message='somthing went wrong please try agian';
		
		 }
		 return json_encode($obj);
	}else{

		$obj->status='0';
		$obj->message='can not blank video id and user id';
		return json_encode($obj);
	}
}

}
