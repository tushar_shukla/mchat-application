<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;
use Session;
use View;
use Validator;
use App\Joblanguage;
use App\Jobscreening;
use App\Jobskill;
use App\Adminnotification;
use App\Jobpost;
use stdClass;
use VideoThumbnail;

class UpdatepostjobController extends Controller
{
 
public function updatepostjob(Request $request){

$jobid=$request['jobid'];

		//$query=Jobpost::select('*')->where('id',$jobid)->get();
		

		$query=DB::table('user_job_post')
            ->leftjoin('user_job_skill', 'user_job_post.id', '=', 'user_job_skill.job_id')
            ->leftjoin('job_screening_que', 'user_job_post.id', '=', 'job_screening_que.job_id')
            ->leftjoin('user_jobpost_language', 'user_job_post.id', '=', 'user_jobpost_language.job_id')
            ->select('user_job_post.*', 'user_job_skill.skills', 'job_screening_que.sreening_question','user_jobpost_language.job_language')
            ->where('user_job_post.id',$jobid)
            ->get();
        
$videonull='';
$voicenull='';
$descriptionnull='';
 		$status=1;
        $notistatus=1;
        $type='job';
        $video=$request['video'];   
        $published=0;  
        $draff=1;  
        $active=0;  
        $make_private=0;  
        $id='';
      foreach ($query as $key => $value) {
      		
      			$id=$value->id;
      }

      if($id==$jobid){
      		/* echo $id;
      		 die();*/
      if($request['video'] !=''){
    $storageUrl=public_path('postjobvice/job_post_thumb');
    $filename=time().".png";
   /* $videoname = "postjobvideo/".time().'.'.request()->video->getClientOriginalExtension();*/
    $videoname = "postjobvice/".time().'.'.request()->video->getClientOriginalExtension();
    $baseurl = pathinfo($videoname, PATHINFO_EXTENSION);


    
    if($baseurl=='mp4'){
            request()->video->move(public_path('postjobvice'), $videoname);
            $homevideo=public_path($videoname);
      VideoThumbnail::createThumbnail($homevideo,$storageUrl,$filename,2,640,480);
            $job=Jobpost::whereid($jobid)->update([

                'user_id'           =>$request['user_id'],
                'user_name'         =>$request['user_name'],
                'nationality'       =>$request['nationality'],
                'work_type'         =>$request['work_type'],
                'working_time'      =>$request['working_time'],
                'salery_range'      =>$request['salery_range'],
                'video'             =>$videoname,
                'video_thumb'       =>$filename,
                'voice'             =>$voicenull,
                'description'       =>$descriptionnull,
                'location'          =>$request['location'],
                'salery'            =>$request['salery'],
                'public_holiday'    =>$request['public_holiday'],
                'leave_option'      =>$request['leave_option'],
                'if_option_other'   =>$request['if_option_other'],
                'chareterstics'     =>$request['chareterstics'],
                'start_date'        =>$request['start_date'],
                'status'            =>$status,
                'published'         =>$published,
                'draft'             =>$draff,
                'active'            =>$active,
                'make_private'      =>$make_private,
                'trip_home'         =>$request['trip_home'],
                'trip_home_other'   =>$request['trip_home_other'],
                

            ]);

            $delete=Joblanguage::where('job_id',$jobid)->delete($jobid);

           	$data=$request['job_language'];
            $language = explode(',', $data);
            $leveldata=$request['lang_level'];
            $level = explode(',', $leveldata);
            
            $jobcount=count($language);
            for ($i=0; $i < $jobcount ; $i++) { 
               
               $joblang=Joblanguage::create([
                
                'job_id'            =>$jobid,
                'user_id'           =>$request['user_id'],
                'language_position' =>$level[$i],
                'job_language'      =>$language[$i],
                'status'            =>$status,
                
                 ]);  
            }

            $skilldelete=Jobskill::where('job_id',$jobid)->delete($jobid);
            $dataskills=$request['skills'];
            $rr1= str_replace('[','',$dataskills);
            $rr= str_replace(']','',$rr1);
            $skilldata = explode(',', $rr);
            

            $totskill=count($skilldata);
        for ($i=0; $i < $totskill ; $i++) { 
            $jobskill=Jobskill::create([

                'job_id'         =>$jobid,
                'user_id'        =>$request['user_id'],
                'skills'         =>$skilldata[$i],
                'status'         =>$status,
            ]); 
        }

        $screendelete=Jobscreening::where('job_id',$jobid)->delete($jobid);
        $screen=$request['sreening_question'];
            $screrendata= explode(',', $screen);
            $totscreen=count($screrendata);
        for ($i=0; $i < $totscreen; $i++) { 
          
                $jobscreen=Jobscreening::create([

                'job_id'               =>$jobid,
                'user_id'              =>$request['user_id'],
                'sreening_question'    =>$screrendata[$i],
                'status'               =>$status,

        ]); 



        }

             $inserted=new stdClass();
         if($jobscreen==true){
            
                $inserted->status="1";
                $inserted->message="Job post data Updated";
                
               return response()->json($inserted);
               
           }else{
                    $inserted->status="0";
                    $inserted->message="job post data Not update";
                    return response()->json($inserted);
           }

      }elseif($baseurl=='mp3'){

      		request()->video->move(public_path('postjobvice'), $videoname);
            $job=Jobpost::whereid($jobid)->update([

                'user_id'           =>$request['user_id'],
                'user_name'         =>$request['user_name'],
              	'nationality'       =>$request['nationality'],
                'work_type'         =>$request['work_type'],
                'working_time'      =>$request['working_time'],
                'salery_range'      =>$request['salery_range'],
               	'voice'             =>$videoname,
                'description'       =>$descriptionnull,
                'video'             =>$videonull,
                'location'          =>$request['location'],
                'salery'            =>$request['salery'],
                'public_holiday'    =>$request['public_holiday'],
                'leave_option'      =>$request['leave_option'],
                'if_option_other'   =>$request['if_option_other'],
                'chareterstics'     =>$request['chareterstics'],
                'start_date'        =>$request['start_date'],
                'status'            =>$status,
                'published'         =>$published,
                'draft'             =>$draff,
                'active'            =>$active,
                'make_private'      =>$make_private,
                'trip_home'         =>$request['trip_home'],
                'trip_home_other'   =>$request['trip_home_other'],
                

            ]);

              $delete=Joblanguage::where('job_id',$jobid)->delete($jobid);

           	$data=$request['job_language'];
            $language = explode(',', $data);
            $leveldata=$request['lang_level'];
            $level = explode(',', $leveldata);
            
            $jobcount=count($language);
            for ($i=0; $i < $jobcount ; $i++) { 
               
               $joblang=Joblanguage::create([
                
                'job_id'            =>$jobid,
                'user_id'           =>$request['user_id'],
                'language_position' =>$level[$i],
                'job_language'      =>$language[$i],
                'status'            =>$status,
                
                 ]);  
            }

            $skilldelete=Jobskill::where('job_id',$jobid)->delete($jobid);
            $dataskills=$request['skills'];
            $rr1= str_replace('[','',$dataskills);
            $rr= str_replace(']','',$rr1);
            $skilldata = explode(',', $rr);
            

            $totskill=count($skilldata);
        for ($i=0; $i < $totskill ; $i++) { 
            $jobskill=Jobskill::create([

                'job_id'         =>$jobid,
                'user_id'        =>$request['user_id'],
                'skills'         =>$skilldata[$i],
                'status'         =>$status,
            ]); 
        }

        $screendelete=Jobscreening::where('job_id',$jobid)->delete($jobid);
        $screen=$request['sreening_question'];
            $screrendata= explode(',', $screen);
            $totscreen=count($screrendata);
        for ($i=0; $i < $totscreen; $i++) { 
          
                $jobscreen=Jobscreening::create([

                'job_id'               =>$jobid,
                'user_id'              =>$request['user_id'],
                'sreening_question'    =>$screrendata[$i],
                'status'               =>$status,

        ]); 



        }

             $inserted=new stdClass();
         if($jobscreen==true){
            
                $inserted->status="1";
                $inserted->message="Job post data Updated";
                
               return response()->json($inserted);
               
           }else{
                    $inserted->status="0";
                    $inserted->message="job post data Not update";
                    return response()->json($inserted);
           }

      }
      }else{

      		 $job=Jobpost::whereid($jobid)->update([

                'user_id'           =>$request['user_id'],
                'user_name'         =>$request['user_name'],
                'description'       =>$request['description'],
                'video'             =>$videonull,
                'voice'             =>$voicenull,
                'nationality'       =>$request['nationality'],
                'work_type'         =>$request['work_type'],
                'working_time'      =>$request['working_time'],
                'salery_range'      =>$request['salery_range'],
                'location'          =>$request['location'],
                'salery'            =>$request['salery'],
                'public_holiday'    =>$request['public_holiday'],
                'leave_option'      =>$request['leave_option'],
                'if_option_other'   =>$request['if_option_other'],
                'chareterstics'     =>$request['chareterstics'],
                'start_date'        =>$request['start_date'],
                'status'            =>$status,
                'published'         =>$published,
                'draft'             =>$draff,
                'active'            =>$active,
                'make_private'      =>$make_private,
                'trip_home'         =>$request['trip_home'],
                'trip_home_other'   =>$request['trip_home_other'],
                

            ]);

      	  $delete=Joblanguage::where('job_id',$jobid)->delete($jobid);

           	$data=$request['job_language'];
            $language = explode(',', $data);
            $leveldata=$request['lang_level'];
            $level = explode(',', $leveldata);
            
            $jobcount=count($language);
            for ($i=0; $i < $jobcount ; $i++) { 
               
               $joblang=Joblanguage::create([
                
                'job_id'            =>$jobid,
                'user_id'           =>$request['user_id'],
                'language_position' =>$level[$i],
                'job_language'      =>$language[$i],
                'status'            =>$status,
                
                 ]);  
            }

            $skilldelete=Jobskill::where('job_id',$jobid)->delete($jobid);
            $dataskills=$request['skills'];
            $rr1= str_replace('[','',$dataskills);
            $rr= str_replace(']','',$rr1);
            $skilldata = explode(',', $rr);
            

            $totskill=count($skilldata);
        for ($i=0; $i < $totskill ; $i++) { 
            $jobskill=Jobskill::create([

                'job_id'         =>$jobid,
                'user_id'        =>$request['user_id'],
                'skills'         =>$skilldata[$i],
                'status'         =>$status,
            ]); 
        }

        $screendelete=Jobscreening::where('job_id',$jobid)->delete($jobid);
        $screen=$request['sreening_question'];
            $screrendata= explode(',', $screen);
            $totscreen=count($screrendata);
        for ($i=0; $i < $totscreen; $i++) { 
          
                $jobscreen=Jobscreening::create([

                'job_id'               =>$jobid,
                'user_id'              =>$request['user_id'],
                'sreening_question'    =>$screrendata[$i],
                'status'               =>$status,

        ]); 



        }

             $inserted=new stdClass();
         if($jobscreen==true){
            
                $inserted->status="1";
                $inserted->message="Job post data Updated";
                
               return response()->json($inserted);
               
           }else{
                    $inserted->status="0";
                    $inserted->message="job post data Not update";
                    return response()->json($inserted);
           			}

      		}
}else{

	$inserted->status="0";
    $inserted->message="Your Job does not match our record";
    return response()->json($inserted);
}



}
}


