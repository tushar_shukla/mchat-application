<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Updateprofile extends Model
{
    

protected $table='users';

protected $fillable=['name','email','city','state','country','image','address'];


}
