<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jobskill extends Model
{

protected $table='user_job_skill';

protected $fillable=['user_id','job_id','skills','status'];


}
