<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Applyjobscreen extends Model
{
    
protected $table='screen_job_apply';

protected $fillable=['screening_ans','status','given_ans','apply_user_id','question_id','apply_thumbnail'];


}
