<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Userquote extends Model
{
   
protected $table='user_quote';

protected $fillable=['user_id','quote','date','status'];


}
