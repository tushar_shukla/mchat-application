<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profession extends Model
{
 

protected $table='job_profession';


protected $fillable=['job_category_id','profession','status','_token'];

 public function name()
    {
        return $this->belongsTo(Jobcategory::class, 'job_category_id','id');
    }
}
