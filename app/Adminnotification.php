<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Adminnotification extends Model
{
    

protected $table='admin_notification';

protected $fillable=['user_id','user_name','post_job_id','post_job_user_id','seen','not_seen','status','type'];


}
