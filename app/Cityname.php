<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cityname extends Model
{
   

protected $table='city';

protected $fillable=['CountryCode','Name','_token'];

    public function name()
    {
        return $this->belongsTo(Location::class, 'CountryCode','Code');
    }

}
