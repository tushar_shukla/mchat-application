<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Uservideo extends Model
{
    
protected $table='user_video';

protected $fillable=['user_id','video_url','status','date','user_video_thumbnail'];


}
