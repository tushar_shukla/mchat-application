<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Userregister extends Model
{
    

 protected $table='chat_user';
 
 protected $fillable = ['user_name','email','password','status','otp','social_id','firebase_token'];


}
