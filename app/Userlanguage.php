<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Userlanguage extends Model
{
 
protected $table='user_language';

protected $fillable=['user_id','language_id','level','status'];


}
