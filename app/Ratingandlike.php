<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ratingandlike extends Model
{
    
protected $table='user_total_rating';

protected $fillable=['user_id','rating_user_id','rating_value','comment','voice','video','discipline','responsbility','cleanness_and_tidy','communication','patient','experience','attitude','laugh','smile','sad','video_thumb'];


}
