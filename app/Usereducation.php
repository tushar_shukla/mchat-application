<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usereducation extends Model
{
    
protected $table='user_education';

protected $fillable=['user_id','level','degree','board','from_year','to_year','date_created','status'];


}
