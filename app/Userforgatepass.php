<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Userforgatepass extends Model
{
   
 protected $table='reset_password';
 
 protected $fillable = ['email','otp','_token'];


}
